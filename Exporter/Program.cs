﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DescentCardGraphics;

namespace Exporter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting export to png\n\nDO NOT CLOSE THIS WINDOW UNLESS YOU WISH TO CANCEL THE EXPORT!!!\n\nThis window will close automatically then export is done!");
            foreach (string s in args)
            {
                Console.WriteLine("Exporting Database: " + s);
                DataFileHandler.LoadCreatorSet(s);
            }
            Console.WriteLine("All done closing down in a sec!");            
        }
    }
}
