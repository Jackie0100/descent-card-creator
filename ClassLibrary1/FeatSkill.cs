﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using DescentCardGraphics.Properties;

namespace DescentCardGraphics
{
    public class FeatSkill : Card
    {
        public FeatSkillTypes SkillType { get; set; }

        public FeatSkill() { }

        public FeatSkill(string _name, string _text, int _count, FeatSkillTypes _skill)
            : base(_name, _text, _count)
        {
            SkillType = _skill;
        }
    
        public override System.Drawing.Image DrawCard()
        {
            Bitmap bitmap = new Bitmap(AskType());
            bitmap.SetResolution(600, 600);
            using (Graphics tmp = Graphics.FromImage(bitmap))
            {
                if (Symbol != null)
                {
                    tmp.DrawSymbol(Image.FromFile(Symbol), new RectangleF(bitmap.Width / 1.17f, bitmap.Height - (bitmap.Height / 1.055f), tmp.DpiX / 8f, tmp.DpiY / 8f));
                }

                tmp.DrawString(Name, mainfont, mainbrush, new RectangleF((bitmap.Width / 5), (bitmap.Height / 13), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 12), strformat);
                tmp.DrawRtfText(Text, new RectangleF((bitmap.Width / 5), (bitmap.Height / 3.4f), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 1.8f));
            }
            return bitmap as Image;
        }

        private Image AskType()
        {
            if (SkillType == FeatSkillTypes.SkillFigthing)
            {
                return Resources.Skill_Fighting;
            }
            else if (SkillType == FeatSkillTypes.SkillSubterfuge)
            {
                return Resources.Skill_Subterfuge;
            }
            else if (SkillType == FeatSkillTypes.SkillWizardary)
            {
                return Resources.Skill_Wizardary;
            }
            else if (SkillType == FeatSkillTypes.FeatFigthing)
            {
                return Resources.Feat_Fighting;
            }
            else if (SkillType == FeatSkillTypes.FeatSubterfuge)
            {
                return Resources.Feat_Subterfuge;
            }
            else if (SkillType == FeatSkillTypes.FeatWizardary)
            {
                return Resources.Feat_Wizardary;
            }
            return null;
        }

        public override string ToString()
        {
            return "FeatSkill" + "\n" + Name + "\n" + Text + "\n" + Count + "\n" + SkillType.ToString();
        }

        public override Image GetCardBack()
        {
            if (SkillType == FeatSkillTypes.FeatFigthing)
            {
                return Resources.Feat_Fighting_Back;
            }
            else if (SkillType == FeatSkillTypes.FeatSubterfuge)
            {
                return Resources.Feat_Subterfuge_Back;
            }
            else if (SkillType == FeatSkillTypes.FeatWizardary)
            {
                return Resources.Feat_Wizardary_Back;
            }
            else if (SkillType == FeatSkillTypes.SkillFigthing)
            {
                return Resources.Skill_Fighting_Back;
            }
            else if (SkillType == FeatSkillTypes.SkillSubterfuge)
            {
                return Resources.Skill_Subterfuge_Back;
            }
            else if (SkillType == FeatSkillTypes.SkillWizardary)
            {
                return Resources.Skill_Wizardary_Back;
            }
            return null;
        }

        public override void Save(System.IO.BinaryWriter br)
        {
            br.Write("FeatSkill");
            br.Write(Name);
            br.Write(Text);
            br.Write(Count);
            br.Write(SkillType.ToString());
        }
    }
}