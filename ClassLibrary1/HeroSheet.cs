﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using DescentCardGraphics.Properties;

namespace DescentCardGraphics
{
    public class HeroSheet : Card
    {

        public string Wound { get; set; }
        public string Fatigue { get; set; }
        public string Armor { get; set; }
        public string Speed { get; set; }

        public int TraitMelee { get; set; }
        public int TraitSubterfuge { get; set; }
        public int TraitWizardary { get; set; }
        public string SkillMelee { get; set; }
        public string SkillSubterfuge { get; set; }
        public string SkillWizardary { get; set; }

        public string Picture { get; set; }
        public string ConquestValue { get; set; }

        public HeroSheet() { Picture = ""; Wound = "0"; Fatigue = "0"; Armor = "0"; Speed = "0"; ConquestValue = "0"; SkillMelee = "0"; SkillSubterfuge = "0"; SkillWizardary = "0"; }

        public HeroSheet(string _name, string _text, int _count, string _wound, string _fatigue, string _armor, string _speed,
            int _triatmelee, int _traitsubterfuge, int _traitwizardary, string _skillmelee, string _skillsubterfuge, string _skillwizardary,
            string _picture, string _conquestvalue)
            : base(_name, _text, _count)
        {
            Wound = _wound;
            Fatigue = _fatigue;
            Armor = _armor;
            Speed = _speed;

            TraitMelee = _triatmelee;
            TraitSubterfuge = _traitsubterfuge;
            TraitWizardary = _traitwizardary;
            SkillMelee = _skillmelee;
            SkillSubterfuge = _skillsubterfuge;
            SkillWizardary = _skillwizardary;

            Picture = _picture;
            ConquestValue = _conquestvalue;
        }

        public override Image DrawCard()
        {
            Bitmap bitmap = new Bitmap(Resources.HeroSheet.Width, Resources.HeroSheet.Height);
            try
            {
                bitmap.SetResolution(600, 600);
                using (Graphics tmp = Graphics.FromImage(bitmap))
                {
                    tmp.SetBestGraphic();
                    try
                    {
                        tmp.DrawImage(Image.FromFile(Picture), new RectangleF(bitmap.Width / 18, bitmap.Height / 18, (bitmap.Width / 2) - ((bitmap.Width / 18) * 1.5f), bitmap.Height - ((bitmap.Height / 18) * 2)));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    tmp.DrawImage(Resources.HeroSheet, new Point(0, 0));
                    Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                    Pen heropen = new Pen(Color.Black, 4 * (tmp.DpiX / g.DpiX));
                    heropen.LineJoin = LineJoin.Round;
                    heropen.Alignment = PenAlignment.Outset;
                    GraphicsPath path = new GraphicsPath();
                    path.AddString(Wound, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.82f, bitmap.Height / 5.55f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(Fatigue, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.52f, bitmap.Height / 5.55f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(Armor, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.32f, bitmap.Height / 5.55f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(Speed, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.16f, bitmap.Height / 5.55f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(SkillMelee, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.75f, bitmap.Height / 1.15f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(SkillSubterfuge, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.4f, bitmap.Height / 1.15f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(SkillWizardary, new FontFamily("CelticHand"), (int)FontStyle.Regular, 24 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 1.175f, bitmap.Height / 1.15f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    path.AddString(ConquestValue, new FontFamily("CelticHand"), (int)FontStyle.Regular, 28 * (tmp.DpiX / g.DpiX), new RectangleF(bitmap.Width / 32, bitmap.Height / 1.12f, bitmap.Width / 10, bitmap.Height / 20), strformat);
                    tmp.DrawPath(heropen, path);
                    tmp.FillPath(whitebrush, path);
                    tmp.DrawString(Name, new Font("KelmScott", 12, FontStyle.Regular), mainbrush, new RectangleF(bitmap.Width / 10, bitmap.Height / 30f, bitmap.Width / 3f, bitmap.Height / 30), strformat);
                    tmp.DrawRtfText(Text, new RectangleF(bitmap.Width / 1.95f, bitmap.Height / 1.95f, bitmap.Width / 2.4f, bitmap.Height / 4.5f));
                    float offset = 0;
                    for (int i = 0; i < TraitMelee; i++)
                    {
                        if (i < 3)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.67f) - offset, bitmap.Height / 2.4f, (Resources.Die_Black.Width * (tmp.DpiX / g.DpiX)) * 0.8f, (Resources.Die_Black.Height * (tmp.DpiY / g.DpiY)) * 0.8f));
                            offset += ((Resources.Die_Black.Width * ((tmp.DpiX / g.DpiX)) + (tmp.DpiX / g.DpiX)) * 0.8f);
                        }
                        else
                        {
                            if (i == 3)
                            {
                                offset = 0;
                            }
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.72f) - offset, bitmap.Height / 2.62f, (Resources.Die_Black.Width * (tmp.DpiX / g.DpiX)) * 0.8f, (Resources.Die_Black.Height * (tmp.DpiY / g.DpiY)) * 0.8f));
                            offset += ((Resources.Die_Black.Width * ((tmp.DpiX / g.DpiX)) + (tmp.DpiX / g.DpiX)) * 0.8f);
                        }
                    }

                    offset = 0;
                    for (int i = 0; i < TraitSubterfuge; i++)
                    {
                        if (i < 3)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.346f) - offset, bitmap.Height / 2.4f, (Resources.Die_Black.Width * (tmp.DpiX / g.DpiX)) * 0.8f, (Resources.Die_Black.Height * (tmp.DpiY / g.DpiY)) * 0.8f));
                            offset += ((Resources.Die_Black.Width * ((tmp.DpiX / g.DpiX)) + (tmp.DpiX / g.DpiX)) * 0.8f);
                        }
                        else
                        {
                            if (i == 3)
                            {
                                offset = 0;
                            }
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.378f) - offset, bitmap.Height / 2.62f, (Resources.Die_Black.Width * (tmp.DpiX / g.DpiX)) * 0.8f, (Resources.Die_Black.Height * (tmp.DpiY / g.DpiY)) * 0.8f));
                            offset += ((Resources.Die_Black.Width * ((tmp.DpiX / g.DpiX)) + (tmp.DpiX / g.DpiX)) * 0.8f);
                        }
                    }

                    offset = 0;
                    for (int i = 0; i < TraitWizardary; i++)
                    {
                        if (i < 3)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.128f) - offset, bitmap.Height / 2.4f, (Resources.Die_Black.Width * (tmp.DpiX / g.DpiX)) * 0.8f, (Resources.Die_Black.Height * (tmp.DpiY / g.DpiY)) * 0.8f));
                            offset += ((Resources.Die_Black.Width * ((tmp.DpiX / g.DpiX)) + (tmp.DpiX / g.DpiX)) * 0.8f);
                        }
                        else
                        {
                            if (i == 3)
                            {
                                offset = 0;
                            }
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.148f) - offset, bitmap.Height / 2.62f, (Resources.Die_Black.Width * (tmp.DpiX / g.DpiX)) * 0.8f, (Resources.Die_Black.Height * (tmp.DpiY / g.DpiY)) * 0.8f));
                            offset += ((Resources.Die_Black.Width * ((tmp.DpiX / g.DpiX)) + (tmp.DpiX / g.DpiX)) * 0.8f);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return bitmap as Image;
        }

        public override string ToString()
        {
            return "HeroSheet" + "\n" + Name + "\n" + Text + "\n" + Count + "\n" + Wound + "\n" + Fatigue + "\n" + Armor + "\n" + Speed + "\n" + TraitMelee.ToString() + "\n" + TraitSubterfuge.ToString() + "\n" + TraitWizardary.ToString() + "\n" + SkillMelee + "\n" + SkillSubterfuge + "\n" + SkillWizardary + "\n" + Picture + "\n" + ConquestValue;
        }

        public override void Save(System.IO.BinaryWriter br)
        {
            br.Write("HeroSheet");
            br.Write(Name);
            br.Write(Text);
            br.Write(Count);
            br.Write(Wound);
            br.Write(Fatigue);
            br.Write(Armor);
            br.Write(Speed);
            br.Write(TraitMelee);
            br.Write(TraitSubterfuge);
            br.Write(TraitWizardary);
            br.Write(SkillMelee);
            br.Write(SkillSubterfuge);
            br.Write(SkillWizardary);
            br.Write(Picture);
            br.Write(ConquestValue);
        }
    }
}
