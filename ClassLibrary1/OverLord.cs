﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using DescentCardGraphics.Properties;

namespace DescentCardGraphics
{
    public class OverLord : Card
    {
        public string ThreadCost { get; set; }
        public string DiscardThread { get; set; }
        public string CardType { get; set; }
        public OverLoadCardType OverlordCardType { get; set; }

        public OverLord()
        {
            ThreadCost = "0";
            DiscardThread = "0";
            CardType = "";
        }

        public OverLord(string _name, string _text, int _count, string _type, string _threadCost, string _discardThread, OverLoadCardType _overlordcardtype)
            : base(_name, _text,_count)
        {
            ThreadCost = _threadCost;
            DiscardThread = _discardThread;
            CardType = _type;
            OverlordCardType = _overlordcardtype;
        }

        public override Image DrawCard()
        {
            Bitmap bitmap = new Bitmap(AskType());
            bitmap.SetResolution(600, 600);
            using (Graphics tmp = Graphics.FromImage(bitmap))
            {
                if (Symbol != null)
                {
                    tmp.DrawSymbol(Image.FromFile(Symbol), new RectangleF(bitmap.Width / 1.178f, bitmap.Height - (bitmap.Height / 1.054f), tmp.DpiX / 7.5f, tmp.DpiY / 7.5f));
                }

                tmp.DrawString(Name, mainfont, mainbrush, new RectangleF((bitmap.Width / 5), (bitmap.Height / 14), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 12), strformat);
                tmp.DrawRtfText(Text, new RectangleF((bitmap.Width / 5), (bitmap.Height / 4.2f), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 1.8f));
                tmp.DrawString(CardType, overlordtypefont, mainbrush, new RectangleF((bitmap.Width / 5), (bitmap.Height / 7f), bitmap.Width - ((bitmap.Width / 5) * 2), bitmap.Height / 10), strformat);
                tmp.DrawString(ThreadCost, overlordnumberfont, mainbrush, new RectangleF(bitmap.Width / 12f, (bitmap.Height / 1.18f), bitmap.Width / 5, bitmap.Height / 12), strformat);
                tmp.DrawString(DiscardThread, overlordnumberfont, whitebrush, new RectangleF((bitmap.Width / 1.4f), (bitmap.Height / 1.18f), bitmap.Width / 5, bitmap.Height / 12), strformat);
            }
            return bitmap as Image;
        }

        private Image AskType()
        {
            if (OverlordCardType == OverLoadCardType.Normal)
            {
                return Resources.Overlord_Spawn;
            }
            else if (OverlordCardType == OverLoadCardType.Spawn)
            {
                return Resources.Overlord_Spawn;
            }
            else if (OverlordCardType == OverLoadCardType.Event)
            {
                return Resources.Overlord_Event;
            }
            else if (OverlordCardType == OverLoadCardType.Trap)
            {
                return Resources.Overlord_Trap;
            }
            else if (OverlordCardType == OverLoadCardType.TreacherySpawn)
            {
                return Resources.Treachery_Spawn;
            }
            else if (OverlordCardType == OverLoadCardType.TreacheryEvent)
            {
                return Resources.Treachery_Event;
            }
            else if (OverlordCardType == OverLoadCardType.TreacheryTrap)
            {
                return Resources.Treachery_Trap;
            }
            return null;
        }

        public override string ToString()
        {
            return "OverLord" + "\n" + Name + "\n" + Text + "\n" + Count + "\n" + CardType + "\n" + ThreadCost + "\n" + DiscardThread + "\n" + OverlordCardType.ToString();
        }

        public override Image GetCardBack()
        {
            return Resources.Overlord_Back;
        }

        public override void Save(System.IO.BinaryWriter br)
        {
            br.Write("OverLord");
            br.Write(Name);
            br.Write(Text);
            br.Write(Count);
            br.Write(CardType);
            br.Write(ThreadCost);
            br.Write(DiscardThread);
            br.Write(OverlordCardType.ToString());
        }
    }
}
