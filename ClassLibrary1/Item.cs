﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using DescentCardGraphics.Properties;

namespace DescentCardGraphics
{


    public class Item : Card
    {
        public string Type { get; set; }
        public string AttackType { get; set; }
        public int Price { get; set; }
        public string ItemImage { get; set; }
        public Hands Hand { get; set; }
        public List<DiceType> Dice { get; set; }
        public ItemType ItemType { get; set; }

        public Item() { Name = ""; Dice = new List<DiceType>(); Type = ""; AttackType = ""; ItemImage = ""; }

        public Item(string _name, string _text, int _count, string _type, string _attackType, int _price, string _itemImage, Hands _hands, List<DiceType> _dice, ItemType _itemType)
            : base(_name, _text, _count)
        {
            Type = _type;
            AttackType = _attackType;
            Price = _price;
            ItemImage = _itemImage;
            Hand = _hands;
            Dice = _dice;
            ItemType = _itemType;
        }

        public Item(string _name, string _text, int _count, string _type, string _attackType, int _price, string _itemImage, Hands _hands, string _dice, ItemType _itemType)
            : base(_name, _text, _count)
        {
            Type = _type;
            AttackType = _attackType;
            Price = _price;
            ItemImage = _itemImage;
            Hand = _hands;
            if (_dice != "")
            {
                foreach (string s in _dice.Split(';'))
                {
                    Dice.Add((DiceType)Enum.Parse(typeof(DiceType), s));
                }
            }
            ItemType = _itemType;
        }

        public override Image DrawCard()
        {
            Bitmap bitmap = new Bitmap(AskType());
            bitmap.SetResolution(600, 600);
            using (Graphics tmp = Graphics.FromImage(bitmap))
            {
                try
                {

                    tmp.DrawString(Name, mainfont, mainbrush, new RectangleF((bitmap.Width / 5), (bitmap.Height / 20), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 12), strformat);
                    tmp.DrawString(Type, mainfont, mainbrush, new RectangleF((bitmap.Width / 10), (bitmap.Height / 1.83f), (bitmap.Width - (bitmap.Width / 10) * 2), bitmap.Height / 22), strformat);


                    if (Symbol != null && Symbol != "")
                    {
                        tmp.DrawSymbol(Image.FromFile(Symbol), new RectangleF(bitmap.Width / 1.17f, bitmap.Height - (bitmap.Height / 1.055f), tmp.DpiX / 8f, tmp.DpiY / 8f));
                    }
                    try
                    {
                        tmp.DrawImage(Image.FromFile(ItemImage), new RectangleF((bitmap.Width / 5.9f), (bitmap.Height / 6.8f), (bitmap.Width - ((bitmap.Width / 5.9f) * 2)), (bitmap.Height / 2.74f)));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    if (AttackType == "" || AttackType == null)
                    {
                        tmp.DrawRtfText(Text, new RectangleF((bitmap.Width / 10), (bitmap.Height / 1.6f), (bitmap.Width - ((bitmap.Width / 10) * 2)), bitmap.Height / 4f));
                    }
                    else
                    {
                        tmp.DrawString(AttackType, typefont, mainbrush, new RectangleF((bitmap.Width / 10), (bitmap.Height / 1.68f), (bitmap.Width - (bitmap.Width / 10) * 2), bitmap.Height / 22), strformat);
                        tmp.DrawRtfText(Text, new RectangleF((bitmap.Width / 10), (bitmap.Height / 1.5f), (bitmap.Width - ((bitmap.Width / 10) * 2)), bitmap.Height / 5f));
                    }

                    if (Price != 0)
                    {
                        tmp.DrawImage(Resources.Coin, new RectangleF((bitmap.Width / 9.5f), bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 16), (bitmap.Height / 16)));
                        tmp.DrawString(Price.ToString(), mainfont, mainbrush, new RectangleF((bitmap.Width / 6), bitmap.Height - (bitmap.Height / 10.5f), (bitmap.Width / 3), (bitmap.Height / 22)), new StringFormat());
                    }

                    #region Hands
                    if (Hand == Hands.Hands1)
                    {
                        if (Price != 0)
                        {
                            tmp.DrawImage(Resources.Hand1_icon, new RectangleF((bitmap.Width / 2.8f), bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 6), (bitmap.Height / 18)));
                        }
                        else
                        {
                            tmp.DrawImage(Resources.Hand1_icon, new RectangleF((bitmap.Width / 9.5f), bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 6), (bitmap.Height / 18)));
                        }
                    }
                    if (Hand == Hands.Hands2)
                    {
                        if (Price != 0)
                        {
                            tmp.DrawImage(Resources.Hand2_icon, new RectangleF((bitmap.Width / 2.8f), bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 6), (bitmap.Height / 18)));
                        }
                        else
                        {
                            tmp.DrawImage(Resources.Hand2_icon, new RectangleF((bitmap.Width / 9.5f), bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 6), (bitmap.Height / 18)));
                        }
                    }
                    #endregion

                    #region Dice

                    float offset = 0;
                    try
                    {
                        foreach (DiceType d in Dice)
                        {
                            if (d == DiceType.Red)
                            {
                                tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18f)));
                            }
                            else if (d == DiceType.Blue)
                            {
                                tmp.DrawImage(Resources.Die_Blue, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 20)));
                            }
                            else if (d == DiceType.White)
                            {
                                tmp.DrawImage(Resources.Die_White, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Purple)
                            {
                                tmp.DrawImage(Resources.Die_Purple, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Morph)
                            {
                                tmp.DrawImage(Resources.Die_Morph, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Green)
                            {
                                tmp.DrawImage(Resources.Die_Green, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Yellow)
                            {
                                tmp.DrawImage(Resources.Die_Yellow, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Black)
                            {
                                tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Silver)
                            {
                                tmp.DrawImage(Resources.Die_Silver, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 18)));
                            }
                            else if (d == DiceType.Gold)
                            {
                                tmp.DrawImage(Resources.Die_Gold, new RectangleF((bitmap.Width / 1.27f) - offset, bitmap.Height - (bitmap.Height / 9.5f), (bitmap.Width / 12), (bitmap.Height / 20)));
                            }
                            offset += ((bitmap.Width / 12) * 1.12f);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return bitmap as Image;
        }

        private Image AskType()
        {
            if (ItemType == ItemType.Artifact)
            {
                return Resources.Artifact_Front;
            }
            else if (ItemType == ItemType.DarkRelic)
            {
                return Resources.Dark_Relic_Front;
            }
            else if (ItemType == ItemType.PartyUpgrade)
            {
                return Resources.PartyUpgrade_Front;
            }
            else
            {
                return Resources.Item_Front;
            }
        }

        public override string ToString()
        {
            string str = "";
            foreach (DiceType d in Dice)
            {
                str += d.ToString() + ";";
            }
            return "Item" + "\n" + Name + "\n" + Text + "\n" + Count + "\n" + Type + "\n" + AttackType + "\n" + Price + "\n" + ItemImage.ToString() + "\n" + Hand.ToString() + "\n" + str + "\n" + ItemType.ToString();
        }

        public override Image GetCardBack()
        {
            if (ItemType == DescentCardGraphics.ItemType.Artifact)
            {
                return DrawCard();
            }
            else if (ItemType == DescentCardGraphics.ItemType.CopperTreasure)
            {
                return Resources.Treasure_Copper_Back;
            }
            else if (ItemType == DescentCardGraphics.ItemType.DarkRelic)
            {
                return Resources.Dark_Relic_Back;
            }
            else if (ItemType == DescentCardGraphics.ItemType.GoldTreasure)
            {
                return Resources.Treasure_Gold_Back;
            }
            else if (ItemType == DescentCardGraphics.ItemType.PartyUpgrade)
            {
                return DrawCard();
            }
            else if (ItemType == DescentCardGraphics.ItemType.ShopItem)
            {
                return DrawCard();
            }
            else if (ItemType == DescentCardGraphics.ItemType.SilverTreasure)
            {
                return Resources.Treasure_Silver_Back;
            }
            return null;
        }

        public override void Save(System.IO.BinaryWriter br)
        {
            string str = "";
            foreach (DiceType d in Dice)
            {
                str += d.ToString() + ";";
            }
            br.Write("Item");
            br.Write(Name);
            br.Write(Text);
            br.Write(Count);
            br.Write(Type);
            br.Write(AttackType);
            br.Write(Price);
            br.Write(ItemImage);
            br.Write(Hand.ToString());
            br.Write(str);
            br.Write(ItemType.ToString());
        }
    }
}
