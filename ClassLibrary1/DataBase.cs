﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DescentCardGraphics
{
    public sealed class DataBase
    {
        public string SetIcon { get; set; }
        public string SetName { get; set; }
        public List<Card> SetCards { get; set; }

        private DataBase()
        {

        }

        public DataBase(string _name)
        {
            SetName = _name;
            SetCards = new List<Card>();
        }

        public DataBase(string _name, string _icon)
        {
            SetName = _name;
            SetIcon = _icon;
            SetCards = new List<Card>();
        }

        public DataBase(string _name, string _icon, List<Card> _cards)
        {
            SetName = _name;
            SetIcon = _icon;
            SetCards = _cards;
        }
    }
}