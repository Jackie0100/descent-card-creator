﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

public class RichTextBoxPrint : RichTextBox
{
    //Convert the unit used by the .NET framework (1/100 inch) 
    //and the unit used by Win32 API calls (twips 1/1440 inch)
    private const double anInch = 14.4;

    [StructLayout(LayoutKind.Sequential)]
    private struct RECT
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct CHARRANGE
    {
        public int cpMin;         //First character of range (0 for start of doc)
        public int cpMax;         //Last character of range (-1 for end of doc)
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct FORMATRANGE
    {
        public IntPtr hdc;             //Actual DC to draw on
        public IntPtr hdcTarget;       //Target DC for determining text formatting
        public RECT rc;                //Region of the DC to draw to (in twips)
        public RECT rcPage;            //Region of the whole DC (page size) (in twips)
        public CHARRANGE chrg;         //Range of text to draw (see earlier declaration)
    }

    private const int WM_USER = 0x0400;
    private const int EM_FORMATRANGE = WM_USER + 57;

    private const int PFM_SPACEAFTER = 128;

    private const int PFM_LINESPACING = 256;

    private const int EM_SETPARAFORMAT = 1095;

    [StructLayout(LayoutKind.Sequential)]

    public struct PARAFORMAT2
    {

        public int cbSize;

        public uint dwMask;

        public short wNumbering;

        public short wReserved;

        public int dxStartIndent;

        public int dxRightIndent;

        public int dxOffset;

        public short wAlignment;

        public short cTabCount;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]

        public int[] rgxTabs;

        // PARAFORMAT2 from here onwards.

        public int dySpaceBefore;

        public int dySpaceAfter;

        public int dyLineSpacing;

        public short sStyle;

        public byte bLineSpacingRule;

        public byte bOutlineLevel;

        public short wShadingWeight;

        public short wShadingStyle;

        public short wNumberingStart;

        public short wNumberingStyle;

        public short wNumberingTab;

        public short wBorderSpace;

        public short wBorderWidth;

        public short wBorders;

    }

    [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]

    public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, ref PARAFORMAT2 lParam);

    public void LineSpace()
    {
        PARAFORMAT2 fmt = new PARAFORMAT2();
        fmt.cbSize = Marshal.SizeOf(fmt);
        fmt.dwMask |= PFM_LINESPACING | PFM_SPACEAFTER;

        fmt.dyLineSpacing = (int)(22 * this.SelectionFont.SizeInPoints); // in twips
        // specify exact line spacing
        fmt.bLineSpacingRule = Convert.ToByte(4);
        SendMessage(this.Handle, EM_SETPARAFORMAT, 0, ref fmt); // 0 - to all text. 1 - only to seleted
    }

    public void LineSpace(int _linespace)
    {
        PARAFORMAT2 fmt = new PARAFORMAT2();
        fmt.cbSize = Marshal.SizeOf(fmt);
        fmt.dwMask |= PFM_LINESPACING | PFM_SPACEAFTER;

        fmt.dyLineSpacing = _linespace; // in twips
        // specify exact line spacing
        fmt.bLineSpacingRule = Convert.ToByte(4);
        SendMessage(this.Handle, EM_SETPARAFORMAT, 0, ref fmt); // 0 - to all text. 1 - only to seleted
    }


    [DllImport("USER32.dll")]
    private static extern IntPtr SendMessage(IntPtr hWnd, Int32 msg, Int32 wParam, Int32 lParam);

    public Bitmap PrintImage(Size size)
    {
        RECT rcToPrint = default(RECT);
        FORMATRANGE fr = default(FORMATRANGE);
        IntPtr iptHdc = IntPtr.Zero;
        IntPtr iptRes = IntPtr.Zero;
        IntPtr iptParam = IntPtr.Zero;
        Bitmap image = new Bitmap(size.Width, size.Height);
        Graphics g = Graphics.FromImage(image);

        // Calculate the area to render and print
        rcToPrint.Top = 0;
        rcToPrint.Bottom = (int)Math.Ceiling((g.VisibleClipBounds.Height * anInch));
        rcToPrint.Left = 0;
        rcToPrint.Right = (int)Math.Ceiling((g.VisibleClipBounds.Width * anInch));

        iptHdc = g.GetHdc();

        //Indicate character from to character to 
        fr.chrg.cpMin = 0;
        fr.chrg.cpMax = -1;
        //Use the same DC for measuring and rendering point at printer hDC        
        fr.hdc = fr.hdcTarget = iptHdc;

        //Indicate the area on page to print
        fr.rc = rcToPrint;

        //Get the pointer to the FORMATRANGE structure in memory
        iptParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(fr));
        Marshal.StructureToPtr(fr, iptParam, false);

        //Send the rendered data for printing 
        iptRes = SendMessage(this.Handle, EM_FORMATRANGE, 1, iptParam.ToInt32());

        //Free the block of memory allocated
        Marshal.FreeCoTaskMem(iptParam);

        //Release the device context handle obtained by a previous call
        g.ReleaseHdc(iptHdc);

        return image;
    }
}