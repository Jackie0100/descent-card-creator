﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using DescentCardGraphics.Properties;

namespace DescentCardGraphics
{
    public class Monster : Card
    {
        public string MonsterImage { get; set; }
        public AttackTypes AttackType { get; set; }
        public int SpeedN { get; set; }
        public int WoundN { get; set; }
        public int ArmorN { get; set; }
        public int SpeedM { get; set; }
        public int WoundM { get; set; }
        public int ArmorM { get; set; }
        public List<DiceType> DiceN { get; set; }
        public List<DiceType> DiceM { get; set; }
        public int PlayerCount { get; set; }
        public MonsterLevel MonsterLevel { get; set; }
        public MonsterType MonsterType { get; set; }
        public int bSpeedN { get; set; }
        public int bWoundN { get; set; }
        public int bArmorN { get; set; }
        public int bSpeedM { get; set; }
        public int bWoundM { get; set; }
        public int bArmorM { get; set; }
        public List<DiceType> bDiceN { get; set; }
        public List<DiceType> bDiceM { get; set; }
        public int bPlayerCount { get; set; }
        public MonsterLevel bMonsterLevel { get; set; }


        public Monster() { MonsterImage = ""; DiceN = new List<DiceType>(); DiceM = new List<DiceType>(); bDiceN = new List<DiceType>(); bDiceM = new List<DiceType>(); Text = "\n\n\n"; }

        public Monster(string _name, string _text, int _count, string _image, AttackTypes _attacktype, int _speedn, int _woundn, int _armorn,
            int _speedm, int _woundm, int _armorm, string _dicen, string _dicem, int _players, MonsterLevel _level, MonsterType _type, int _bspeedn,
            int _bwoundn, int _barmorn, int _bspeedm, int _bwoundm, int _barmorm, string _bdicen, string _bdicem, int _bplayers, MonsterLevel _blevel)
            : base(_name, _text, _count)
        {
            MonsterImage = _image;
            AttackType = _attacktype;
            SpeedN = _speedn;
            WoundN = _woundn;
            ArmorN = _armorn;
            SpeedM = _speedm;
            WoundM = _woundm;
            ArmorM = _armorm;
            if (_dicen != "")
            {
                foreach (string s in _dicen.Split(';'))
                {
                    DiceN.Add((DiceType)Enum.Parse(typeof(DiceType), s));
                }
            }
            if (_dicem != "")
            {
                foreach (string s in _dicem.Split(';'))
                {
                    DiceM.Add((DiceType)Enum.Parse(typeof(DiceType), s));
                }
            }
            if (_bdicen != "")
            {
                foreach (string s in _bdicen.Split(';'))
                {
                    bDiceN.Add((DiceType)Enum.Parse(typeof(DiceType), s));
                }
            }
            if (_bdicem != "")
            {
                foreach (string s in _bdicem.Split(';'))
                {
                    bDiceM.Add((DiceType)Enum.Parse(typeof(DiceType), s));
                }
            }
            PlayerCount = _players;
            MonsterLevel = _level;
            MonsterType = _type;
            bSpeedN = _bspeedn;
            bWoundN = _bwoundn;
            bArmorN = _armorn;
            bSpeedM = _bspeedm;
            bWoundM = _bwoundm;
            bArmorM = _barmorm;
            bPlayerCount = _bplayers;
            bMonsterLevel = _blevel;
        }

        //public Monster(string _name, string _text, int _count, string _image, AttackTypes _attacktype, int _speedn, int _woundn, int _armorn,
        //    int _speedm, int _woundm, int _armorm, List<DiceType> _dicen, List<DiceType> _dicem, int _players, MonsterLevel _level, MonsterType _type)
        //    : base(_name, _text, _count)
        //{
        //    MonsterImage = _image;
        //    AttackType = _attacktype;
        //    SpeedN = _speedn;
        //    WoundN = _woundn;
        //    ArmorN = _armorn;
        //    SpeedM = _speedm;
        //    WoundM = _woundm;
        //    ArmorM = _armorm;
        //    DiceN = _dicen;
        //    DiceM = _dicem;
        //    PlayerCount = _players;
        //    MonsterLevel = _level;
        //    MonsterType = _type;
        //}

        public override Image DrawCard()
        {
            Bitmap bitmap = new Bitmap(Resources.Monster_Reference);
            bitmap.SetResolution(600, 600);
            using (Graphics tmp = Graphics.FromImage(bitmap))
            {
                if (Symbol != null && Symbol != "")
                {
                    try
                    {
                        tmp.DrawSymbol(Image.FromFile(Symbol), new RectangleF(bitmap.Width / 1.178f, bitmap.Height - (bitmap.Height / 1.054f), tmp.DpiX / 7.5f, tmp.DpiY / 7.5f));
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                    }
                }
                try
                {
                    tmp.DrawString(Name, mainfont, mainbrush, new RectangleF((bitmap.Width / 5), (bitmap.Height / 16), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 12), strformat);
                    if (MonsterLevel == DescentCardGraphics.MonsterLevel.None)
                    {
                        tmp.DrawString(PlayerCount.ToString(), MonsterNumfont, mainbrush, new RectangleF((bitmap.Width / 20.5f), (bitmap.Height / 12.5f), (bitmap.Width / 8), bitmap.Height / 12), strformat);
                    }
                    else if (MonsterLevel == DescentCardGraphics.MonsterLevel.Copper)
                    {
                        tmp.DrawImage(Resources.Level_Copper, new RectangleF((bitmap.Width / 20.5f), (bitmap.Height / 12.5f), (bitmap.Width / 8), bitmap.Height / 12));
                    }
                    else if (MonsterLevel == DescentCardGraphics.MonsterLevel.Silver)
                    {
                        tmp.DrawImage(Resources.Level_Silver, new RectangleF((bitmap.Width / 20.5f), (bitmap.Height / 12.5f), (bitmap.Width / 8), bitmap.Height / 12));
                    }
                    else if (MonsterLevel == DescentCardGraphics.MonsterLevel.Gold)
                    {
                        tmp.DrawImage(Resources.Level_Gold, new RectangleF((bitmap.Width / 20.5f), (bitmap.Height / 12.5f), (bitmap.Width / 8), bitmap.Height / 12));
                    }
                    else if (MonsterLevel == DescentCardGraphics.MonsterLevel.Diamond)
                    {
                        tmp.DrawImage(Resources.Level_Diamond, new RectangleF((bitmap.Width / 20f), (bitmap.Height / 26f), (bitmap.Width / 3), bitmap.Height / 5));
                    }

                    tmp.DrawString(SpeedN.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 5.2f), (bitmap.Height / 1.6f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(WoundN.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 2f), (bitmap.Height / 1.6f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(ArmorN.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 1.28f), (bitmap.Height / 1.6f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(SpeedM.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 5.2f), (bitmap.Height / 1.25f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(WoundM.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 2f), (bitmap.Height / 1.25f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(ArmorM.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 1.28f), (bitmap.Height / 1.25f), (((bitmap.Width / 8))), bitmap.Height / 12));

                    tmp.DrawImage(Image.FromFile(MonsterImage), new RectangleF((bitmap.Width / 16f), (bitmap.Height / 5.8f), (bitmap.Width - ((bitmap.Width / 16f) * 2)), (bitmap.Height / 2.35f)));
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }
                if (AttackType == AttackTypes.Figthing)
                {
                    tmp.DrawImage(Resources.AttackIcon_melee, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_melee, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }
                else if (AttackType == AttackTypes.Subterfuge)
                {
                    tmp.DrawImage(Resources.AttackIcon_Ranged, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Ranged, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }
                else if (AttackType == AttackTypes.Wizardary)
                {
                    tmp.DrawImage(Resources.AttackIcon_Magic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Magic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));

                }
                else if (AttackType == AttackTypes.Morph)
                {
                    tmp.DrawImage(Resources.AttackIcon_Morph, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Morph, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }
                else if (AttackType == AttackTypes.Psionic)
                {
                    tmp.DrawImage(Resources.AttackIcon_Psionic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Psionic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }

                #region Dice
                float offset = 0;
                try
                {
                    foreach (DiceType d in DiceN)
                    {
                        if (d == DiceType.Red)
                        {
                            tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Blue)
                        {
                            tmp.DrawImage(Resources.Die_Blue, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.White)
                        {
                            tmp.DrawImage(Resources.Die_White, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Purple)
                        {
                            tmp.DrawImage(Resources.Die_Purple, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Morph)
                        {
                            tmp.DrawImage(Resources.Die_Morph, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Green)
                        {
                            tmp.DrawImage(Resources.Die_Green, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Yellow)
                        {
                            tmp.DrawImage(Resources.Die_Yellow, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Black)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Silver)
                        {
                            tmp.DrawImage(Resources.Die_Silver, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Gold)
                        {
                            tmp.DrawImage(Resources.Die_Gold, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        offset += ((bitmap.Width / 16) * 1.12f);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                #endregion

                #region Dice
                offset = 0;
                try
                {
                    foreach (DiceType d in DiceM)
                    {
                        if (d == DiceType.Red)
                        {
                            tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Blue)
                        {
                            tmp.DrawImage(Resources.Die_Blue, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.White)
                        {
                            tmp.DrawImage(Resources.Die_White, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Morph)
                        {
                            tmp.DrawImage(Resources.Die_Morph, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Green)
                        {
                            tmp.DrawImage(Resources.Die_Green, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Yellow)
                        {
                            tmp.DrawImage(Resources.Die_Yellow, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Black)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Silver)
                        {
                            tmp.DrawImage(Resources.Die_Silver, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Gold)
                        {
                            tmp.DrawImage(Resources.Die_Gold, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        offset += ((bitmap.Width / 16) * 1.12f);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                #endregion

                try
                {
                    string[] s = Text.Split('\n');

                    tmp.DrawString(s[0], MonsterTextfont, mainbrush, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.365f), (bitmap.Width / 1.4f), (bitmap.Height / 22)));
                    tmp.DrawString(s[1], MonsterTextfont, mainbrush, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.104f), (bitmap.Width / 1.4f), (bitmap.Height / 22)));
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }
            }
            return bitmap as Image;
        }

        public override Image GetCardBack()
        {
            Bitmap bitmap = new Bitmap(Resources.Monster_Reference);
            bitmap.SetResolution(600, 600);
            using (Graphics tmp = Graphics.FromImage(bitmap))
            {
                if (Symbol != null)
                {
                    tmp.DrawSymbol(Image.FromFile(Symbol), new RectangleF(bitmap.Width / 1.178f, bitmap.Height - (bitmap.Height / 1.054f), tmp.DpiX / 7.5f, tmp.DpiY / 7.5f));
                }
                try
                {
                    tmp.DrawString(Name, mainfont, mainbrush, new RectangleF((bitmap.Width / 5), (bitmap.Height / 16), (bitmap.Width - ((bitmap.Width / 5) * 2)), bitmap.Height / 12), strformat);
                    tmp.DrawString(bPlayerCount.ToString(), MonsterNumfont, mainbrush, new RectangleF((bitmap.Width / 20.5f), (bitmap.Height / 12.5f), (((bitmap.Width / 8))), bitmap.Height / 12), strformat);

                    tmp.DrawString(bSpeedN.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 5.2f), (bitmap.Height / 1.6f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(bWoundN.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 2f), (bitmap.Height / 1.6f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(bArmorN.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 1.28f), (bitmap.Height / 1.6f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(bSpeedM.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 5.2f), (bitmap.Height / 1.25f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(bWoundM.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 2f), (bitmap.Height / 1.25f), (((bitmap.Width / 8))), bitmap.Height / 12));
                    tmp.DrawString(bArmorM.ToString(), NumberMainfont, mainbrush, new RectangleF((bitmap.Width / 1.28f), (bitmap.Height / 1.25f), (((bitmap.Width / 8))), bitmap.Height / 12));

                    tmp.DrawImage(Image.FromFile(MonsterImage), new RectangleF((bitmap.Width / 16f), (bitmap.Height / 5.8f), (bitmap.Width - ((bitmap.Width / 16f) * 2)), (bitmap.Height / 2.35f)));
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }
                if (AttackType == AttackTypes.Figthing)
                {
                    tmp.DrawImage(Resources.AttackIcon_melee, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_melee, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }
                else if (AttackType == AttackTypes.Subterfuge)
                {
                    tmp.DrawImage(Resources.AttackIcon_Ranged, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Ranged, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }
                else if (AttackType == AttackTypes.Wizardary)
                {
                    tmp.DrawImage(Resources.AttackIcon_Magic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Magic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));

                }
                else if (AttackType == AttackTypes.Morph)
                {
                    tmp.DrawImage(Resources.AttackIcon_Morph, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Morph, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }
                else if (AttackType == AttackTypes.Psionic)
                {
                    tmp.DrawImage(Resources.AttackIcon_Psionic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.484f), (bitmap.Width / 14), (bitmap.Height / 22)));
                    tmp.DrawImage(Resources.AttackIcon_Psionic, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.178f), (bitmap.Width / 14), (bitmap.Height / 22)));
                }

                #region Dice
                float offset = 0;
                try
                {
                    foreach (DiceType d in bDiceN)
                    {
                        if (d == DiceType.Red)
                        {
                            tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Blue)
                        {
                            tmp.DrawImage(Resources.Die_Blue, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.White)
                        {
                            tmp.DrawImage(Resources.Die_White, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Morph)
                        {
                            tmp.DrawImage(Resources.Die_Morph, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Green)
                        {
                            tmp.DrawImage(Resources.Die_Green, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Yellow)
                        {
                            tmp.DrawImage(Resources.Die_Yellow, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Black)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Silver)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Gold)
                        {
                            tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.47f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        offset += ((bitmap.Width / 16) * 1.12f);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                #endregion

                #region Dice
                offset = 0;
                try
                {
                    foreach (DiceType d in bDiceM)
                    {
                        if (d == DiceType.Red)
                        {
                            tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Blue)
                        {
                            tmp.DrawImage(Resources.Die_Blue, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.White)
                        {
                            tmp.DrawImage(Resources.Die_White, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Morph)
                        {
                            tmp.DrawImage(Resources.Die_Morph, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Green)
                        {
                            tmp.DrawImage(Resources.Die_Green, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Yellow)
                        {
                            tmp.DrawImage(Resources.Die_Yellow, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Black)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Silver)
                        {
                            tmp.DrawImage(Resources.Die_Black, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        else if (d == DiceType.Gold)
                        {
                            tmp.DrawImage(Resources.Die_Red, new RectangleF((bitmap.Width / 5f) + offset, (bitmap.Height / 1.17f), (bitmap.Width / 16), (bitmap.Height / 26)));
                        }
                        offset += ((bitmap.Width / 16) * 1.12f);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                #endregion

                try
                {
                    string[] s = Text.Split('\n');

                    tmp.DrawString(s[2], MonsterTextfont, mainbrush, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.365f), (bitmap.Width / 1.4f), (bitmap.Height / 22)));
                    tmp.DrawString(s[3], MonsterTextfont, mainbrush, new RectangleF((bitmap.Width / 10f), (bitmap.Height / 1.104f), (bitmap.Width / 1.4f), (bitmap.Height / 22)));
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }
            }
            return bitmap as Image;
        }

        public override string ToString()
        {
            string strn = "";
            string strm = "";
            string bstrn = "";
            string bstrm = "";
            try
            {

                foreach (DiceType d in DiceN)
                {
                    strn += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                foreach (DiceType d in DiceM)
                {
                    strm += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                foreach (DiceType d in bDiceN)
                {
                    bstrn += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                foreach (DiceType d in bDiceM)
                {
                    bstrm += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return "Monster"  + "\n" + Name + "\n" + Text + "\n" + Count + "\n" + MonsterImage + "\n" + AttackType + "\n" + SpeedN + "\n" + WoundN + "\n" + ArmorN + "\n" + SpeedM + "\n" + WoundM + "\n" + ArmorM + "\n" + strn + "\n" + strm + "\n" + PlayerCount + "\n" + MonsterLevel + "\n" + MonsterType + "\n" + bSpeedN + "\n" + bWoundN + "\n" + bArmorN + "\n" + bSpeedM + "\n" + bWoundM + "\n" + bArmorM + "\n" + bstrn + "\n" + bstrm + "\n" + bPlayerCount + "\n" + bMonsterLevel;
        }

        public override void Save(System.IO.BinaryWriter br)
        {
            string strn = "";
            string strm = "";
            string bstrn = "";
            string bstrm = "";
            try
            {

                foreach (DiceType d in DiceN)
                {
                    strn += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                foreach (DiceType d in DiceM)
                {
                    strm += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                foreach (DiceType d in bDiceN)
                {
                    bstrn += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                foreach (DiceType d in bDiceM)
                {
                    bstrm += d.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            br.Write("Monster");
            br.Write(Name);
            br.Write(Text);
            br.Write(Count);
            br.Write(MonsterImage);
            br.Write(AttackType.ToString());
            br.Write(SpeedN);
            br.Write(WoundN);
            br.Write(ArmorN);
            br.Write(SpeedM);
            br.Write(WoundM);
            br.Write(ArmorM);
            br.Write(strn);
            br.Write(strm);
            br.Write(PlayerCount);
            br.Write(MonsterLevel.ToString());
            br.Write(bSpeedN);
            br.Write(bWoundN);
            br.Write(bArmorN);
            br.Write(bSpeedM);
            br.Write(bWoundM);
            br.Write(bArmorM);
            br.Write(bstrn);
            br.Write(bstrm);
            br.Write(bPlayerCount);
            br.Write(bMonsterLevel.ToString());
        }
    }
}
