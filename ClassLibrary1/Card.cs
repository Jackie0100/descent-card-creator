﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace DescentCardGraphics
{
    [System.Xml.Serialization.XmlInclude(typeof(FeatSkill))]
    [System.Xml.Serialization.XmlInclude(typeof(HeroSheet))]
    [System.Xml.Serialization.XmlInclude(typeof(Item))]
    [System.Xml.Serialization.XmlInclude(typeof(DarkRelic))]
    [System.Xml.Serialization.XmlInclude(typeof(Monster))]
    [System.Xml.Serialization.XmlInclude(typeof(OverLord))]
    [System.Xml.Serialization.XmlInclude(typeof(Treachery))]
    public abstract class Card
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public int Count { get; set; }
        public string Symbol { get; set; }

        protected readonly Font overlordnumberfont = new Font("KelmScott", 13, FontStyle.Regular);
        protected readonly Font MonsterNumfont = new Font("CelticHand", 18, FontStyle.Regular);
        protected readonly Font HeroNumfont = new Font("CelticHand", 16, FontStyle.Regular);
        protected readonly Font NumberMainfont = new Font("CelticHand", 11, FontStyle.Regular);
        protected readonly Font mainfont = new Font("KelmScott", 10, FontStyle.Regular);
        protected readonly Font overlordtypefont = new Font("KelmScott", 8, FontStyle.Regular);
        protected readonly Font typefont = new Font("KelmScott", 6, FontStyle.Regular);
        protected readonly Font MonsterTextfont = new Font("Times New Roman", 10, FontStyle.Bold | FontStyle.Italic);
        protected readonly StringFormat strformat = new StringFormat();
        protected readonly Brush mainbrush = Brushes.Black;
        protected readonly Brush whitebrush = Brushes.White;

        public Card()
        {
            Name = "";
            Text = "";
            Count = 0;
            strformat.Alignment = StringAlignment.Center;
            strformat.LineAlignment = StringAlignment.Center;
        }

        public Card(string _name, string _text, int _count)
        {
            Name = _name;
            Text = _text;
            Count = _count;
            strformat.Alignment = StringAlignment.Center;
            strformat.LineAlignment = StringAlignment.Center;
        }

        public abstract Image DrawCard();
        public virtual Image GetCardBack()
        {
            return null;
        }
        public abstract override string ToString();
        public abstract void Save(System.IO.BinaryWriter br);
    }
}
