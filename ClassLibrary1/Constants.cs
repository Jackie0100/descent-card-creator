﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace DescentCardGraphics
{
    public enum Hands
    {
        None, Hands1, Hands2,
    }

    public enum MonsterLevel
    {
        None, Copper, Silver, Gold, Diamond,
    }

    public enum MonsterType
    {
        None, Humanoid, Beast, Eldritch,
    }

    public enum DiceType
    {
        Red, Blue, White, Purple, Morph, Green, Yellow, Black, Silver, Gold,
    }

    public enum ItemType
    {
        ShopItem, Artifact, DarkRelic, PartyUpgrade, CopperTreasure, SilverTreasure, GoldTreasure,
    }

    public enum OverLoadCardType
    {
        Normal, Spawn, Event, Trap, TreacherySpawn, TreacheryEvent, TreacheryTrap, 
    }

    public enum FeatSkillTypes
    {
        FeatFigthing, FeatSubterfuge, FeatWizardary, SkillFigthing, SkillSubterfuge, SkillWizardary,
    }

    public enum AttackTypes
    {
        Figthing, Subterfuge, Wizardary, Morph, Psionic,
    }
    
    public static class DatabasesHandler
    {
        //public static Image ItemFront;
        //public static Image ArtifactFront;
        //public static Image DarkRelicFront;
        //public static Image PartyUpgadeFront;
        //public static Image TreasureCopperBack;
        //public static Image TreasureSilverBack;
        //public static Image TreasureGoldBack;
        //public static Image DarkRelicBack;

        //public static Image OverLordFront;
        //public static Image OverLordSpawnFront;
        //public static Image OverLordEventFront;
        //public static Image OverLordTrapFront;
        //public static Image TreacherySpawn;
        //public static Image TreacheryEvent;
        //public static Image TreacheryTrap;
        //public static Image OverLordBack;

        //public static Image SkillFront;
        //public static Image SkillFightingFront;
        //public static Image SkillSubterfugeFront;
        //public static Image SkillWizardaryFront;
        //public static Image SkillFightingBack;
        //public static Image SkillSubterfugeBack;
        //public static Image SkillWizardaryBack;

        //public static Image FeatFightingFront;
        //public static Image FeatSubterfugeFront;
        //public static Image FeatWizardaryFront;
        //public static Image FeatFightingBack;
        //public static Image FeatSubterfugeBack;
        //public static Image FeatWizardaryBack;

        //public static Image MonsterReferenceFront;
        //public static Image MonsterCopperReferenceFront;
        //public static Image MonsterSilverReferenceFront;
        //public static Image MonsterGoldReferenceFront;
        //public static Image MonsterDiamondReferenceFront;

        //public static Image RedDie;
        //public static Image BlueDie;
        //public static Image WhiteDie;
        //public static Image GreenDie;
        //public static Image YellowDie;
        //public static Image MorphDie;
        //public static Image BlackDie;
        //public static Image SilverDie;
        //public static Image GoldDie;
        //public static Image StealthDie;

        //public static Image Hands1;
        //public static Image Hands2;
        //public static Image Coin;
        //public static Image MeleeAttackIcon;
        //public static Image RangedAttackIcon;
        //public static Image MagicAttackIcon;
        //public static Image SurgeIcon;

        //public static Image HeroSheet;

        public static List<DataBase> Cards = new List<DataBase>();
    }
}