﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Xml;
using System.Xml.Serialization;
using System.Drawing.Printing;

namespace DescentCardGraphics
{
    public sealed class DataFileHandler
    {
        public static void LoadCreatorSet()
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Jacks Descent Card Creator File |*.dcc;";
            ofd.Title = "select a Jacks Descent Card Creator File";
            if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            foreach (string s in ofd.FileNames)
            {
                FileStream fs = new FileStream(s, FileMode.Open);
                XmlSerializer xs = new XmlSerializer(typeof(DataBase));
                object o = xs.Deserialize(fs);
                DatabasesHandler.Cards.Add((DataBase)o);
                fs.Close();
                fs.Dispose();
                fs = null;
            }
        }

        public static void LoadCreatorSet(string export)
        {
            FileStream fs = new FileStream(export, FileMode.Open);
            XmlSerializer xs = new XmlSerializer(typeof(DataBase));
            object o = xs.Deserialize(fs);
            DatabasesHandler.Cards.Add((DataBase)o);
            ExportAllImages(export);
            fs.Close();
            fs.Dispose();
            fs = null;
        }

        public static void SaveCreatorSet()
        {
            System.Windows.Forms.FolderBrowserDialog ofd = new System.Windows.Forms.FolderBrowserDialog();
            ofd.Description = "Choose a folder too save all currently oppened databases to:";
            if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            foreach (DataBase db in DatabasesHandler.Cards)
            {
                List<Card> c = new List<Card>();
                FileStream fs = new FileStream(ofd.SelectedPath + "\\" + db.SetName + ".dcc", FileMode.OpenOrCreate);
                XmlSerializer xs = new XmlSerializer(db.GetType());
                xs.Serialize(fs, db);
                fs.Close();
                fs.Dispose();
                fs = null;
            }
        }

        private static void MakeHeroImage(List<Card> images, string path)
        {
            Bitmap bitmapfront = new Bitmap(Properties.Resources.HeroSheet.Width, Properties.Resources.HeroSheet.Height * 2 + Properties.Resources.HeroSheet.Height / 60);

            bitmapfront.SetResolution(600, 600);

            using (Graphics tmpfront = Graphics.FromImage(bitmapfront))
            {
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 1)
                {
                    using (Image i = images[0].DrawCard())
                    {
                        tmpfront.DrawImage(i, new RectangleF(0, 0, Properties.Resources.HeroSheet.Width, Properties.Resources.HeroSheet.Height));
                        images[0] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 2)
                {
                    using (Image i = images[1].DrawCard())
                    {
                        tmpfront.DrawImage(i, new RectangleF(0, Properties.Resources.HeroSheet.Height + Properties.Resources.HeroSheet.Height / 60, Properties.Resources.HeroSheet.Width, Properties.Resources.HeroSheet.Height));
                        images[1] = null;
                    }
                }
            }
            bitmapfront.Save(path + "_Front.png", System.Drawing.Imaging.ImageFormat.Png);
            bitmapfront.Dispose();
        }

        private static void MakeImageBack(List<Card> images, string path)
        {
            Bitmap bitmapfront = new Bitmap((Properties.Resources.Overlord_Back.Width * 3) + ((Properties.Resources.Overlord_Back.Width / 50) * 2), (Properties.Resources.Overlord_Back.Height * 3) + ((Properties.Resources.Overlord_Back.Height / 50) * 2));

            bitmapfront.SetResolution(600, 600);

            using (Graphics tmpfront = Graphics.FromImage(bitmapfront))
            {
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 1)
                {
                    using (Image i = images[0].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF((Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50)) * 2, 0, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));                        
                        images[0] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 2)
                {
                    using (Image i = images[1].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF(Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50), 0, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[1] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 3)
                {
                    using (Image i = images[2].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF(0, 0, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[2] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 4)
                {
                    using (Image i = images[3].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF((Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50)) * 2, Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50), Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[3] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 5)
                {
                    using (Image i = images[4].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF(Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50), Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50), Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[4] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 6)
                {
                    using (Image i = images[5].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF(0, Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50), Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[5] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 7)
                {
                    using (Image i = images[6].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF((Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50)) * 2, (Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50)) * 2, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[6] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 8)
                {
                    using (Image i = images[7].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF(Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50), (Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50)) * 2, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[7] = null;
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                if (images.Count >= 9)
                {
                    using (Image i = images[8].GetCardBack())
                    {
                        tmpfront.DrawImage(i, new RectangleF(0, (Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50)) * 2, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                        images[8] = null;
                    }
                }
            }
            GC.Collect(2, GCCollectionMode.Forced);
            bitmapfront.Save(path + "_Back.png", System.Drawing.Imaging.ImageFormat.Png);
            bitmapfront.Dispose();
            GC.Collect(2, GCCollectionMode.Forced);
        }
        
        private static void MakeImage(List<Card> images, string path)
        {
            Bitmap bitmapfront = new Bitmap((Properties.Resources.Overlord_Back.Width * 3) + ((Properties.Resources.Overlord_Back.Width / 50) * 2), (Properties.Resources.Overlord_Back.Height * 3) + ((Properties.Resources.Overlord_Back.Height / 50) * 2));
            
            bitmapfront.SetResolution(600, 600);

                using (Graphics tmpfront = Graphics.FromImage(bitmapfront))
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 1)
                    {
                        using (Image i = images[0].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF(0, 0, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[0] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 2)
                    {
                        using (Image i = images[1].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF(Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50), 0, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[1] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 3)
                    {
                        using (Image i = images[2].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF((Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50)) * 2, 0, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[2] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 4)
                    {
                        using (Image i = images[3].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF(0, Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50), Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[3] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 5)
                    {
                        using (Image i = images[4].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF(Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50), Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50), Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[4] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 6)
                    {
                        using (Image i = images[5].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF((Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50)) * 2, Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50), Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[5] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 7)
                    {
                        using (Image i = images[6].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF(0, (Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50)) * 2, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[6] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 8)
                    {
                        using (Image i = images[7].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF(Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50), (Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50)) * 2, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[7] = null;
                        }
                    }
                    GC.Collect(2, GCCollectionMode.Forced);
                    if (images.Count >= 9)
                    {
                        using (Image i = images[8].DrawCard())
                        {
                            tmpfront.DrawImage(i, new RectangleF((Properties.Resources.Overlord_Back.Width + (Properties.Resources.Overlord_Back.Width / 50)) * 2, (Properties.Resources.Overlord_Back.Height + (Properties.Resources.Overlord_Back.Height / 50)) * 2, Properties.Resources.Overlord_Back.Width, Properties.Resources.Overlord_Back.Height));
                            images[8] = null;
                        }
                    }
                }
                GC.Collect(2, GCCollectionMode.Forced);
                bitmapfront.Save(path + "_Front.png", System.Drawing.Imaging.ImageFormat.Png);
                bitmapfront.Dispose();
                GC.Collect(2, GCCollectionMode.Forced);
        }

        public static void ExportAllImages(string db)
        {
            List<Card> localcards = new List<Card>();
            Console.WriteLine("Exporting FeatSkill to png...");
            foreach (Card c in DatabasesHandler.Cards.Find(s => s.SetName == Path.GetFileNameWithoutExtension(db)).SetCards.Where(s => s is FeatSkill).ToList())
            {
                for (int i = 0; i < c.Count; i++)
                {
                    localcards.Add(c);
                }
            }
            int count = 0;
            for (count = 0; count < (int)(localcards.Count / 9); count++)
            {
                MakeImage(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Feat_" + count);
                MakeImageBack(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Feat_" + count);
            }
            if (localcards.Count % 9 != 0)
            {
                MakeImage(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Feat_" + count);
                MakeImageBack(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Feat_" + count);
            }

            Console.WriteLine("Exporting Items to png...");
            localcards = new List<Card>();
            foreach (Card c in DatabasesHandler.Cards.Find(s => s.SetName == Path.GetFileNameWithoutExtension(db)).SetCards.Where(s => s is Item || s is DarkRelic).ToList())
            {
                for (int i = 0; i < c.Count; i++)
                {
                    localcards.Add(c);
                }
            }
            count = 0;
            for (count = 0; count < (int)(localcards.Count / 9); count++)
            {
                MakeImage(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Item_" + count);
                MakeImageBack(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Item_" + count);
            }
            if (localcards.Count % 9 != 0)
            {
                MakeImage(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Item_" + count);
                MakeImageBack(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Item_" + count);
            }

            Console.WriteLine("Exporting Overlord to png...");
            localcards = new List<Card>();
            foreach (Card c in DatabasesHandler.Cards.Find(s => s.SetName == Path.GetFileNameWithoutExtension(db)).SetCards.Where(s => s is OverLord || s is Treachery).ToList())
            {
                for (int i = 0; i < c.Count; i++)
                {
                    localcards.Add(c);
                }
            }
            count = 0;
            for (count = 0; count < (int)(localcards.Count / 9); count++)
            {
                MakeImage(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Overlord_" + count);
                MakeImageBack(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Overlord_" + count);
            }
            if (localcards.Count % 9 != 0)
            {
                MakeImage(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Overlord_" + count);
                MakeImageBack(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Overlord_" + count);
            }

            Console.WriteLine("Exporting Monsters to png...");
            localcards = new List<Card>();
            foreach (Card c in DatabasesHandler.Cards.Find(s => s.SetName == Path.GetFileNameWithoutExtension(db)).SetCards.Where(s => s is Monster).ToList())
            {
                for (int i = 0; i < c.Count; i++)
                {
                    localcards.Add(c);
                }
            }
            count = 0;
            for (count = 0; count < (int)(localcards.Count / 9); count++)
            {
                MakeImage(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Monster_" + count);
                MakeImageBack(localcards.GetRange(count * 9, 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Monster_" + count);
            }
            if (localcards.Count % 9 != 0)
            {
                MakeImage(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Monster_" + count);
                MakeImageBack(localcards.GetRange(count * 9, localcards.Count % 9), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_Monster_" + count);
            }

            Console.WriteLine("Exporting Herosheet to png...");
            localcards = new List<Card>();
            foreach (Card c in DatabasesHandler.Cards.Find(s => s.SetName == Path.GetFileNameWithoutExtension(db)).SetCards.Where(s => s is HeroSheet).ToList())
            {
                for (int i = 0; i < c.Count; i++)
                {
                    localcards.Add(c);
                }
            }
            count = 0;
            for (count = 0; count < (int)(localcards.Count / 2); count++)
            {
                MakeHeroImage(localcards.GetRange(count * 2, 2), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_HeroSheet_" + count);
            }
            if (localcards.Count % 9 != 0)
            {
                MakeHeroImage(localcards.GetRange(count * 2, localcards.Count % 2), Path.GetDirectoryName(db) + "\\" + Path.GetFileNameWithoutExtension(db) + "_HeroSheet_" + count);
            }
        }

        public static void ExportSingleImage(Image image)
        {
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.Filter = "Save Image | *.png";
            sfd.Title = "select a place to save image";
            sfd.ShowDialog();
            if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Png);
        }
    }
}
