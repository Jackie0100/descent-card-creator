﻿namespace DescentCardCreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label_Name = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label_Icon = new System.Windows.Forms.Label();
            this.label_Picture = new System.Windows.Forms.Label();
            this.textBox_Icon = new System.Windows.Forms.TextBox();
            this.button_Browse_Icon = new System.Windows.Forms.Button();
            this.button_Browse_Picture = new System.Windows.Forms.Button();
            this.textBox_Picture = new System.Windows.Forms.TextBox();
            this.label_Item_Type = new System.Windows.Forms.Label();
            this.checkBox_1Hand = new System.Windows.Forms.CheckBox();
            this.checkBox_2Hand = new System.Windows.Forms.CheckBox();
            this.numericUpDown_Coins = new System.Windows.Forms.NumericUpDown();
            this.label_Gold = new System.Windows.Forms.Label();
            this.comboBox_Item_Type = new System.Windows.Forms.ComboBox();
            this.comboBox_Attack_Type = new System.Windows.Forms.ComboBox();
            this.label_Attack_Type = new System.Windows.Forms.Label();
            this.pictureBox_Card_Back = new System.Windows.Forms.PictureBox();
            this.pictureBox_Card_Front = new System.Windows.Forms.PictureBox();
            this.numericUpDown_Red_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Blue_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_White_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Green_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Yellow_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Black_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Gold_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Silver_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Morph_Die = new System.Windows.Forms.NumericUpDown();
            this.pictureBox_Red_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Blue_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_White_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Green_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Yellow_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Black_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Silver_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Gold_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Morph_Die = new System.Windows.Forms.PictureBox();
            this.label_Item_Special = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.numericUpDown_Font_Size = new System.Windows.Forms.NumericUpDown();
            this.richTextBoxPrint = new RichTextBoxPrint();
            this.richTextBoxPrint1 = new RichTextBoxPrint();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Coins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Card_Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Card_Front)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Red_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Blue_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_White_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Green_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Yellow_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Black_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Gold_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Silver_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Morph_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Red_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Blue_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_White_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Green_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Yellow_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Black_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Silver_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Gold_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Morph_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Font_Size)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.Location = new System.Drawing.Point(8, 31);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(51, 20);
            this.label_Name.TabIndex = 13;
            this.label_Name.Text = "Name";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(925, 24);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filesToolStripMenuItem
            // 
            this.filesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.filesToolStripMenuItem.Name = "filesToolStripMenuItem";
            this.filesToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.filesToolStripMenuItem.Text = "Files";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem.Text = "Export Image";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // label_Icon
            // 
            this.label_Icon.AutoSize = true;
            this.label_Icon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Icon.Location = new System.Drawing.Point(12, 104);
            this.label_Icon.Name = "label_Icon";
            this.label_Icon.Size = new System.Drawing.Size(40, 20);
            this.label_Icon.TabIndex = 15;
            this.label_Icon.Text = "Icon";
            // 
            // label_Picture
            // 
            this.label_Picture.AutoSize = true;
            this.label_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Picture.Location = new System.Drawing.Point(12, 156);
            this.label_Picture.Name = "label_Picture";
            this.label_Picture.Size = new System.Drawing.Size(58, 20);
            this.label_Picture.TabIndex = 16;
            this.label_Picture.Text = "Picture";
            // 
            // textBox_Icon
            // 
            this.textBox_Icon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Icon.Location = new System.Drawing.Point(12, 127);
            this.textBox_Icon.Name = "textBox_Icon";
            this.textBox_Icon.Size = new System.Drawing.Size(206, 26);
            this.textBox_Icon.TabIndex = 17;
            // 
            // button_Browse_Icon
            // 
            this.button_Browse_Icon.Location = new System.Drawing.Point(223, 130);
            this.button_Browse_Icon.Name = "button_Browse_Icon";
            this.button_Browse_Icon.Size = new System.Drawing.Size(75, 23);
            this.button_Browse_Icon.TabIndex = 18;
            this.button_Browse_Icon.Text = "Browse";
            this.button_Browse_Icon.UseVisualStyleBackColor = true;
            this.button_Browse_Icon.Click += new System.EventHandler(this.button_Browse_Icon_Click);
            // 
            // button_Browse_Picture
            // 
            this.button_Browse_Picture.Location = new System.Drawing.Point(224, 182);
            this.button_Browse_Picture.Name = "button_Browse_Picture";
            this.button_Browse_Picture.Size = new System.Drawing.Size(75, 23);
            this.button_Browse_Picture.TabIndex = 20;
            this.button_Browse_Picture.Text = "Browse";
            this.button_Browse_Picture.UseVisualStyleBackColor = true;
            this.button_Browse_Picture.Click += new System.EventHandler(this.button_Browse_Picture_Click);
            // 
            // textBox_Picture
            // 
            this.textBox_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Picture.Location = new System.Drawing.Point(12, 179);
            this.textBox_Picture.Name = "textBox_Picture";
            this.textBox_Picture.Size = new System.Drawing.Size(206, 26);
            this.textBox_Picture.TabIndex = 19;
            // 
            // label_Item_Type
            // 
            this.label_Item_Type.AutoSize = true;
            this.label_Item_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Item_Type.Location = new System.Drawing.Point(12, 208);
            this.label_Item_Type.Name = "label_Item_Type";
            this.label_Item_Type.Size = new System.Drawing.Size(79, 20);
            this.label_Item_Type.TabIndex = 22;
            this.label_Item_Type.Text = "Item Type";
            // 
            // checkBox_1Hand
            // 
            this.checkBox_1Hand.AutoSize = true;
            this.checkBox_1Hand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_1Hand.Location = new System.Drawing.Point(12, 507);
            this.checkBox_1Hand.Name = "checkBox_1Hand";
            this.checkBox_1Hand.Size = new System.Drawing.Size(80, 24);
            this.checkBox_1Hand.TabIndex = 23;
            this.checkBox_1Hand.Text = "1 Hand";
            this.checkBox_1Hand.UseVisualStyleBackColor = true;
            this.checkBox_1Hand.CheckedChanged += new System.EventHandler(this.checkBox_1Hand_CheckedChanged);
            // 
            // checkBox_2Hand
            // 
            this.checkBox_2Hand.AutoSize = true;
            this.checkBox_2Hand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_2Hand.Location = new System.Drawing.Point(98, 507);
            this.checkBox_2Hand.Name = "checkBox_2Hand";
            this.checkBox_2Hand.Size = new System.Drawing.Size(80, 24);
            this.checkBox_2Hand.TabIndex = 24;
            this.checkBox_2Hand.Text = "2 Hand";
            this.checkBox_2Hand.UseVisualStyleBackColor = true;
            this.checkBox_2Hand.CheckedChanged += new System.EventHandler(this.checkBox_2Hand_CheckedChanged);
            // 
            // numericUpDown_Coins
            // 
            this.numericUpDown_Coins.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Coins.Location = new System.Drawing.Point(184, 506);
            this.numericUpDown_Coins.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Coins.Name = "numericUpDown_Coins";
            this.numericUpDown_Coins.Size = new System.Drawing.Size(44, 26);
            this.numericUpDown_Coins.TabIndex = 25;
            this.numericUpDown_Coins.ValueChanged += new System.EventHandler(this.numericUpDown_Coins_ValueChanged);
            // 
            // label_Gold
            // 
            this.label_Gold.AutoSize = true;
            this.label_Gold.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Gold.Location = new System.Drawing.Point(234, 510);
            this.label_Gold.Name = "label_Gold";
            this.label_Gold.Size = new System.Drawing.Size(49, 20);
            this.label_Gold.TabIndex = 26;
            this.label_Gold.Text = "Coins";
            // 
            // comboBox_Item_Type
            // 
            this.comboBox_Item_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Item_Type.FormattingEnabled = true;
            this.comboBox_Item_Type.Items.AddRange(new object[] {
            "Item Type",
            "Armor",
            "Other",
            "Other - Rune",
            "Shield",
            "Weapon",
            "Weapon - Rune"});
            this.comboBox_Item_Type.Location = new System.Drawing.Point(12, 231);
            this.comboBox_Item_Type.Name = "comboBox_Item_Type";
            this.comboBox_Item_Type.Size = new System.Drawing.Size(140, 28);
            this.comboBox_Item_Type.TabIndex = 27;
            this.comboBox_Item_Type.SelectedIndexChanged += new System.EventHandler(this.comboBox_Item_Type_SelectedIndexChanged);
            this.comboBox_Item_Type.TextChanged += new System.EventHandler(this.comboBox_Item_Type_SelectedIndexChanged);
            // 
            // comboBox_Attack_Type
            // 
            this.comboBox_Attack_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Attack_Type.FormattingEnabled = true;
            this.comboBox_Attack_Type.Items.AddRange(new object[] {
            "Magic",
            "Melee",
            "Ranged"});
            this.comboBox_Attack_Type.Location = new System.Drawing.Point(161, 231);
            this.comboBox_Attack_Type.Name = "comboBox_Attack_Type";
            this.comboBox_Attack_Type.Size = new System.Drawing.Size(140, 28);
            this.comboBox_Attack_Type.TabIndex = 28;
            this.comboBox_Attack_Type.SelectedIndexChanged += new System.EventHandler(this.comboBox_Attack_Type_SelectedIndexChanged);
            this.comboBox_Attack_Type.TextChanged += new System.EventHandler(this.comboBox_Attack_Type_SelectedIndexChanged);
            // 
            // label_Attack_Type
            // 
            this.label_Attack_Type.AutoSize = true;
            this.label_Attack_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Attack_Type.Location = new System.Drawing.Point(154, 208);
            this.label_Attack_Type.Name = "label_Attack_Type";
            this.label_Attack_Type.Size = new System.Drawing.Size(93, 20);
            this.label_Attack_Type.TabIndex = 29;
            this.label_Attack_Type.Text = "Attack Type";
            // 
            // pictureBox_Card_Back
            // 
            this.pictureBox_Card_Back.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Card_Back.Image")));
            this.pictureBox_Card_Back.Location = new System.Drawing.Point(612, 34);
            this.pictureBox_Card_Back.Name = "pictureBox_Card_Back";
            this.pictureBox_Card_Back.Size = new System.Drawing.Size(301, 464);
            this.pictureBox_Card_Back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Card_Back.TabIndex = 11;
            this.pictureBox_Card_Back.TabStop = false;
            // 
            // pictureBox_Card_Front
            // 
            this.pictureBox_Card_Front.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Card_Front.Image")));
            this.pictureBox_Card_Front.Location = new System.Drawing.Point(307, 34);
            this.pictureBox_Card_Front.Name = "pictureBox_Card_Front";
            this.pictureBox_Card_Front.Size = new System.Drawing.Size(301, 464);
            this.pictureBox_Card_Front.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Card_Front.TabIndex = 1;
            this.pictureBox_Card_Front.TabStop = false;
            // 
            // numericUpDown_Red_Die
            // 
            this.numericUpDown_Red_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Red_Die.Location = new System.Drawing.Point(330, 506);
            this.numericUpDown_Red_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Red_Die.Name = "numericUpDown_Red_Die";
            this.numericUpDown_Red_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Red_Die.TabIndex = 31;
            this.numericUpDown_Red_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Blue_Die
            // 
            this.numericUpDown_Blue_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Blue_Die.Location = new System.Drawing.Point(392, 506);
            this.numericUpDown_Blue_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Blue_Die.Name = "numericUpDown_Blue_Die";
            this.numericUpDown_Blue_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Blue_Die.TabIndex = 32;
            this.numericUpDown_Blue_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_White_Die
            // 
            this.numericUpDown_White_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_White_Die.Location = new System.Drawing.Point(454, 506);
            this.numericUpDown_White_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_White_Die.Name = "numericUpDown_White_Die";
            this.numericUpDown_White_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_White_Die.TabIndex = 33;
            this.numericUpDown_White_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Green_Die
            // 
            this.numericUpDown_Green_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Green_Die.Location = new System.Drawing.Point(516, 506);
            this.numericUpDown_Green_Die.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown_Green_Die.Name = "numericUpDown_Green_Die";
            this.numericUpDown_Green_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Green_Die.TabIndex = 34;
            this.numericUpDown_Green_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Yellow_Die
            // 
            this.numericUpDown_Yellow_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Yellow_Die.Location = new System.Drawing.Point(578, 506);
            this.numericUpDown_Yellow_Die.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown_Yellow_Die.Name = "numericUpDown_Yellow_Die";
            this.numericUpDown_Yellow_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Yellow_Die.TabIndex = 35;
            this.numericUpDown_Yellow_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Black_Die
            // 
            this.numericUpDown_Black_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Black_Die.Location = new System.Drawing.Point(702, 506);
            this.numericUpDown_Black_Die.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Black_Die.Name = "numericUpDown_Black_Die";
            this.numericUpDown_Black_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Black_Die.TabIndex = 36;
            this.numericUpDown_Black_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Gold_Die
            // 
            this.numericUpDown_Gold_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Gold_Die.Location = new System.Drawing.Point(826, 506);
            this.numericUpDown_Gold_Die.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Gold_Die.Name = "numericUpDown_Gold_Die";
            this.numericUpDown_Gold_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Gold_Die.TabIndex = 37;
            this.numericUpDown_Gold_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Silver_Die
            // 
            this.numericUpDown_Silver_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Silver_Die.Location = new System.Drawing.Point(764, 506);
            this.numericUpDown_Silver_Die.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Silver_Die.Name = "numericUpDown_Silver_Die";
            this.numericUpDown_Silver_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Silver_Die.TabIndex = 38;
            this.numericUpDown_Silver_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // numericUpDown_Morph_Die
            // 
            this.numericUpDown_Morph_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Morph_Die.Location = new System.Drawing.Point(640, 506);
            this.numericUpDown_Morph_Die.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Morph_Die.Name = "numericUpDown_Morph_Die";
            this.numericUpDown_Morph_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Morph_Die.TabIndex = 39;
            this.numericUpDown_Morph_Die.ValueChanged += new System.EventHandler(this.numericUpDown_Dice_Value_Changed);
            // 
            // pictureBox_Red_Die
            // 
            this.pictureBox_Red_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Red;
            this.pictureBox_Red_Die.Location = new System.Drawing.Point(304, 510);
            this.pictureBox_Red_Die.Name = "pictureBox_Red_Die";
            this.pictureBox_Red_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Red_Die.TabIndex = 40;
            this.pictureBox_Red_Die.TabStop = false;
            // 
            // pictureBox_Blue_Die
            // 
            this.pictureBox_Blue_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Blue;
            this.pictureBox_Blue_Die.Location = new System.Drawing.Point(366, 510);
            this.pictureBox_Blue_Die.Name = "pictureBox_Blue_Die";
            this.pictureBox_Blue_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Blue_Die.TabIndex = 41;
            this.pictureBox_Blue_Die.TabStop = false;
            // 
            // pictureBox_White_Die
            // 
            this.pictureBox_White_Die.Image = global::DescentCardCreator.Properties.Resources.Die_White;
            this.pictureBox_White_Die.Location = new System.Drawing.Point(428, 510);
            this.pictureBox_White_Die.Name = "pictureBox_White_Die";
            this.pictureBox_White_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_White_Die.TabIndex = 42;
            this.pictureBox_White_Die.TabStop = false;
            // 
            // pictureBox_Green_Die
            // 
            this.pictureBox_Green_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Green;
            this.pictureBox_Green_Die.Location = new System.Drawing.Point(490, 510);
            this.pictureBox_Green_Die.Name = "pictureBox_Green_Die";
            this.pictureBox_Green_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Green_Die.TabIndex = 43;
            this.pictureBox_Green_Die.TabStop = false;
            // 
            // pictureBox_Yellow_Die
            // 
            this.pictureBox_Yellow_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Yellow;
            this.pictureBox_Yellow_Die.Location = new System.Drawing.Point(552, 510);
            this.pictureBox_Yellow_Die.Name = "pictureBox_Yellow_Die";
            this.pictureBox_Yellow_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Yellow_Die.TabIndex = 44;
            this.pictureBox_Yellow_Die.TabStop = false;
            // 
            // pictureBox_Black_Die
            // 
            this.pictureBox_Black_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Black;
            this.pictureBox_Black_Die.Location = new System.Drawing.Point(676, 510);
            this.pictureBox_Black_Die.Name = "pictureBox_Black_Die";
            this.pictureBox_Black_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Black_Die.TabIndex = 45;
            this.pictureBox_Black_Die.TabStop = false;
            // 
            // pictureBox_Silver_Die
            // 
            this.pictureBox_Silver_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Red;
            this.pictureBox_Silver_Die.Location = new System.Drawing.Point(738, 510);
            this.pictureBox_Silver_Die.Name = "pictureBox_Silver_Die";
            this.pictureBox_Silver_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Silver_Die.TabIndex = 46;
            this.pictureBox_Silver_Die.TabStop = false;
            // 
            // pictureBox_Gold_Die
            // 
            this.pictureBox_Gold_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Black;
            this.pictureBox_Gold_Die.Location = new System.Drawing.Point(800, 510);
            this.pictureBox_Gold_Die.Name = "pictureBox_Gold_Die";
            this.pictureBox_Gold_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Gold_Die.TabIndex = 47;
            this.pictureBox_Gold_Die.TabStop = false;
            // 
            // pictureBox_Morph_Die
            // 
            this.pictureBox_Morph_Die.Image = global::DescentCardCreator.Properties.Resources.Die_Morph;
            this.pictureBox_Morph_Die.Location = new System.Drawing.Point(614, 510);
            this.pictureBox_Morph_Die.Name = "pictureBox_Morph_Die";
            this.pictureBox_Morph_Die.Size = new System.Drawing.Size(20, 20);
            this.pictureBox_Morph_Die.TabIndex = 48;
            this.pictureBox_Morph_Die.TabStop = false;
            // 
            // label_Item_Special
            // 
            this.label_Item_Special.AutoSize = true;
            this.label_Item_Special.BackColor = System.Drawing.Color.Transparent;
            this.label_Item_Special.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Item_Special.Location = new System.Drawing.Point(12, 262);
            this.label_Item_Special.Name = "label_Item_Special";
            this.label_Item_Special.Size = new System.Drawing.Size(97, 20);
            this.label_Item_Special.TabIndex = 49;
            this.label_Item_Special.Text = "Item Special";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(115, 262);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 52;
            this.button1.Text = "B";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(141, 262);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(20, 20);
            this.button2.TabIndex = 53;
            this.button2.Text = "i";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.Location = new System.Drawing.Point(167, 262);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(20, 20);
            this.button3.TabIndex = 54;
            this.button3.Text = "u";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(193, 262);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(20, 20);
            this.button4.TabIndex = 55;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // numericUpDown_Font_Size
            // 
            this.numericUpDown_Font_Size.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Font_Size.Location = new System.Drawing.Point(219, 259);
            this.numericUpDown_Font_Size.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDown_Font_Size.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Font_Size.Name = "numericUpDown_Font_Size";
            this.numericUpDown_Font_Size.Size = new System.Drawing.Size(44, 26);
            this.numericUpDown_Font_Size.TabIndex = 56;
            this.numericUpDown_Font_Size.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Font_Size.ValueChanged += new System.EventHandler(this.numericUpDown_Font_Size_ValueChanged);
            // 
            // richTextBoxPrint
            // 
            this.richTextBoxPrint.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxPrint.Location = new System.Drawing.Point(12, 285);
            this.richTextBoxPrint.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.richTextBoxPrint.Name = "richTextBoxPrint";
            this.richTextBoxPrint.Size = new System.Drawing.Size(289, 215);
            this.richTextBoxPrint.TabIndex = 51;
            this.richTextBoxPrint.Text = "";
            this.richTextBoxPrint.SelectionChanged += new System.EventHandler(this.richTextBoxPrint_SelectionChanged);
            this.richTextBoxPrint.TextChanged += new System.EventHandler(this.richTextBoxPrint_TextChanged);
            // 
            // richTextBoxPrint1
            // 
            this.richTextBoxPrint1.Font = new System.Drawing.Font("Kelmscott", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxPrint1.Location = new System.Drawing.Point(13, 55);
            this.richTextBoxPrint1.Name = "richTextBoxPrint1";
            this.richTextBoxPrint1.Size = new System.Drawing.Size(288, 66);
            this.richTextBoxPrint1.TabIndex = 57;
            this.richTextBoxPrint1.Text = "";
            this.richTextBoxPrint1.TextChanged += new System.EventHandler(this.richTextBoxPrint1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 543);
            this.Controls.Add(this.richTextBoxPrint1);
            this.Controls.Add(this.numericUpDown_Font_Size);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBoxPrint);
            this.Controls.Add(this.label_Item_Special);
            this.Controls.Add(this.pictureBox_Morph_Die);
            this.Controls.Add(this.pictureBox_Gold_Die);
            this.Controls.Add(this.pictureBox_Silver_Die);
            this.Controls.Add(this.pictureBox_Black_Die);
            this.Controls.Add(this.pictureBox_Yellow_Die);
            this.Controls.Add(this.pictureBox_Green_Die);
            this.Controls.Add(this.pictureBox_White_Die);
            this.Controls.Add(this.pictureBox_Blue_Die);
            this.Controls.Add(this.pictureBox_Red_Die);
            this.Controls.Add(this.numericUpDown_Morph_Die);
            this.Controls.Add(this.numericUpDown_Silver_Die);
            this.Controls.Add(this.numericUpDown_Gold_Die);
            this.Controls.Add(this.numericUpDown_Black_Die);
            this.Controls.Add(this.numericUpDown_Yellow_Die);
            this.Controls.Add(this.numericUpDown_Green_Die);
            this.Controls.Add(this.numericUpDown_White_Die);
            this.Controls.Add(this.numericUpDown_Blue_Die);
            this.Controls.Add(this.numericUpDown_Red_Die);
            this.Controls.Add(this.label_Attack_Type);
            this.Controls.Add(this.comboBox_Attack_Type);
            this.Controls.Add(this.comboBox_Item_Type);
            this.Controls.Add(this.label_Gold);
            this.Controls.Add(this.numericUpDown_Coins);
            this.Controls.Add(this.checkBox_2Hand);
            this.Controls.Add(this.checkBox_1Hand);
            this.Controls.Add(this.label_Item_Type);
            this.Controls.Add(this.button_Browse_Picture);
            this.Controls.Add(this.textBox_Picture);
            this.Controls.Add(this.button_Browse_Icon);
            this.Controls.Add(this.textBox_Icon);
            this.Controls.Add(this.label_Picture);
            this.Controls.Add(this.label_Icon);
            this.Controls.Add(this.label_Name);
            this.Controls.Add(this.pictureBox_Card_Back);
            this.Controls.Add(this.pictureBox_Card_Front);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Coins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Card_Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Card_Front)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Red_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Blue_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_White_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Green_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Yellow_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Black_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Gold_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Silver_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Morph_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Red_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Blue_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_White_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Green_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Yellow_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Black_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Silver_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Gold_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Morph_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Font_Size)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Card_Front;
        private System.Windows.Forms.PictureBox pictureBox_Card_Back;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filesToolStripMenuItem;
        private System.Windows.Forms.Label label_Icon;
        private System.Windows.Forms.Label label_Picture;
        private System.Windows.Forms.TextBox textBox_Icon;
        private System.Windows.Forms.Button button_Browse_Icon;
        private System.Windows.Forms.Button button_Browse_Picture;
        private System.Windows.Forms.TextBox textBox_Picture;
        private System.Windows.Forms.Label label_Item_Type;
        private System.Windows.Forms.CheckBox checkBox_1Hand;
        private System.Windows.Forms.CheckBox checkBox_2Hand;
        private System.Windows.Forms.NumericUpDown numericUpDown_Coins;
        private System.Windows.Forms.Label label_Gold;
        private System.Windows.Forms.ComboBox comboBox_Item_Type;
        private System.Windows.Forms.ComboBox comboBox_Attack_Type;
        private System.Windows.Forms.Label label_Attack_Type;
        private System.Windows.Forms.NumericUpDown numericUpDown_Red_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Blue_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_White_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Green_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Yellow_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Black_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Gold_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Silver_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Morph_Die;
        private System.Windows.Forms.PictureBox pictureBox_Red_Die;
        private System.Windows.Forms.PictureBox pictureBox_Blue_Die;
        private System.Windows.Forms.PictureBox pictureBox_White_Die;
        private System.Windows.Forms.PictureBox pictureBox_Green_Die;
        private System.Windows.Forms.PictureBox pictureBox_Yellow_Die;
        private System.Windows.Forms.PictureBox pictureBox_Black_Die;
        private System.Windows.Forms.PictureBox pictureBox_Silver_Die;
        private System.Windows.Forms.PictureBox pictureBox_Gold_Die;
        private System.Windows.Forms.PictureBox pictureBox_Morph_Die;
        private System.Windows.Forms.Label label_Item_Special;
        private RichTextBoxPrint richTextBoxPrint;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.NumericUpDown numericUpDown_Font_Size;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private RichTextBoxPrint richTextBoxPrint1;
    }
}

