﻿namespace DescentCardCreator
{
    partial class Overlord
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_CardType = new System.Windows.Forms.ComboBox();
            this.label_CardType = new System.Windows.Forms.Label();
            this.richTextBox_Name = new System.Windows.Forms.RichTextBox();
            this.label_Name = new System.Windows.Forms.Label();
            this.richTextBox_CardText = new System.Windows.Forms.RichTextBox();
            this.label_CardText = new System.Windows.Forms.Label();
            this.comboBox_Type = new System.Windows.Forms.ComboBox();
            this.label_Type = new System.Windows.Forms.Label();
            this.label_DiscardThread = new System.Windows.Forms.Label();
            this.numericUpDown_DiscardThread = new System.Windows.Forms.NumericUpDown();
            this.label_ThreadCost = new System.Windows.Forms.Label();
            this.label_TreacheryCost = new System.Windows.Forms.Label();
            this.numericUpDown_ThreadCost = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_TreacheryCost = new System.Windows.Forms.NumericUpDown();
            this.checkBox_X_Discard = new System.Windows.Forms.CheckBox();
            this.checkBox_Y_Discard = new System.Windows.Forms.CheckBox();
            this.checkBox_Z_Discard = new System.Windows.Forms.CheckBox();
            this.checkBox_Z_Cost = new System.Windows.Forms.CheckBox();
            this.checkBox_Y_Cost = new System.Windows.Forms.CheckBox();
            this.checkBox_X_Cost = new System.Windows.Forms.CheckBox();
            this.checkBox_Z_Treachery = new System.Windows.Forms.CheckBox();
            this.checkBox_Y_Treachery = new System.Windows.Forms.CheckBox();
            this.checkBox_X_Treachery = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DiscardThread)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ThreadCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TreacheryCost)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox_CardType
            // 
            this.comboBox_CardType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_CardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_CardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_CardType.FormattingEnabled = true;
            this.comboBox_CardType.Items.AddRange(new object[] {
            "Normal",
            "Spawn",
            "Event",
            "Trap",
            "Treachery Spawn",
            "Treachery Event",
            "Treachery Trap"});
            this.comboBox_CardType.Location = new System.Drawing.Point(3, 23);
            this.comboBox_CardType.Name = "comboBox_CardType";
            this.comboBox_CardType.Size = new System.Drawing.Size(363, 28);
            this.comboBox_CardType.TabIndex = 107;
            this.comboBox_CardType.SelectedIndexChanged += new System.EventHandler(this.comboBox_CardType_SelectedIndexChanged);
            // 
            // label_CardType
            // 
            this.label_CardType.AutoSize = true;
            this.label_CardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CardType.Location = new System.Drawing.Point(3, 0);
            this.label_CardType.Name = "label_CardType";
            this.label_CardType.Size = new System.Drawing.Size(81, 20);
            this.label_CardType.TabIndex = 106;
            this.label_CardType.Text = "Card Type";
            // 
            // richTextBox_Name
            // 
            this.richTextBox_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_Name.Location = new System.Drawing.Point(3, 77);
            this.richTextBox_Name.Multiline = false;
            this.richTextBox_Name.Name = "richTextBox_Name";
            this.richTextBox_Name.Size = new System.Drawing.Size(363, 56);
            this.richTextBox_Name.TabIndex = 105;
            this.richTextBox_Name.Text = "";
            this.richTextBox_Name.TextChanged += new System.EventHandler(this.richTextBox_Name_TextChanged);
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.Location = new System.Drawing.Point(3, 54);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(51, 20);
            this.label_Name.TabIndex = 104;
            this.label_Name.Text = "Name";
            // 
            // richTextBox_CardText
            // 
            this.richTextBox_CardText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_CardText.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_CardText.Location = new System.Drawing.Point(3, 159);
            this.richTextBox_CardText.Name = "richTextBox_CardText";
            this.richTextBox_CardText.Size = new System.Drawing.Size(363, 122);
            this.richTextBox_CardText.TabIndex = 109;
            this.richTextBox_CardText.Text = "";
            this.richTextBox_CardText.TextChanged += new System.EventHandler(this.richTextBox_CardText_TextChanged);
            // 
            // label_CardText
            // 
            this.label_CardText.AutoSize = true;
            this.label_CardText.BackColor = System.Drawing.Color.Transparent;
            this.label_CardText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CardText.Location = new System.Drawing.Point(3, 136);
            this.label_CardText.Name = "label_CardText";
            this.label_CardText.Size = new System.Drawing.Size(77, 20);
            this.label_CardText.TabIndex = 108;
            this.label_CardText.Text = "Card Text";
            // 
            // comboBox_Type
            // 
            this.comboBox_Type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Type.FormattingEnabled = true;
            this.comboBox_Type.Items.AddRange(new object[] {
            "Power",
            "Spawn",
            "Event",
            "Trap",
            "Trap (Chest)",
            "Trap (Door)",
            "Trap (Space)",
            "Trap (Treasure)"});
            this.comboBox_Type.Location = new System.Drawing.Point(3, 307);
            this.comboBox_Type.Name = "comboBox_Type";
            this.comboBox_Type.Size = new System.Drawing.Size(363, 28);
            this.comboBox_Type.TabIndex = 113;
            this.comboBox_Type.SelectedIndexChanged += new System.EventHandler(this.comboBox_Type_SelectedIndexChanged);
            // 
            // label_Type
            // 
            this.label_Type.AutoSize = true;
            this.label_Type.BackColor = System.Drawing.Color.Transparent;
            this.label_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Type.Location = new System.Drawing.Point(3, 284);
            this.label_Type.Name = "label_Type";
            this.label_Type.Size = new System.Drawing.Size(43, 20);
            this.label_Type.TabIndex = 117;
            this.label_Type.Text = "Type";
            // 
            // label_DiscardThread
            // 
            this.label_DiscardThread.AutoSize = true;
            this.label_DiscardThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_DiscardThread.Location = new System.Drawing.Point(126, 338);
            this.label_DiscardThread.Name = "label_DiscardThread";
            this.label_DiscardThread.Size = new System.Drawing.Size(117, 20);
            this.label_DiscardThread.TabIndex = 119;
            this.label_DiscardThread.Text = "Discard Thread";
            // 
            // numericUpDown_DiscardThread
            // 
            this.numericUpDown_DiscardThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_DiscardThread.Location = new System.Drawing.Point(126, 361);
            this.numericUpDown_DiscardThread.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_DiscardThread.Name = "numericUpDown_DiscardThread";
            this.numericUpDown_DiscardThread.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_DiscardThread.TabIndex = 118;
            this.numericUpDown_DiscardThread.ValueChanged += new System.EventHandler(this.numericUpDown_DiscardThread_ValueChanged);
            // 
            // label_ThreadCost
            // 
            this.label_ThreadCost.AutoSize = true;
            this.label_ThreadCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ThreadCost.Location = new System.Drawing.Point(3, 338);
            this.label_ThreadCost.Name = "label_ThreadCost";
            this.label_ThreadCost.Size = new System.Drawing.Size(96, 20);
            this.label_ThreadCost.TabIndex = 120;
            this.label_ThreadCost.Text = "Thread Cost";
            // 
            // label_TreacheryCost
            // 
            this.label_TreacheryCost.AutoSize = true;
            this.label_TreacheryCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_TreacheryCost.Location = new System.Drawing.Point(249, 338);
            this.label_TreacheryCost.Name = "label_TreacheryCost";
            this.label_TreacheryCost.Size = new System.Drawing.Size(116, 20);
            this.label_TreacheryCost.TabIndex = 121;
            this.label_TreacheryCost.Text = "Treachery Cost";
            // 
            // numericUpDown_ThreadCost
            // 
            this.numericUpDown_ThreadCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_ThreadCost.Location = new System.Drawing.Point(3, 361);
            this.numericUpDown_ThreadCost.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_ThreadCost.Name = "numericUpDown_ThreadCost";
            this.numericUpDown_ThreadCost.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_ThreadCost.TabIndex = 122;
            this.numericUpDown_ThreadCost.ValueChanged += new System.EventHandler(this.numericUpDown_ThreadCost_ValueChanged);
            // 
            // numericUpDown_TreacheryCost
            // 
            this.numericUpDown_TreacheryCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_TreacheryCost.Location = new System.Drawing.Point(249, 361);
            this.numericUpDown_TreacheryCost.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_TreacheryCost.Name = "numericUpDown_TreacheryCost";
            this.numericUpDown_TreacheryCost.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_TreacheryCost.TabIndex = 123;
            this.numericUpDown_TreacheryCost.ValueChanged += new System.EventHandler(this.numericUpDown_TreacheryCost_ValueChanged);
            // 
            // checkBox_X_Discard
            // 
            this.checkBox_X_Discard.AutoSize = true;
            this.checkBox_X_Discard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_X_Discard.Location = new System.Drawing.Point(126, 393);
            this.checkBox_X_Discard.Name = "checkBox_X_Discard";
            this.checkBox_X_Discard.Size = new System.Drawing.Size(39, 24);
            this.checkBox_X_Discard.TabIndex = 124;
            this.checkBox_X_Discard.Text = "X";
            this.checkBox_X_Discard.UseVisualStyleBackColor = true;
            this.checkBox_X_Discard.CheckedChanged += new System.EventHandler(this.checkBox_X_Discard_CheckedChanged);
            // 
            // checkBox_Y_Discard
            // 
            this.checkBox_Y_Discard.AutoSize = true;
            this.checkBox_Y_Discard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Y_Discard.Location = new System.Drawing.Point(126, 423);
            this.checkBox_Y_Discard.Name = "checkBox_Y_Discard";
            this.checkBox_Y_Discard.Size = new System.Drawing.Size(39, 24);
            this.checkBox_Y_Discard.TabIndex = 125;
            this.checkBox_Y_Discard.Text = "Y";
            this.checkBox_Y_Discard.UseVisualStyleBackColor = true;
            this.checkBox_Y_Discard.CheckedChanged += new System.EventHandler(this.checkBox_Y_Discard_CheckedChanged);
            // 
            // checkBox_Z_Discard
            // 
            this.checkBox_Z_Discard.AutoSize = true;
            this.checkBox_Z_Discard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Z_Discard.Location = new System.Drawing.Point(126, 453);
            this.checkBox_Z_Discard.Name = "checkBox_Z_Discard";
            this.checkBox_Z_Discard.Size = new System.Drawing.Size(38, 24);
            this.checkBox_Z_Discard.TabIndex = 126;
            this.checkBox_Z_Discard.Text = "Z";
            this.checkBox_Z_Discard.UseVisualStyleBackColor = true;
            this.checkBox_Z_Discard.CheckedChanged += new System.EventHandler(this.checkBox_Z_Discard_CheckedChanged);
            // 
            // checkBox_Z_Cost
            // 
            this.checkBox_Z_Cost.AutoSize = true;
            this.checkBox_Z_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Z_Cost.Location = new System.Drawing.Point(3, 453);
            this.checkBox_Z_Cost.Name = "checkBox_Z_Cost";
            this.checkBox_Z_Cost.Size = new System.Drawing.Size(38, 24);
            this.checkBox_Z_Cost.TabIndex = 129;
            this.checkBox_Z_Cost.Text = "Z";
            this.checkBox_Z_Cost.UseVisualStyleBackColor = true;
            this.checkBox_Z_Cost.CheckedChanged += new System.EventHandler(this.checkBox_Z_Cost_CheckedChanged);
            // 
            // checkBox_Y_Cost
            // 
            this.checkBox_Y_Cost.AutoSize = true;
            this.checkBox_Y_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Y_Cost.Location = new System.Drawing.Point(3, 423);
            this.checkBox_Y_Cost.Name = "checkBox_Y_Cost";
            this.checkBox_Y_Cost.Size = new System.Drawing.Size(39, 24);
            this.checkBox_Y_Cost.TabIndex = 128;
            this.checkBox_Y_Cost.Text = "Y";
            this.checkBox_Y_Cost.UseVisualStyleBackColor = true;
            this.checkBox_Y_Cost.CheckedChanged += new System.EventHandler(this.checkBox_Y_Cost_CheckedChanged);
            // 
            // checkBox_X_Cost
            // 
            this.checkBox_X_Cost.AutoSize = true;
            this.checkBox_X_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_X_Cost.Location = new System.Drawing.Point(3, 393);
            this.checkBox_X_Cost.Name = "checkBox_X_Cost";
            this.checkBox_X_Cost.Size = new System.Drawing.Size(39, 24);
            this.checkBox_X_Cost.TabIndex = 127;
            this.checkBox_X_Cost.Text = "X";
            this.checkBox_X_Cost.UseVisualStyleBackColor = true;
            this.checkBox_X_Cost.CheckedChanged += new System.EventHandler(this.checkBox_X_Cost_CheckedChanged);
            // 
            // checkBox_Z_Treachery
            // 
            this.checkBox_Z_Treachery.AutoSize = true;
            this.checkBox_Z_Treachery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Z_Treachery.Location = new System.Drawing.Point(249, 453);
            this.checkBox_Z_Treachery.Name = "checkBox_Z_Treachery";
            this.checkBox_Z_Treachery.Size = new System.Drawing.Size(38, 24);
            this.checkBox_Z_Treachery.TabIndex = 132;
            this.checkBox_Z_Treachery.Text = "Z";
            this.checkBox_Z_Treachery.UseVisualStyleBackColor = true;
            this.checkBox_Z_Treachery.CheckedChanged += new System.EventHandler(this.checkBox_Z_Treachery_CheckedChanged);
            // 
            // checkBox_Y_Treachery
            // 
            this.checkBox_Y_Treachery.AutoSize = true;
            this.checkBox_Y_Treachery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Y_Treachery.Location = new System.Drawing.Point(249, 423);
            this.checkBox_Y_Treachery.Name = "checkBox_Y_Treachery";
            this.checkBox_Y_Treachery.Size = new System.Drawing.Size(39, 24);
            this.checkBox_Y_Treachery.TabIndex = 131;
            this.checkBox_Y_Treachery.Text = "Y";
            this.checkBox_Y_Treachery.UseVisualStyleBackColor = true;
            this.checkBox_Y_Treachery.CheckedChanged += new System.EventHandler(this.checkBox_Y_Treachery_CheckedChanged);
            // 
            // checkBox_X_Treachery
            // 
            this.checkBox_X_Treachery.AutoSize = true;
            this.checkBox_X_Treachery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_X_Treachery.Location = new System.Drawing.Point(249, 393);
            this.checkBox_X_Treachery.Name = "checkBox_X_Treachery";
            this.checkBox_X_Treachery.Size = new System.Drawing.Size(39, 24);
            this.checkBox_X_Treachery.TabIndex = 130;
            this.checkBox_X_Treachery.Text = "X";
            this.checkBox_X_Treachery.UseVisualStyleBackColor = true;
            this.checkBox_X_Treachery.CheckedChanged += new System.EventHandler(this.checkBox_X_Treachery_CheckedChanged);
            // 
            // Overlord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.checkBox_Z_Treachery);
            this.Controls.Add(this.checkBox_Y_Treachery);
            this.Controls.Add(this.checkBox_X_Treachery);
            this.Controls.Add(this.checkBox_Z_Cost);
            this.Controls.Add(this.checkBox_Y_Cost);
            this.Controls.Add(this.checkBox_X_Cost);
            this.Controls.Add(this.checkBox_Z_Discard);
            this.Controls.Add(this.checkBox_Y_Discard);
            this.Controls.Add(this.checkBox_X_Discard);
            this.Controls.Add(this.numericUpDown_TreacheryCost);
            this.Controls.Add(this.numericUpDown_ThreadCost);
            this.Controls.Add(this.label_TreacheryCost);
            this.Controls.Add(this.label_ThreadCost);
            this.Controls.Add(this.label_DiscardThread);
            this.Controls.Add(this.numericUpDown_DiscardThread);
            this.Controls.Add(this.label_Type);
            this.Controls.Add(this.comboBox_Type);
            this.Controls.Add(this.richTextBox_CardText);
            this.Controls.Add(this.label_CardText);
            this.Controls.Add(this.comboBox_CardType);
            this.Controls.Add(this.label_CardType);
            this.Controls.Add(this.richTextBox_Name);
            this.Controls.Add(this.label_Name);
            this.Name = "Overlord";
            this.Size = new System.Drawing.Size(369, 487);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DiscardThread)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ThreadCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TreacheryCost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_CardType;
        private System.Windows.Forms.Label label_CardType;
        private System.Windows.Forms.RichTextBox richTextBox_Name;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.RichTextBox richTextBox_CardText;
        private System.Windows.Forms.Label label_CardText;
        private System.Windows.Forms.ComboBox comboBox_Type;
        private System.Windows.Forms.Label label_Type;
        private System.Windows.Forms.Label label_DiscardThread;
        private System.Windows.Forms.NumericUpDown numericUpDown_DiscardThread;
        private System.Windows.Forms.Label label_ThreadCost;
        private System.Windows.Forms.Label label_TreacheryCost;
        private System.Windows.Forms.NumericUpDown numericUpDown_ThreadCost;
        private System.Windows.Forms.NumericUpDown numericUpDown_TreacheryCost;
        private System.Windows.Forms.CheckBox checkBox_X_Discard;
        private System.Windows.Forms.CheckBox checkBox_Y_Discard;
        private System.Windows.Forms.CheckBox checkBox_Z_Discard;
        private System.Windows.Forms.CheckBox checkBox_Z_Cost;
        private System.Windows.Forms.CheckBox checkBox_Y_Cost;
        private System.Windows.Forms.CheckBox checkBox_X_Cost;
        private System.Windows.Forms.CheckBox checkBox_Z_Treachery;
        private System.Windows.Forms.CheckBox checkBox_Y_Treachery;
        private System.Windows.Forms.CheckBox checkBox_X_Treachery;

    }
}
