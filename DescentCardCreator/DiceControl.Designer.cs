﻿namespace DescentCardCreator
{
    partial class DiceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiceControl));
            this.pictureBox_Morph_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Gold_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Silver_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Black_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Yellow_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Green_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_White_Die = new System.Windows.Forms.PictureBox();
            this.pictureBox_Blue_Die = new System.Windows.Forms.PictureBox();
            this.numericUpDown_Morph_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Silver_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Gold_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Black_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Yellow_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Green_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_White_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Blue_Die = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Red_Die = new System.Windows.Forms.NumericUpDown();
            this.pictureBox_Purple_Die = new System.Windows.Forms.PictureBox();
            this.numericUpDown_Purple_Die = new System.Windows.Forms.NumericUpDown();
            this.pictureBox_Red_Die = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Morph_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Gold_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Silver_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Black_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Yellow_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Green_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_White_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Blue_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Morph_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Silver_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Gold_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Black_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Yellow_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Green_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_White_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Blue_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Red_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Purple_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Purple_Die)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Red_Die)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_Morph_Die
            // 
            this.pictureBox_Morph_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Morph_Die.Image")));
            this.pictureBox_Morph_Die.Location = new System.Drawing.Point(405, 3);
            this.pictureBox_Morph_Die.Name = "pictureBox_Morph_Die";
            this.pictureBox_Morph_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Morph_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Morph_Die.TabIndex = 115;
            this.pictureBox_Morph_Die.TabStop = false;
            // 
            // pictureBox_Gold_Die
            // 
            this.pictureBox_Gold_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Gold_Die.Image")));
            this.pictureBox_Gold_Die.Location = new System.Drawing.Point(606, 3);
            this.pictureBox_Gold_Die.Name = "pictureBox_Gold_Die";
            this.pictureBox_Gold_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Gold_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Gold_Die.TabIndex = 114;
            this.pictureBox_Gold_Die.TabStop = false;
            // 
            // pictureBox_Silver_Die
            // 
            this.pictureBox_Silver_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Silver_Die.Image")));
            this.pictureBox_Silver_Die.Location = new System.Drawing.Point(539, 3);
            this.pictureBox_Silver_Die.Name = "pictureBox_Silver_Die";
            this.pictureBox_Silver_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Silver_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Silver_Die.TabIndex = 113;
            this.pictureBox_Silver_Die.TabStop = false;
            // 
            // pictureBox_Black_Die
            // 
            this.pictureBox_Black_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Black_Die.Image")));
            this.pictureBox_Black_Die.Location = new System.Drawing.Point(472, 3);
            this.pictureBox_Black_Die.Name = "pictureBox_Black_Die";
            this.pictureBox_Black_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Black_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Black_Die.TabIndex = 112;
            this.pictureBox_Black_Die.TabStop = false;
            // 
            // pictureBox_Yellow_Die
            // 
            this.pictureBox_Yellow_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Yellow_Die.Image")));
            this.pictureBox_Yellow_Die.Location = new System.Drawing.Point(338, 3);
            this.pictureBox_Yellow_Die.Name = "pictureBox_Yellow_Die";
            this.pictureBox_Yellow_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Yellow_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Yellow_Die.TabIndex = 111;
            this.pictureBox_Yellow_Die.TabStop = false;
            // 
            // pictureBox_Green_Die
            // 
            this.pictureBox_Green_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Green_Die.Image")));
            this.pictureBox_Green_Die.Location = new System.Drawing.Point(271, 3);
            this.pictureBox_Green_Die.Name = "pictureBox_Green_Die";
            this.pictureBox_Green_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Green_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Green_Die.TabIndex = 110;
            this.pictureBox_Green_Die.TabStop = false;
            // 
            // pictureBox_White_Die
            // 
            this.pictureBox_White_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_White_Die.Image")));
            this.pictureBox_White_Die.Location = new System.Drawing.Point(137, 3);
            this.pictureBox_White_Die.Name = "pictureBox_White_Die";
            this.pictureBox_White_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_White_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_White_Die.TabIndex = 109;
            this.pictureBox_White_Die.TabStop = false;
            // 
            // pictureBox_Blue_Die
            // 
            this.pictureBox_Blue_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Blue_Die.Image")));
            this.pictureBox_Blue_Die.Location = new System.Drawing.Point(70, 3);
            this.pictureBox_Blue_Die.Name = "pictureBox_Blue_Die";
            this.pictureBox_Blue_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Blue_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Blue_Die.TabIndex = 108;
            this.pictureBox_Blue_Die.TabStop = false;
            // 
            // numericUpDown_Morph_Die
            // 
            this.numericUpDown_Morph_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Morph_Die.Location = new System.Drawing.Point(436, 3);
            this.numericUpDown_Morph_Die.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Morph_Die.Name = "numericUpDown_Morph_Die";
            this.numericUpDown_Morph_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Morph_Die.TabIndex = 106;
            this.numericUpDown_Morph_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Silver_Die
            // 
            this.numericUpDown_Silver_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Silver_Die.Location = new System.Drawing.Point(570, 3);
            this.numericUpDown_Silver_Die.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Silver_Die.Name = "numericUpDown_Silver_Die";
            this.numericUpDown_Silver_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Silver_Die.TabIndex = 105;
            this.numericUpDown_Silver_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Gold_Die
            // 
            this.numericUpDown_Gold_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Gold_Die.Location = new System.Drawing.Point(637, 3);
            this.numericUpDown_Gold_Die.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Gold_Die.Name = "numericUpDown_Gold_Die";
            this.numericUpDown_Gold_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Gold_Die.TabIndex = 104;
            this.numericUpDown_Gold_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Black_Die
            // 
            this.numericUpDown_Black_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Black_Die.Location = new System.Drawing.Point(503, 3);
            this.numericUpDown_Black_Die.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Black_Die.Name = "numericUpDown_Black_Die";
            this.numericUpDown_Black_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Black_Die.TabIndex = 103;
            this.numericUpDown_Black_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Yellow_Die
            // 
            this.numericUpDown_Yellow_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Yellow_Die.Location = new System.Drawing.Point(369, 3);
            this.numericUpDown_Yellow_Die.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown_Yellow_Die.Name = "numericUpDown_Yellow_Die";
            this.numericUpDown_Yellow_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Yellow_Die.TabIndex = 102;
            this.numericUpDown_Yellow_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Green_Die
            // 
            this.numericUpDown_Green_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Green_Die.Location = new System.Drawing.Point(302, 3);
            this.numericUpDown_Green_Die.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown_Green_Die.Name = "numericUpDown_Green_Die";
            this.numericUpDown_Green_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Green_Die.TabIndex = 101;
            this.numericUpDown_Green_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_White_Die
            // 
            this.numericUpDown_White_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_White_Die.Location = new System.Drawing.Point(168, 3);
            this.numericUpDown_White_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_White_Die.Name = "numericUpDown_White_Die";
            this.numericUpDown_White_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_White_Die.TabIndex = 100;
            this.numericUpDown_White_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Blue_Die
            // 
            this.numericUpDown_Blue_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Blue_Die.Location = new System.Drawing.Point(101, 3);
            this.numericUpDown_Blue_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Blue_Die.Name = "numericUpDown_Blue_Die";
            this.numericUpDown_Blue_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Blue_Die.TabIndex = 99;
            this.numericUpDown_Blue_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // numericUpDown_Red_Die
            // 
            this.numericUpDown_Red_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Red_Die.Location = new System.Drawing.Point(34, 3);
            this.numericUpDown_Red_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Red_Die.Name = "numericUpDown_Red_Die";
            this.numericUpDown_Red_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Red_Die.TabIndex = 98;
            this.numericUpDown_Red_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // pictureBox_Purple_Die
            // 
            this.pictureBox_Purple_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Purple_Die.Image")));
            this.pictureBox_Purple_Die.Location = new System.Drawing.Point(204, 3);
            this.pictureBox_Purple_Die.Name = "pictureBox_Purple_Die";
            this.pictureBox_Purple_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Purple_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Purple_Die.TabIndex = 117;
            this.pictureBox_Purple_Die.TabStop = false;
            // 
            // numericUpDown_Purple_Die
            // 
            this.numericUpDown_Purple_Die.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Purple_Die.Location = new System.Drawing.Point(235, 3);
            this.numericUpDown_Purple_Die.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Purple_Die.Name = "numericUpDown_Purple_Die";
            this.numericUpDown_Purple_Die.Size = new System.Drawing.Size(30, 26);
            this.numericUpDown_Purple_Die.TabIndex = 116;
            this.numericUpDown_Purple_Die.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // pictureBox_Red_Die
            // 
            this.pictureBox_Red_Die.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Red_Die.Image")));
            this.pictureBox_Red_Die.Location = new System.Drawing.Point(3, 3);
            this.pictureBox_Red_Die.Name = "pictureBox_Red_Die";
            this.pictureBox_Red_Die.Size = new System.Drawing.Size(25, 26);
            this.pictureBox_Red_Die.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Red_Die.TabIndex = 107;
            this.pictureBox_Red_Die.TabStop = false;
            // 
            // DiceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox_Purple_Die);
            this.Controls.Add(this.numericUpDown_Purple_Die);
            this.Controls.Add(this.pictureBox_Morph_Die);
            this.Controls.Add(this.pictureBox_Gold_Die);
            this.Controls.Add(this.pictureBox_Silver_Die);
            this.Controls.Add(this.pictureBox_Black_Die);
            this.Controls.Add(this.pictureBox_Yellow_Die);
            this.Controls.Add(this.pictureBox_Green_Die);
            this.Controls.Add(this.pictureBox_White_Die);
            this.Controls.Add(this.pictureBox_Blue_Die);
            this.Controls.Add(this.pictureBox_Red_Die);
            this.Controls.Add(this.numericUpDown_Morph_Die);
            this.Controls.Add(this.numericUpDown_Silver_Die);
            this.Controls.Add(this.numericUpDown_Gold_Die);
            this.Controls.Add(this.numericUpDown_Black_Die);
            this.Controls.Add(this.numericUpDown_Yellow_Die);
            this.Controls.Add(this.numericUpDown_Green_Die);
            this.Controls.Add(this.numericUpDown_White_Die);
            this.Controls.Add(this.numericUpDown_Blue_Die);
            this.Controls.Add(this.numericUpDown_Red_Die);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DiceControl";
            this.Size = new System.Drawing.Size(674, 31);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Morph_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Gold_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Silver_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Black_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Yellow_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Green_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_White_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Blue_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Morph_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Silver_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Gold_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Black_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Yellow_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Green_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_White_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Blue_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Red_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Purple_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Purple_Die)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Red_Die)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Morph_Die;
        private System.Windows.Forms.PictureBox pictureBox_Gold_Die;
        private System.Windows.Forms.PictureBox pictureBox_Silver_Die;
        private System.Windows.Forms.PictureBox pictureBox_Black_Die;
        private System.Windows.Forms.PictureBox pictureBox_Yellow_Die;
        private System.Windows.Forms.PictureBox pictureBox_Green_Die;
        private System.Windows.Forms.PictureBox pictureBox_White_Die;
        private System.Windows.Forms.PictureBox pictureBox_Blue_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Morph_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Silver_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Gold_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Black_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Yellow_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Green_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_White_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Blue_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Red_Die;
        private System.Windows.Forms.PictureBox pictureBox_Purple_Die;
        private System.Windows.Forms.NumericUpDown numericUpDown_Purple_Die;
        private System.Windows.Forms.PictureBox pictureBox_Red_Die;
    }
}
