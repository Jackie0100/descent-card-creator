﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DescentCardGraphics;

namespace DescentCardCreator
{
    public partial class DiceControl : UserControl
    {
        public DiceControl()
        {
            InitializeComponent();
        }

        private void numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            (GetControl(this.Parent) as ICardDesigner).UpdateCard();
        }

        private Control GetControl(Control c)
        {
            if (!(c is ICardDesigner))
            {
                return GetControl(c.Parent);
            }
            else
            {
                return c;
            }
        }

        public List<DiceType> GetDice()
        {
            List<DescentCardGraphics.DiceType> ret = new List<DescentCardGraphics.DiceType>();
            for (int i = 0; i < numericUpDown_Red_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Red);
            }
            for (int i = 0; i < numericUpDown_Blue_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Blue);
            }
            for (int i = 0; i < numericUpDown_White_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.White);
            }
            for (int i = 0; i < numericUpDown_Purple_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Purple);
            }
            for (int i = 0; i < numericUpDown_Morph_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Morph);
            }
            for (int i = 0; i < numericUpDown_Green_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Green);
            }
            for (int i = 0; i < numericUpDown_Yellow_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Yellow);
            }
            for (int i = 0; i < numericUpDown_Black_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Black);
            }
            for (int i = 0; i < numericUpDown_Silver_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Silver);
            }
            for (int i = 0; i < numericUpDown_Gold_Die.Value; i++)
            {
                ret.Add(DescentCardGraphics.DiceType.Gold);
            }
            return ret;
        }

        public void SetDice(List<DiceType> value)
        {
            try
            {
                numericUpDown_Red_Die.Value = value.Count(s => s == DiceType.Red);
                numericUpDown_Blue_Die.Value = value.Count(s => s == DiceType.Blue);
                numericUpDown_White_Die.Value = value.Count(s => s == DiceType.White);
                numericUpDown_Green_Die.Value = value.Count(s => s == DiceType.Green);
                numericUpDown_Yellow_Die.Value = value.Count(s => s == DiceType.Yellow);
                numericUpDown_Morph_Die.Value = value.Count(s => s == DiceType.Morph);
                numericUpDown_Black_Die.Value = value.Count(s => s == DiceType.Black);
                numericUpDown_Silver_Die.Value = value.Count(s => s == DiceType.Silver);
                numericUpDown_Gold_Die.Value = value.Count(s => s == DiceType.Gold);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
