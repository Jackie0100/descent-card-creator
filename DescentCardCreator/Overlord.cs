﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DescentCardGraphics;

namespace DescentCardCreator
{
    public partial class Overlord : UserControl, ICardDesigner
    {
        public MainForm ParentOwner { get; set; }
        public RichTextBox ForcusedRTFControl { get; set; }
        public UserControl ThisControl { get; set; }

        public Overlord()
        {
            InitializeComponent();
            ThisControl = this;
            comboBox_CardType.SelectedIndex = 0;
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    c.GotFocus += new EventHandler(RTFGotFocus);
                    c.LostFocus += new EventHandler(RTFLostFocus);
                    (c as RichTextBox).SelectAll();
                    (c as RichTextBox).SelectionAlignment = HorizontalAlignment.Center;
                }
            }
            ForcusedRTFControl = this.richTextBox_Name;
            //richTextBox_CardText.SelectionAlignment = HorizontalAlignment.Center;
        }

        public void SetProps(Card c)
        {
            richTextBox_Name.Text = (c as OverLord).Name;
            richTextBox_CardText.Text = (c as OverLord).Name;
            comboBox_Type.Text = (c as OverLord).CardType;
            comboBox_CardType.SelectedIndex = (int)(c as OverLord).OverlordCardType;
            if ((c as OverLord).ThreadCost == null)
            {
                (c as OverLord).ThreadCost = "0";
            }
            if ((c as OverLord).ThreadCost != "X" || (c as OverLord).ThreadCost != "Y" || (c as OverLord).ThreadCost != "Z")
            {
                numericUpDown_ThreadCost.Value = decimal.Parse((c as OverLord).ThreadCost);
            }
            else
            {
                numericUpDown_ThreadCost.Enabled = false;
                if ((c as OverLord).DiscardThread == "X")
                {
                    checkBox_X_Cost.Checked = true;
                }
                else if ((c as OverLord).DiscardThread == "Y")
                {
                    checkBox_Y_Cost.Checked = true;
                }
                else
                {
                    checkBox_Z_Cost.Checked = true;
                }
            }
            if ((c as OverLord).DiscardThread != "X" || (c as OverLord).ThreadCost != "Y" || (c as OverLord).ThreadCost != "Z")
            {
                numericUpDown_DiscardThread.Value = decimal.Parse((c as OverLord).DiscardThread);
            }
            else
            {
                numericUpDown_DiscardThread.Enabled = false;
                if ((c as OverLord).DiscardThread == "X")
                {
                    checkBox_X_Discard.Checked = true;
                }
                else if ((c as OverLord).DiscardThread == "Y")
                {
                    checkBox_Y_Discard.Checked = true;
                }
                else
                {
                    checkBox_Z_Discard.Checked = true;
                }
            }
            if (c is Treachery)
            {
                if ((c as Treachery).TreacheryCost != "X" || (c as Treachery).TreacheryCost != "Y" || (c as Treachery).TreacheryCost != "Z")
                {
                    numericUpDown_TreacheryCost.Value = decimal.Parse((c as Treachery).TreacheryCost);
                }
                else
                {
                    numericUpDown_TreacheryCost.Enabled = false;
                    if ((c as Treachery).TreacheryCost == "X")
                    {
                        checkBox_X_Treachery.Checked = true;
                    }
                    else if ((c as Treachery).TreacheryCost == "Y")
                    {
                        checkBox_Y_Treachery.Checked = true;
                    }
                    else
                    {
                        checkBox_Z_Treachery.Checked = true;
                    }
                }
            }
        }

        public void Reset()
        {
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    (c as RichTextBox).Text = "";
                }
                else if (c is TextBox)
                {
                    (c as TextBox).Text = "";
                }
                else if (c is NumericUpDown)
                {
                    (c as NumericUpDown).Value = 0;
                }
                else if (c is ComboBox)
                {
                    (c as ComboBox).SelectedIndex = 0;
                }
                else if (c is CheckBox)
                {
                    (c as CheckBox).Checked = false;
                }
            }
        }

        public void RTFLostFocus(object sender, EventArgs e)
        {
            ParentOwner.HideFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        public void RTFGotFocus(object sender, EventArgs e)
        {
            ParentOwner.ShowFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        private void comboBox_CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_CardType.SelectedIndex < 4)
                {
                    checkBox_X_Treachery.Enabled = false;
                    checkBox_Y_Treachery.Enabled = false;
                    checkBox_Z_Treachery.Enabled = false;
                    checkBox_X_Treachery.Checked = false;
                    checkBox_Y_Treachery.Checked = false;
                    checkBox_Z_Treachery.Checked = false;
                    numericUpDown_TreacheryCost.Enabled = false;
                    OverLord tr = new OverLord();
                    tr.Name = (ParentOwner.CurrentActiveCard as OverLord).Name;
                    tr.Text = (ParentOwner.CurrentActiveCard as OverLord).Text;
                    tr.Count = (ParentOwner.CurrentActiveCard as OverLord).Count;
                    tr.CardType = (ParentOwner.CurrentActiveCard as OverLord).CardType;
                    tr.ThreadCost = (ParentOwner.CurrentActiveCard as OverLord).ThreadCost;
                    tr.DiscardThread = (ParentOwner.CurrentActiveCard as OverLord).DiscardThread;
                    tr.Symbol = (ParentOwner.CurrentActiveCard as OverLord).Symbol;
                    tr.OverlordCardType = (OverLoadCardType)comboBox_CardType.SelectedIndex;
                    DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards[DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards.IndexOf(ParentOwner.CurrentActiveCard)] = tr;
                    ParentOwner.CurrentActiveCard = tr;
                }
                else
                {
                    checkBox_X_Treachery.Enabled = true;
                    checkBox_Y_Treachery.Enabled = true;
                    checkBox_Z_Treachery.Enabled = true;
                    numericUpDown_TreacheryCost.Enabled = true;
                    Treachery tr = new Treachery();
                    tr.Name = (ParentOwner.CurrentActiveCard as OverLord).Name;
                    tr.Text = (ParentOwner.CurrentActiveCard as OverLord).Text;
                    tr.Count = (ParentOwner.CurrentActiveCard as OverLord).Count;
                    tr.CardType = (ParentOwner.CurrentActiveCard as OverLord).CardType;
                    tr.ThreadCost = (ParentOwner.CurrentActiveCard as OverLord).ThreadCost;
                    tr.DiscardThread = (ParentOwner.CurrentActiveCard as OverLord).DiscardThread;
                    tr.DiscardThread = (ParentOwner.CurrentActiveCard as OverLord).DiscardThread;
                    tr.Symbol = (ParentOwner.CurrentActiveCard as OverLord).Symbol;
                    tr.OverlordCardType = (OverLoadCardType)comboBox_CardType.SelectedIndex;
                    if (ParentOwner.CurrentActiveCard is Treachery)
                    {
                        tr.TreacheryCost = (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost;
                    }
                    else
                    {
                        if (numericUpDown_TreacheryCost.Enabled)
                        {
                            tr.TreacheryCost = numericUpDown_TreacheryCost.Value.ToString();
                        }
                        else
                        {
                            if(checkBox_X_Treachery.Checked)
                            {
                                tr.TreacheryCost = "X";
                            }
                            else if (checkBox_Y_Treachery.Checked)
                            {
                                tr.TreacheryCost = "Y";                            
                            }
                            else if (checkBox_Z_Treachery.Checked)
                            {
                                tr.TreacheryCost = "Z";
                            }
                            else
                            {
                                tr.TreacheryCost = "0";
                            }
                        }
                    }
                    DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards[DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards.IndexOf(ParentOwner.CurrentActiveCard)] = tr;
                    ParentOwner.CurrentActiveCard = tr;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            UpdateCard();
        }

        private void checkBox_X_Treachery_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_X_Treachery.Checked)
            {
                checkBox_Y_Treachery.Checked = false;
                checkBox_Z_Treachery.Checked = false;
                numericUpDown_TreacheryCost.Enabled = false;
                (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = "X";
            }
            else
            {
                numericUpDown_TreacheryCost.Enabled = true;
                (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = numericUpDown_TreacheryCost.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Y_Treachery_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Y_Treachery.Checked)
            {
                checkBox_X_Treachery.Checked = false;
                checkBox_Z_Treachery.Checked = false;
                numericUpDown_TreacheryCost.Enabled = false;
                (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = "Y";
            }
            else
            {
                numericUpDown_TreacheryCost.Enabled = true;
                (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = numericUpDown_TreacheryCost.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Z_Treachery_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Z_Treachery.Checked)
            {
                checkBox_X_Treachery.Checked = false;
                checkBox_Y_Treachery.Checked = false;
                numericUpDown_TreacheryCost.Enabled = false;
                (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = "Z";
            }
            else
            {
                numericUpDown_TreacheryCost.Enabled = true;
                (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = numericUpDown_TreacheryCost.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_X_Discard_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_X_Discard.Checked)
            {
                checkBox_Y_Discard.Checked = false;
                checkBox_Z_Discard.Checked = false;
                numericUpDown_DiscardThread.Enabled = false;
                (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = "X";
            }
            else
            {
                numericUpDown_DiscardThread.Enabled = true;
                (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = numericUpDown_DiscardThread.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Y_Discard_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Y_Discard.Checked)
            {
                checkBox_X_Discard.Checked = false;
                checkBox_Z_Discard.Checked = false;
                numericUpDown_DiscardThread.Enabled = false;
                (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = "Y";
            }
            else
            {
                numericUpDown_DiscardThread.Enabled = true;
                (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = numericUpDown_DiscardThread.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Z_Discard_CheckedChanged(object sender, EventArgs e)
        {


            if (checkBox_Z_Discard.Checked)
            {
                checkBox_X_Discard.Checked = false;
                checkBox_Y_Discard.Checked = false;
                numericUpDown_DiscardThread.Enabled = false;
                (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = "Z";
            }
            else
            {
                numericUpDown_DiscardThread.Enabled = true;
                (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = numericUpDown_DiscardThread.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_X_Cost_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_X_Cost.Checked)
            {
                checkBox_Y_Cost.Checked = false;
                checkBox_Z_Cost.Checked = false;
                numericUpDown_ThreadCost.Enabled = false;
                (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = "X";
            }
            else
            {
                numericUpDown_ThreadCost.Enabled = true;
                (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = numericUpDown_ThreadCost.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Y_Cost_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Y_Cost.Checked)
            {
                checkBox_X_Cost.Checked = false;
                checkBox_Z_Cost.Checked = false;
                numericUpDown_ThreadCost.Enabled = false;
                (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = "Y";

            }
            else
            {
                numericUpDown_ThreadCost.Enabled = true;
                (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = numericUpDown_ThreadCost.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Z_Cost_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Z_Cost.Checked)
            {
                checkBox_X_Cost.Checked = false;
                checkBox_Y_Cost.Checked = false;
                numericUpDown_ThreadCost.Enabled = false;
                (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = "Z";
            }
            else
            {
                numericUpDown_ThreadCost.Enabled = true;
                (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = numericUpDown_ThreadCost.Value.ToString();
            }
            UpdateCard();
        }

        public void UpdateCard()
        {
            try
            {
                if (ParentOwner.backgroundWorkerPictureUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerPictureUpdater.CancelAsync();
                }
                if (ParentOwner.backgroundWorkerListViewUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerListViewUpdater.CancelAsync();
                }
                ParentOwner.backgroundWorkerPictureUpdater.RunWorkerAsync();
                ParentOwner.backgroundWorkerListViewUpdater.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void richTextBox_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as OverLord).Name = richTextBox_Name.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void comboBox_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as OverLord).CardType = comboBox_Type.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void richTextBox_CardText_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as OverLord).Text = richTextBox_CardText.Rtf;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void numericUpDown_ThreadCost_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as OverLord).ThreadCost = numericUpDown_ThreadCost.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_DiscardThread_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as OverLord).DiscardThread = numericUpDown_DiscardThread.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_TreacheryCost_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as Treachery).TreacheryCost = numericUpDown_TreacheryCost.Value.ToString();
            UpdateCard();
        }
    }
}