﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DescentCardCreator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            richTextBoxPrint.SelectionAlignment = HorizontalAlignment.Center;
            richTextBoxPrint1.SelectionAlignment = HorizontalAlignment.Center;
            richTextBoxPrint.LineSpace();
        }

        

        private void ChangePicture()
        {
            try
            {
                Bitmap bitmap = new Bitmap(Properties.Resources.Item_Front);
                bitmap.SetResolution(600, 600);
                Graphics tmp = Graphics.FromImage(bitmap);
                StringFormat strformat = new StringFormat();
                strformat.Alignment = StringAlignment.Center;
                strformat.LineAlignment = StringAlignment.Center;

                tmp.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                tmp.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                tmp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                //tmp.DrawImage(Properties.Resources.Item_Front, 0, 0, 216, 336);
                richTextBoxPrint1.LineSpace(210);//Ring of Protection

                Bitmap image = richTextBoxPrint1.PrintImage(new Size(148, 80));
                image.MakeTransparent(image.GetPixel(1, 1));
                tmp.DrawImage(image, new RectangleF(34, 16, 148, 80));
                tmp.DrawString(comboBox_Item_Type.Text, new Font("KelmScott", 10, FontStyle.Regular), Brushes.Black, new RectangleF(34, 142, 148, 100), strformat);
                
                if (comboBox_Attack_Type.Text == "")
                { 
                    strformat = new StringFormat();
                    strformat.Alignment = StringAlignment.Center;
                    image = richTextBoxPrint.PrintImage(new Size(148, 130));
                    image.MakeTransparent(image.GetPixel(1, 1));
                    image.MakeTransparent(Properties.Resources.surgeIcon.GetPixel(1, 1));
                    tmp.DrawImage(image, new RectangleF(34, 200, 148, 130));
                }
                else
                {
                    tmp.DrawString(comboBox_Attack_Type.Text, new Font("KelmScott", 8, FontStyle.Regular), Brushes.Black, new RectangleF(50, 260, 200, 50), strformat);
                    strformat = new StringFormat();
                    strformat.Alignment = StringAlignment.Center;
                    image = richTextBoxPrint.PrintImage(new Size(250, 110));
                    image.MakeTransparent(image.GetPixel(1, 1));
                    image.MakeTransparent(Properties.Resources.surgeIcon.GetPixel(1, 1));
                    tmp.DrawImage(image, new RectangleF(30, 300, 250, 110));
                }

                try
                {
                    tmp.DrawImage(Image.FromFile(textBox_Icon.Text), new RectangleF(260, 26, 12, 12));
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                try
                {
                    tmp.DrawImage(Image.FromFile(textBox_Picture.Text), new RectangleF(52, 70, 196, 166));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                if (numericUpDown_Coins.Value != 0)
                {
                    strformat = new StringFormat();
                    tmp.DrawImage(Properties.Resources.Coin, new RectangleF(32, 415, 18, 28));
                    tmp.DrawString(numericUpDown_Coins.Value.ToString(), new Font("KelmScott", 14, FontStyle.Regular), Brushes.Black, new RectangleF(48, 420, 100, 23), strformat);
                }
                #region Hands
                if (checkBox_1Hand.Checked)
                {
                    if (numericUpDown_Coins.Value != 0)
                    {
                        tmp.DrawImage(Properties.Resources.Hand1_icon, new RectangleF(100, 413, 50, 27));
                    }
                    else
                    {
                        tmp.DrawImage(Properties.Resources.Hand1_icon, new RectangleF(38, 413, 50, 27));
                    }
                }
                if (checkBox_2Hand.Checked)
                {
                    if (numericUpDown_Coins.Value != 0)
                    {
                        tmp.DrawImage(Properties.Resources.Hand2_icon, new RectangleF(100, 413, 50, 27));
                    }
                    else
                    {
                        tmp.DrawImage(Properties.Resources.Hand2_icon, new RectangleF(38, 413, 50, 27));
                    }
                }
                #endregion
                int offset = 0;
                #region Dice
                for (int i = 0; i < numericUpDown_Gold_Die.Value; i++)
                {
                    
                }
                for (int i = 0; i < numericUpDown_Silver_Die.Value; i++)
                {
                }
                for (int i = 0; i < numericUpDown_Black_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_Black, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                for (int i = 0; i < numericUpDown_Morph_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_Morph, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                for (int i = 0; i < numericUpDown_Yellow_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_Yellow, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                for (int i = 0; i < numericUpDown_Green_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_Green, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                for (int i = 0; i < numericUpDown_White_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_White, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                for (int i = 0; i < numericUpDown_Blue_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_Blue, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                for (int i = 0; i < numericUpDown_Red_Die.Value; i++)
                {
                    tmp.DrawImage(Properties.Resources.Die_Red, new RectangleF(240 - offset, 415, 24, 24));
                    offset += 28;
                }
                #endregion
                
                pictureBox_Card_Front.Image = bitmap;
                pictureBox_Card_Back.Image = bitmap;

                bitmap.Save("lol.png", ImageFormat.Png);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void textBox_Name_TextChanged(object sender, EventArgs e)
        {
            ChangePicture();
        }

        private void button_Browse_Icon_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
            dialog.Title = "select an image";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox_Icon.Text = dialog.FileName;
            }
            ChangePicture();
        }

        private void button_Browse_Picture_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
            dialog.Title = "select an image";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox_Picture.Text = dialog.FileName;
            }
            ChangePicture();
        }

        private void numericUpDown_Coins_ValueChanged(object sender, EventArgs e)
        {
            ChangePicture();
        }

        private void checkBox_1Hand_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_1Hand.Checked)
            {
                checkBox_2Hand.Checked = false;
            }
            ChangePicture();
        }

        private void checkBox_2Hand_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_2Hand.Checked)
            {
                checkBox_1Hand.Checked = false;
            }
            ChangePicture();
        }

        private void comboBox_Attack_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangePicture();
        }

        private void comboBox_Item_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangePicture();
        }

        private void numericUpDown_Dice_Value_Changed(object sender, EventArgs e)
        {
            ChangePicture();
        }

        private void richTextBoxPrint_TextChanged(object sender, EventArgs e)
        {
            ChangePicture();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Image b = Properties.Resources.surgeIcon;

            Clipboard.SetImage(b);
            richTextBoxPrint.Paste();
            richTextBoxPrint.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (richTextBoxPrint.SelectionFont.Bold)
            {
                richTextBoxPrint.SelectionFont = new Font(richTextBoxPrint.Font, FontStyle.Bold ^ richTextBoxPrint.SelectionFont.Style);
            }
            else
            {
                richTextBoxPrint.SelectionFont = new Font(richTextBoxPrint.Font, FontStyle.Bold | richTextBoxPrint.SelectionFont.Style);
            }
            richTextBoxPrint.Focus();
            richTextBoxPrint.DeselectAll();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (richTextBoxPrint.SelectionFont.Italic)
            {
                richTextBoxPrint.SelectionFont = new Font(richTextBoxPrint.Font, FontStyle.Italic ^ richTextBoxPrint.SelectionFont.Style);
            }
            else
            {
                richTextBoxPrint.SelectionFont = new Font(richTextBoxPrint.Font, FontStyle.Italic | richTextBoxPrint.SelectionFont.Style);
            }
            richTextBoxPrint.Focus();
            richTextBoxPrint.DeselectAll();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (richTextBoxPrint.SelectionFont.Underline)
            {
                richTextBoxPrint.SelectionFont = new Font(richTextBoxPrint.Font, FontStyle.Underline ^ richTextBoxPrint.SelectionFont.Style);
            }
            else
            {
                richTextBoxPrint.SelectionFont = new Font(richTextBoxPrint.Font, FontStyle.Underline | richTextBoxPrint.SelectionFont.Style);
            }
            richTextBoxPrint.Focus();
            richTextBoxPrint.DeselectAll();
        }

        private void numericUpDown_Font_Size_ValueChanged(object sender, EventArgs e)
        {
            richTextBoxPrint.SelectionFont = new Font("Times New Roman", float.Parse(numericUpDown_Font_Size.Value.ToString()), richTextBoxPrint.Font.Style);
            richTextBoxPrint.LineSpace();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void richTextBoxPrint_SelectionChanged(object sender, EventArgs e)
        {
            numericUpDown_Font_Size.Value = (decimal)richTextBoxPrint.SelectionFont.SizeInPoints;
        }

        private void richTextBoxPrint1_TextChanged(object sender, EventArgs e)
        {
            ChangePicture();
        }
    }
}
