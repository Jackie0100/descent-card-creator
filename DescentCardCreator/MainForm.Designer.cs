﻿namespace DescentCardCreator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featSkillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heroSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monsterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overlordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.newCustomCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.italicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.surgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setExpansionIconToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.donateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.endUserAgreementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButton_AddCard = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripMenuItem_Feat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Hero = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Item = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_monster = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Overlord = new System.Windows.Forms.ToolStripMenuItem();
            this.removeCardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripNumericUpDown_Cards = new DescentCardCreator.ToolStripNumericUpDown();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.boldToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.italicToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.understrikeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.surgeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripNumericUpDown_TextSize = new DescentCardCreator.ToolStripNumericUpDown();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.treeView_Databases = new System.Windows.Forms.TreeView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.listView_Cards = new System.Windows.Forms.ListView();
            this.featandSkill1 = new DescentCardCreator.FeatandSkill();
            this.items1 = new DescentCardCreator.Items();
            this.overlord1 = new DescentCardCreator.Overlord();
            this.monster1 = new DescentCardCreator.Monsters();
            this.heroSheet1 = new DescentCardCreator.HeroSheets();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.backgroundWorkerPictureUpdater = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerListViewUpdater = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.cardsToolStripMenuItem,
            this.formatToolStripMenuItem,
            this.optionToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1045, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1,
            this.openToolStripMenuItem1,
            this.toolStripSeparator8,
            this.saveToolStripMenuItem1,
            this.exportToToolStripMenuItem,
            this.toolStripSeparator9,
            this.printToolStripMenuItem1,
            this.printPreviewToolStripMenuItem1,
            this.toolStripSeparator10,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem1.Image")));
            this.newToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(192, 24);
            this.newToolStripMenuItem1.Text = "&New";
            this.newToolStripMenuItem1.Click += new System.EventHandler(this.CreateNewDatabase);
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem1.Image")));
            this.openToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(192, 24);
            this.openToolStripMenuItem1.Text = "&Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.LoadDataBase);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(189, 6);
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem1.Image")));
            this.saveToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(192, 24);
            this.saveToolStripMenuItem1.Text = "&Save";
            this.saveToolStripMenuItem1.Click += new System.EventHandler(this.SaveDataBase);
            // 
            // exportToToolStripMenuItem
            // 
            this.exportToToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportToToolStripMenuItem.Image")));
            this.exportToToolStripMenuItem.Name = "exportToToolStripMenuItem";
            this.exportToToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exportToToolStripMenuItem.Size = new System.Drawing.Size(192, 24);
            this.exportToToolStripMenuItem.Text = "&Export To";
            this.exportToToolStripMenuItem.Click += new System.EventHandler(this.Export);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(189, 6);
            // 
            // printToolStripMenuItem1
            // 
            this.printToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem1.Image")));
            this.printToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem1.Name = "printToolStripMenuItem1";
            this.printToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem1.Size = new System.Drawing.Size(192, 24);
            this.printToolStripMenuItem1.Text = "&Print";
            this.printToolStripMenuItem1.Click += new System.EventHandler(this.PrintCards);
            // 
            // printPreviewToolStripMenuItem1
            // 
            this.printPreviewToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem1.Image")));
            this.printPreviewToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem1.Name = "printPreviewToolStripMenuItem1";
            this.printPreviewToolStripMenuItem1.Size = new System.Drawing.Size(192, 24);
            this.printPreviewToolStripMenuItem1.Text = "Print Pre&view";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(189, 6);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(192, 24);
            this.exitToolStripMenuItem1.Text = "E&xit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.Exit);
            // 
            // cardsToolStripMenuItem
            // 
            this.cardsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCardToolStripMenuItem,
            this.deleteCardToolStripMenuItem,
            this.toolStripSeparator5,
            this.newCustomCardToolStripMenuItem});
            this.cardsToolStripMenuItem.Name = "cardsToolStripMenuItem";
            this.cardsToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.cardsToolStripMenuItem.Text = "Car&ds";
            // 
            // addCardToolStripMenuItem
            // 
            this.addCardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.featSkillToolStripMenuItem,
            this.heroSheetToolStripMenuItem,
            this.itemToolStripMenuItem,
            this.monsterToolStripMenuItem,
            this.overlordToolStripMenuItem});
            this.addCardToolStripMenuItem.Name = "addCardToolStripMenuItem";
            this.addCardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.addCardToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.addCardToolStripMenuItem.Text = "&Add Card";
            // 
            // featSkillToolStripMenuItem
            // 
            this.featSkillToolStripMenuItem.Name = "featSkillToolStripMenuItem";
            this.featSkillToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.featSkillToolStripMenuItem.Text = "Feat/Skill";
            this.featSkillToolStripMenuItem.Click += new System.EventHandler(this.AddFeatSkill);
            // 
            // heroSheetToolStripMenuItem
            // 
            this.heroSheetToolStripMenuItem.Name = "heroSheetToolStripMenuItem";
            this.heroSheetToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.heroSheetToolStripMenuItem.Text = "HeroSheet";
            this.heroSheetToolStripMenuItem.Click += new System.EventHandler(this.AddHeroSheet);
            // 
            // itemToolStripMenuItem
            // 
            this.itemToolStripMenuItem.Name = "itemToolStripMenuItem";
            this.itemToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.itemToolStripMenuItem.Text = "Item";
            this.itemToolStripMenuItem.Click += new System.EventHandler(this.AddItem);
            // 
            // monsterToolStripMenuItem
            // 
            this.monsterToolStripMenuItem.Name = "monsterToolStripMenuItem";
            this.monsterToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.monsterToolStripMenuItem.Text = "Monster";
            this.monsterToolStripMenuItem.Click += new System.EventHandler(this.AddMonster);
            // 
            // overlordToolStripMenuItem
            // 
            this.overlordToolStripMenuItem.Name = "overlordToolStripMenuItem";
            this.overlordToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.overlordToolStripMenuItem.Text = "Overlord";
            this.overlordToolStripMenuItem.Click += new System.EventHandler(this.AddOverlord);
            // 
            // deleteCardToolStripMenuItem
            // 
            this.deleteCardToolStripMenuItem.Name = "deleteCardToolStripMenuItem";
            this.deleteCardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.deleteCardToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.deleteCardToolStripMenuItem.Text = "&Remove Card";
            this.deleteCardToolStripMenuItem.Click += new System.EventHandler(this.RemoveCard);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(215, 6);
            // 
            // newCustomCardToolStripMenuItem
            // 
            this.newCustomCardToolStripMenuItem.Enabled = false;
            this.newCustomCardToolStripMenuItem.Name = "newCustomCardToolStripMenuItem";
            this.newCustomCardToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.newCustomCardToolStripMenuItem.Text = "New Custom Card";
            // 
            // formatToolStripMenuItem
            // 
            this.formatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.boldToolStripMenuItem,
            this.italicToolStripMenuItem,
            this.underlineToolStripMenuItem,
            this.surgeToolStripMenuItem});
            this.formatToolStripMenuItem.Name = "formatToolStripMenuItem";
            this.formatToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.formatToolStripMenuItem.Text = "For&mat";
            // 
            // boldToolStripMenuItem
            // 
            this.boldToolStripMenuItem.Name = "boldToolStripMenuItem";
            this.boldToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.boldToolStripMenuItem.Size = new System.Drawing.Size(194, 24);
            this.boldToolStripMenuItem.Text = "Bold";
            this.boldToolStripMenuItem.Click += new System.EventHandler(this.boldToolStripButton_Click);
            // 
            // italicToolStripMenuItem
            // 
            this.italicToolStripMenuItem.Name = "italicToolStripMenuItem";
            this.italicToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.italicToolStripMenuItem.Size = new System.Drawing.Size(194, 24);
            this.italicToolStripMenuItem.Text = "Italic";
            this.italicToolStripMenuItem.Click += new System.EventHandler(this.italicToolStripButton_Click);
            // 
            // underlineToolStripMenuItem
            // 
            this.underlineToolStripMenuItem.Name = "underlineToolStripMenuItem";
            this.underlineToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.underlineToolStripMenuItem.Size = new System.Drawing.Size(194, 24);
            this.underlineToolStripMenuItem.Text = "Underline";
            this.underlineToolStripMenuItem.Click += new System.EventHandler(this.understrikeToolStripButton_Click);
            // 
            // surgeToolStripMenuItem
            // 
            this.surgeToolStripMenuItem.Name = "surgeToolStripMenuItem";
            this.surgeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.surgeToolStripMenuItem.Size = new System.Drawing.Size(194, 24);
            this.surgeToolStripMenuItem.Text = "Surge";
            this.surgeToolStripMenuItem.Click += new System.EventHandler(this.surgeToolStripButton_Click);
            // 
            // optionToolStripMenuItem
            // 
            this.optionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setExpansionIconToolStripMenuItem,
            this.languageToolStripMenuItem});
            this.optionToolStripMenuItem.Name = "optionToolStripMenuItem";
            this.optionToolStripMenuItem.Size = new System.Drawing.Size(67, 24);
            this.optionToolStripMenuItem.Text = "&Option";
            // 
            // setExpansionIconToolStripMenuItem
            // 
            this.setExpansionIconToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeNameToolStripMenuItem,
            this.changeImageToolStripMenuItem});
            this.setExpansionIconToolStripMenuItem.Name = "setExpansionIconToolStripMenuItem";
            this.setExpansionIconToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.setExpansionIconToolStripMenuItem.Text = "Card Set Options";
            // 
            // changeNameToolStripMenuItem
            // 
            this.changeNameToolStripMenuItem.Name = "changeNameToolStripMenuItem";
            this.changeNameToolStripMenuItem.Size = new System.Drawing.Size(174, 24);
            this.changeNameToolStripMenuItem.Text = "Change Name";
            this.changeNameToolStripMenuItem.Click += new System.EventHandler(this.changeNameToolStripMenuItem_Click);
            // 
            // changeImageToolStripMenuItem
            // 
            this.changeImageToolStripMenuItem.Name = "changeImageToolStripMenuItem";
            this.changeImageToolStripMenuItem.Size = new System.Drawing.Size(174, 24);
            this.changeImageToolStripMenuItem.Text = "Change Image";
            this.changeImageToolStripMenuItem.Click += new System.EventHandler(this.changeImageToolStripMenuItem_Click);
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishToolStripMenuItem});
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            this.languageToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.languageToolStripMenuItem.Text = "Language";
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            this.englishToolStripMenuItem.Size = new System.Drawing.Size(125, 24);
            this.englishToolStripMenuItem.Text = "English";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1,
            this.donateToolStripMenuItem,
            this.endUserAgreementToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(214, 24);
            this.aboutToolStripMenuItem1.Text = "&About...";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.About);
            // 
            // donateToolStripMenuItem
            // 
            this.donateToolStripMenuItem.Name = "donateToolStripMenuItem";
            this.donateToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.donateToolStripMenuItem.Text = "Donate";
            this.donateToolStripMenuItem.Click += new System.EventHandler(this.donateToolStripMenuItem_Click);
            // 
            // endUserAgreementToolStripMenuItem
            // 
            this.endUserAgreementToolStripMenuItem.Name = "endUserAgreementToolStripMenuItem";
            this.endUserAgreementToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.endUserAgreementToolStripMenuItem.Text = "End User Agreement";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 93;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 104;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Width = 101;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripButton1,
            this.printToolStripButton,
            this.toolStripSeparator,
            this.toolStripSplitButton_AddCard,
            this.removeCardToolStripButton,
            this.toolStripLabel1,
            this.toolStripNumericUpDown_Cards,
            this.toolStripSeparator2,
            this.toolStripTextBox1,
            this.toolStripSeparator1,
            this.boldToolStripButton,
            this.italicToolStripButton,
            this.understrikeToolStripButton,
            this.surgeToolStripButton,
            this.toolStripNumericUpDown_TextSize});
            this.toolStrip1.Location = new System.Drawing.Point(0, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1045, 30);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "x";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.newToolStripButton.Text = "&New";
            this.newToolStripButton.Click += new System.EventHandler(this.CreateNewDatabase);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.openToolStripButton.Text = "&Open";
            this.openToolStripButton.Click += new System.EventHandler(this.LoadDataBase);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.SaveDataBase);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 27);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Export";
            this.toolStripButton1.Click += new System.EventHandler(this.Export);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.printToolStripButton.Text = "&Print";
            this.printToolStripButton.Click += new System.EventHandler(this.PrintCards);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButton_AddCard
            // 
            this.toolStripSplitButton_AddCard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton_AddCard.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Feat,
            this.toolStripMenuItem_Hero,
            this.toolStripMenuItem_Item,
            this.toolStripMenuItem_monster,
            this.toolStripMenuItem_Overlord});
            this.toolStripSplitButton_AddCard.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton_AddCard.Image")));
            this.toolStripSplitButton_AddCard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton_AddCard.Name = "toolStripSplitButton_AddCard";
            this.toolStripSplitButton_AddCard.Size = new System.Drawing.Size(32, 27);
            this.toolStripSplitButton_AddCard.Text = "&Add";
            this.toolStripSplitButton_AddCard.Click += new System.EventHandler(this.AddNewCardSplitButton);
            // 
            // toolStripMenuItem_Feat
            // 
            this.toolStripMenuItem_Feat.Name = "toolStripMenuItem_Feat";
            this.toolStripMenuItem_Feat.Size = new System.Drawing.Size(148, 24);
            this.toolStripMenuItem_Feat.Text = "Feat/&Skill";
            this.toolStripMenuItem_Feat.Click += new System.EventHandler(this.AddFeatSkill);
            // 
            // toolStripMenuItem_Hero
            // 
            this.toolStripMenuItem_Hero.Name = "toolStripMenuItem_Hero";
            this.toolStripMenuItem_Hero.Size = new System.Drawing.Size(148, 24);
            this.toolStripMenuItem_Hero.Text = "Hero&Sheet";
            this.toolStripMenuItem_Hero.Click += new System.EventHandler(this.AddHeroSheet);
            // 
            // toolStripMenuItem_Item
            // 
            this.toolStripMenuItem_Item.Name = "toolStripMenuItem_Item";
            this.toolStripMenuItem_Item.Size = new System.Drawing.Size(148, 24);
            this.toolStripMenuItem_Item.Text = "&Item";
            this.toolStripMenuItem_Item.Click += new System.EventHandler(this.AddItem);
            // 
            // toolStripMenuItem_monster
            // 
            this.toolStripMenuItem_monster.Name = "toolStripMenuItem_monster";
            this.toolStripMenuItem_monster.Size = new System.Drawing.Size(148, 24);
            this.toolStripMenuItem_monster.Text = "M&onster";
            this.toolStripMenuItem_monster.Click += new System.EventHandler(this.AddMonster);
            // 
            // toolStripMenuItem_Overlord
            // 
            this.toolStripMenuItem_Overlord.Name = "toolStripMenuItem_Overlord";
            this.toolStripMenuItem_Overlord.Size = new System.Drawing.Size(148, 24);
            this.toolStripMenuItem_Overlord.Text = "Over&lord";
            this.toolStripMenuItem_Overlord.Click += new System.EventHandler(this.AddOverlord);
            // 
            // removeCardToolStripButton
            // 
            this.removeCardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.removeCardToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("removeCardToolStripButton.Image")));
            this.removeCardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeCardToolStripButton.Name = "removeCardToolStripButton";
            this.removeCardToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.removeCardToolStripButton.Text = "&Remove";
            this.removeCardToolStripButton.Click += new System.EventHandler(this.RemoveCard);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(122, 27);
            this.toolStripLabel1.Text = "Number of Cards";
            // 
            // toolStripNumericUpDown_Cards
            // 
            this.toolStripNumericUpDown_Cards.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.toolStripNumericUpDown_Cards.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.toolStripNumericUpDown_Cards.Name = "toolStripNumericUpDown_Cards";
            this.toolStripNumericUpDown_Cards.Size = new System.Drawing.Size(49, 27);
            this.toolStripNumericUpDown_Cards.Text = "1";
            this.toolStripNumericUpDown_Cards.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.toolStripNumericUpDown_Cards.Click += new System.EventHandler(this.toolStripNumericUpDown_Cards_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(132, 30);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // boldToolStripButton
            // 
            this.boldToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.boldToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.boldToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("boldToolStripButton.Image")));
            this.boldToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.boldToolStripButton.Name = "boldToolStripButton";
            this.boldToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.boldToolStripButton.Text = "B";
            this.boldToolStripButton.Click += new System.EventHandler(this.boldToolStripButton_Click);
            // 
            // italicToolStripButton
            // 
            this.italicToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.italicToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic);
            this.italicToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("italicToolStripButton.Image")));
            this.italicToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.italicToolStripButton.Name = "italicToolStripButton";
            this.italicToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.italicToolStripButton.Text = "I";
            this.italicToolStripButton.Click += new System.EventHandler(this.italicToolStripButton_Click);
            // 
            // understrikeToolStripButton
            // 
            this.understrikeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.understrikeToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Underline);
            this.understrikeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("understrikeToolStripButton.Image")));
            this.understrikeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.understrikeToolStripButton.Name = "understrikeToolStripButton";
            this.understrikeToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.understrikeToolStripButton.Text = "U";
            this.understrikeToolStripButton.Click += new System.EventHandler(this.understrikeToolStripButton_Click);
            // 
            // surgeToolStripButton
            // 
            this.surgeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.surgeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("surgeToolStripButton.Image")));
            this.surgeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.surgeToolStripButton.Name = "surgeToolStripButton";
            this.surgeToolStripButton.Size = new System.Drawing.Size(23, 27);
            this.surgeToolStripButton.Text = "S";
            this.surgeToolStripButton.Click += new System.EventHandler(this.surgeToolStripButton_Click);
            // 
            // toolStripNumericUpDown_TextSize
            // 
            this.toolStripNumericUpDown_TextSize.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.toolStripNumericUpDown_TextSize.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.toolStripNumericUpDown_TextSize.Name = "toolStripNumericUpDown_TextSize";
            this.toolStripNumericUpDown_TextSize.Size = new System.Drawing.Size(73, 27);
            this.toolStripNumericUpDown_TextSize.Text = "10";
            this.toolStripNumericUpDown_TextSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.toolStripNumericUpDown_TextSize.ValueChanged += new System.EventHandler(this.toolStripNumericUpDown_TextSize_ValueChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 58);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1045, 634);
            this.splitContainer1.SplitterDistance = 258;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 6;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.treeView_Databases);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer3.Size = new System.Drawing.Size(258, 634);
            this.splitContainer3.SplitterDistance = 198;
            this.splitContainer3.SplitterWidth = 5;
            this.splitContainer3.TabIndex = 0;
            // 
            // treeView_Databases
            // 
            this.treeView_Databases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_Databases.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView_Databases.Location = new System.Drawing.Point(0, 0);
            this.treeView_Databases.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.treeView_Databases.Name = "treeView_Databases";
            this.treeView_Databases.Size = new System.Drawing.Size(254, 194);
            this.treeView_Databases.TabIndex = 0;
            this.treeView_Databases.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_Databases_AfterSelect);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(254, 427);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.listView_Cards);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.featandSkill1);
            this.splitContainer2.Panel2.Controls.Add(this.items1);
            this.splitContainer2.Panel2.Controls.Add(this.overlord1);
            this.splitContainer2.Panel2.Controls.Add(this.monster1);
            this.splitContainer2.Panel2.Controls.Add(this.heroSheet1);
            this.splitContainer2.Size = new System.Drawing.Size(782, 634);
            this.splitContainer2.SplitterDistance = 198;
            this.splitContainer2.SplitterWidth = 5;
            this.splitContainer2.TabIndex = 0;
            // 
            // listView_Cards
            // 
            this.listView_Cards.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listView_Cards.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_Cards.FullRowSelect = true;
            this.listView_Cards.GridLines = true;
            this.listView_Cards.Location = new System.Drawing.Point(0, 0);
            this.listView_Cards.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listView_Cards.MultiSelect = false;
            this.listView_Cards.Name = "listView_Cards";
            this.listView_Cards.Size = new System.Drawing.Size(778, 194);
            this.listView_Cards.TabIndex = 0;
            this.listView_Cards.UseCompatibleStateImageBehavior = false;
            this.listView_Cards.View = System.Windows.Forms.View.Details;
            this.listView_Cards.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView_Cards_ColumnClick);
            this.listView_Cards.SelectedIndexChanged += new System.EventHandler(this.listView_Cards_SelectedIndexChanged);
            // 
            // featandSkill1
            // 
            this.featandSkill1.AutoScroll = true;
            this.featandSkill1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.featandSkill1.ForcusedRTFControl = null;
            this.featandSkill1.Location = new System.Drawing.Point(0, 0);
            this.featandSkill1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.featandSkill1.Name = "featandSkill1";
            this.featandSkill1.ParentOwner = null;
            this.featandSkill1.Size = new System.Drawing.Size(778, 427);
            this.featandSkill1.TabIndex = 5;
            this.featandSkill1.ThisControl = this.featandSkill1;
            // 
            // items1
            // 
            this.items1.AutoScroll = true;
            this.items1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.items1.Location = new System.Drawing.Point(0, 0);
            this.items1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.items1.Name = "items1";
            this.items1.ParentOwner = null;
            this.items1.Size = new System.Drawing.Size(778, 427);
            this.items1.TabIndex = 0;
            this.items1.ThisControl = this.items1;
            // 
            // overlord1
            // 
            this.overlord1.AutoScroll = true;
            this.overlord1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overlord1.Location = new System.Drawing.Point(0, 0);
            this.overlord1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.overlord1.Name = "overlord1";
            this.overlord1.ParentOwner = null;
            this.overlord1.Size = new System.Drawing.Size(778, 427);
            this.overlord1.TabIndex = 1;
            this.overlord1.ThisControl = this.overlord1;
            // 
            // monster1
            // 
            this.monster1.AutoScroll = true;
            this.monster1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monster1.Location = new System.Drawing.Point(0, 0);
            this.monster1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.monster1.Name = "monster1";
            this.monster1.ParentOwner = null;
            this.monster1.Size = new System.Drawing.Size(778, 427);
            this.monster1.TabIndex = 2;
            this.monster1.ThisControl = this.monster1;
            // 
            // heroSheet1
            // 
            this.heroSheet1.AutoScroll = true;
            this.heroSheet1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.heroSheet1.Location = new System.Drawing.Point(0, 0);
            this.heroSheet1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.heroSheet1.Name = "heroSheet1";
            this.heroSheet1.ParentOwner = null;
            this.heroSheet1.Size = new System.Drawing.Size(778, 427);
            this.heroSheet1.TabIndex = 4;
            this.heroSheet1.ThisControl = this.heroSheet1;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // backgroundWorkerPictureUpdater
            // 
            this.backgroundWorkerPictureUpdater.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // backgroundWorkerListViewUpdater
            // 
            this.backgroundWorkerListViewUpdater.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerListViewUpdater_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 692);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(1061, 728);
            this.Name = "MainForm";
            this.Text = "Jacks Descent Card Creator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ToolStripMenuItem cardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem newCustomCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem surgeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setExpansionIconToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.TreeView treeView_Databases;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton boldToolStripButton;
        private System.Windows.Forms.ToolStripButton italicToolStripButton;
        private System.Windows.Forms.ToolStripButton understrikeToolStripButton;
        private System.Windows.Forms.ToolStripButton surgeToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private Items items1;
        private System.Windows.Forms.ToolStripButton removeCardToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton_AddCard;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Feat;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Hero;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Item;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_monster;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Overlord;
        private Overlord overlord1;
        private Monsters monster1;
        private System.Windows.Forms.ToolStripMenuItem exportToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featSkillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heroSheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monsterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overlordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem donateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem endUserAgreementToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private ToolStripNumericUpDown toolStripNumericUpDown_TextSize;
        private HeroSheets heroSheet1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ListView listView_Cards;
        public ToolStripNumericUpDown toolStripNumericUpDown_Cards;
        public System.ComponentModel.BackgroundWorker backgroundWorkerPictureUpdater;
        public System.ComponentModel.BackgroundWorker backgroundWorkerListViewUpdater;
        private System.Windows.Forms.ToolStripMenuItem changeNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeImageToolStripMenuItem;
        private FeatandSkill featandSkill1;
    }
}