﻿namespace DescentCardCreator
{
    partial class Items
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Item_Special = new System.Windows.Forms.Label();
            this.label_Attack_Type = new System.Windows.Forms.Label();
            this.comboBox_Attack_Type = new System.Windows.Forms.ComboBox();
            this.comboBox_Item_Type = new System.Windows.Forms.ComboBox();
            this.label_Gold = new System.Windows.Forms.Label();
            this.numericUpDown_Coins = new System.Windows.Forms.NumericUpDown();
            this.checkBox_2Hand = new System.Windows.Forms.CheckBox();
            this.checkBox_1Hand = new System.Windows.Forms.CheckBox();
            this.label_Item_Type = new System.Windows.Forms.Label();
            this.button_Browse_Picture = new System.Windows.Forms.Button();
            this.textBox_Picture = new System.Windows.Forms.TextBox();
            this.label_Picture = new System.Windows.Forms.Label();
            this.label_Name = new System.Windows.Forms.Label();
            this.label_Thread = new System.Windows.Forms.Label();
            this.numericUpDown_Thread = new System.Windows.Forms.NumericUpDown();
            this.richTextBox_Name = new System.Windows.Forms.RichTextBox();
            this.richTextBox_Text = new System.Windows.Forms.RichTextBox();
            this.comboBox_CardType = new System.Windows.Forms.ComboBox();
            this.label_CardType = new System.Windows.Forms.Label();
            this.diceControl_Dice = new DescentCardCreator.DiceControl();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Coins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Thread)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Item_Special
            // 
            this.label_Item_Special.AutoSize = true;
            this.label_Item_Special.BackColor = System.Drawing.Color.Transparent;
            this.label_Item_Special.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Item_Special.Location = new System.Drawing.Point(3, 242);
            this.label_Item_Special.Name = "label_Item_Special";
            this.label_Item_Special.Size = new System.Drawing.Size(97, 20);
            this.label_Item_Special.TabIndex = 72;
            this.label_Item_Special.Text = "Item Special";
            // 
            // label_Attack_Type
            // 
            this.label_Attack_Type.AutoSize = true;
            this.label_Attack_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Attack_Type.Location = new System.Drawing.Point(341, 188);
            this.label_Attack_Type.Name = "label_Attack_Type";
            this.label_Attack_Type.Size = new System.Drawing.Size(93, 20);
            this.label_Attack_Type.TabIndex = 71;
            this.label_Attack_Type.Text = "Attack Type";
            // 
            // comboBox_Attack_Type
            // 
            this.comboBox_Attack_Type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Attack_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Attack_Type.FormattingEnabled = true;
            this.comboBox_Attack_Type.Items.AddRange(new object[] {
            "Magic",
            "Melee",
            "Ranged"});
            this.comboBox_Attack_Type.Location = new System.Drawing.Point(343, 211);
            this.comboBox_Attack_Type.Name = "comboBox_Attack_Type";
            this.comboBox_Attack_Type.Size = new System.Drawing.Size(333, 28);
            this.comboBox_Attack_Type.TabIndex = 70;
            this.comboBox_Attack_Type.TextChanged += new System.EventHandler(this.comboBox_Attack_Type_TextChanged);
            // 
            // comboBox_Item_Type
            // 
            this.comboBox_Item_Type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Item_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Item_Type.FormattingEnabled = true;
            this.comboBox_Item_Type.Items.AddRange(new object[] {
            "Item Type",
            "Armor",
            "Other",
            "Other - Rune",
            "Shield",
            "Weapon",
            "Weapon - Rune",
            "Armor - Cursed",
            "Other - Cursed",
            "Shield - Cursed",
            "Weapon  - Cursed"});
            this.comboBox_Item_Type.Location = new System.Drawing.Point(3, 211);
            this.comboBox_Item_Type.Name = "comboBox_Item_Type";
            this.comboBox_Item_Type.Size = new System.Drawing.Size(333, 28);
            this.comboBox_Item_Type.TabIndex = 69;
            this.comboBox_Item_Type.TextChanged += new System.EventHandler(this.comboBox_Item_Type_TextChanged);
            // 
            // label_Gold
            // 
            this.label_Gold.AutoSize = true;
            this.label_Gold.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Gold.Location = new System.Drawing.Point(225, 429);
            this.label_Gold.Name = "label_Gold";
            this.label_Gold.Size = new System.Drawing.Size(49, 20);
            this.label_Gold.TabIndex = 68;
            this.label_Gold.Text = "Coins";
            // 
            // numericUpDown_Coins
            // 
            this.numericUpDown_Coins.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Coins.Location = new System.Drawing.Point(175, 427);
            this.numericUpDown_Coins.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Coins.Name = "numericUpDown_Coins";
            this.numericUpDown_Coins.Size = new System.Drawing.Size(44, 26);
            this.numericUpDown_Coins.TabIndex = 67;
            this.numericUpDown_Coins.ValueChanged += new System.EventHandler(this.numericUpDown_Coins_ValueChanged);
            // 
            // checkBox_2Hand
            // 
            this.checkBox_2Hand.AutoSize = true;
            this.checkBox_2Hand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_2Hand.Location = new System.Drawing.Point(89, 428);
            this.checkBox_2Hand.Name = "checkBox_2Hand";
            this.checkBox_2Hand.Size = new System.Drawing.Size(80, 24);
            this.checkBox_2Hand.TabIndex = 66;
            this.checkBox_2Hand.Text = "2 Hand";
            this.checkBox_2Hand.UseVisualStyleBackColor = true;
            this.checkBox_2Hand.CheckedChanged += new System.EventHandler(this.checkBox_2Hand_CheckedChanged);
            // 
            // checkBox_1Hand
            // 
            this.checkBox_1Hand.AutoSize = true;
            this.checkBox_1Hand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_1Hand.Location = new System.Drawing.Point(3, 428);
            this.checkBox_1Hand.Name = "checkBox_1Hand";
            this.checkBox_1Hand.Size = new System.Drawing.Size(80, 24);
            this.checkBox_1Hand.TabIndex = 65;
            this.checkBox_1Hand.Text = "1 Hand";
            this.checkBox_1Hand.UseVisualStyleBackColor = true;
            this.checkBox_1Hand.CheckedChanged += new System.EventHandler(this.checkBox_1Hand_CheckedChanged);
            // 
            // label_Item_Type
            // 
            this.label_Item_Type.AutoSize = true;
            this.label_Item_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Item_Type.Location = new System.Drawing.Point(3, 188);
            this.label_Item_Type.Name = "label_Item_Type";
            this.label_Item_Type.Size = new System.Drawing.Size(79, 20);
            this.label_Item_Type.TabIndex = 64;
            this.label_Item_Type.Text = "Item Type";
            // 
            // button_Browse_Picture
            // 
            this.button_Browse_Picture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Browse_Picture.Location = new System.Drawing.Point(601, 162);
            this.button_Browse_Picture.Name = "button_Browse_Picture";
            this.button_Browse_Picture.Size = new System.Drawing.Size(75, 23);
            this.button_Browse_Picture.TabIndex = 63;
            this.button_Browse_Picture.Text = "Browse";
            this.button_Browse_Picture.UseVisualStyleBackColor = true;
            this.button_Browse_Picture.Click += new System.EventHandler(this.button_Browse_Picture_Click);
            // 
            // textBox_Picture
            // 
            this.textBox_Picture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Picture.Location = new System.Drawing.Point(3, 159);
            this.textBox_Picture.Name = "textBox_Picture";
            this.textBox_Picture.Size = new System.Drawing.Size(592, 26);
            this.textBox_Picture.TabIndex = 62;
            this.textBox_Picture.TextChanged += new System.EventHandler(this.textBox_Picture_TextChanged);
            // 
            // label_Picture
            // 
            this.label_Picture.AutoSize = true;
            this.label_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Picture.Location = new System.Drawing.Point(3, 136);
            this.label_Picture.Name = "label_Picture";
            this.label_Picture.Size = new System.Drawing.Size(58, 20);
            this.label_Picture.TabIndex = 59;
            this.label_Picture.Text = "Picture";
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.Location = new System.Drawing.Point(3, 54);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(51, 20);
            this.label_Name.TabIndex = 57;
            this.label_Name.Text = "Name";
            // 
            // label_Thread
            // 
            this.label_Thread.AutoSize = true;
            this.label_Thread.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Thread.Location = new System.Drawing.Point(365, 429);
            this.label_Thread.Name = "label_Thread";
            this.label_Thread.Size = new System.Drawing.Size(190, 20);
            this.label_Thread.TabIndex = 99;
            this.label_Thread.Text = "Thread (Dark Relics only!)";
            // 
            // numericUpDown_Thread
            // 
            this.numericUpDown_Thread.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Thread.Location = new System.Drawing.Point(315, 427);
            this.numericUpDown_Thread.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Thread.Name = "numericUpDown_Thread";
            this.numericUpDown_Thread.Size = new System.Drawing.Size(44, 26);
            this.numericUpDown_Thread.TabIndex = 98;
            this.numericUpDown_Thread.ValueChanged += new System.EventHandler(this.numericUpDown_Thread_ValueChanged);
            // 
            // richTextBox_Name
            // 
            this.richTextBox_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Name.Font = new System.Drawing.Font("Kelmscott", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_Name.Location = new System.Drawing.Point(3, 77);
            this.richTextBox_Name.Multiline = false;
            this.richTextBox_Name.Name = "richTextBox_Name";
            this.richTextBox_Name.Size = new System.Drawing.Size(671, 56);
            this.richTextBox_Name.TabIndex = 100;
            this.richTextBox_Name.Text = "";
            this.richTextBox_Name.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // richTextBox_Text
            // 
            this.richTextBox_Text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Text.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_Text.Location = new System.Drawing.Point(3, 265);
            this.richTextBox_Text.Name = "richTextBox_Text";
            this.richTextBox_Text.Size = new System.Drawing.Size(671, 122);
            this.richTextBox_Text.TabIndex = 101;
            this.richTextBox_Text.Text = "";
            this.richTextBox_Text.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            // 
            // comboBox_CardType
            // 
            this.comboBox_CardType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_CardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_CardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_CardType.FormattingEnabled = true;
            this.comboBox_CardType.Items.AddRange(new object[] {
            "ShopItem",
            "Artifact",
            "Dark Relic",
            "Party Upgrade",
            "Copper Treasure",
            "Silver Treasure",
            "Gold Treasure"});
            this.comboBox_CardType.Location = new System.Drawing.Point(3, 23);
            this.comboBox_CardType.Name = "comboBox_CardType";
            this.comboBox_CardType.Size = new System.Drawing.Size(673, 28);
            this.comboBox_CardType.TabIndex = 103;
            this.comboBox_CardType.SelectedIndexChanged += new System.EventHandler(this.comboBox_CardType_SelectedIndexChanged);
            // 
            // label_CardType
            // 
            this.label_CardType.AutoSize = true;
            this.label_CardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CardType.Location = new System.Drawing.Point(3, 0);
            this.label_CardType.Name = "label_CardType";
            this.label_CardType.Size = new System.Drawing.Size(81, 20);
            this.label_CardType.TabIndex = 102;
            this.label_CardType.Text = "Card Type";
            // 
            // diceControl_Dice
            // 
            this.diceControl_Dice.Location = new System.Drawing.Point(2, 392);
            this.diceControl_Dice.Margin = new System.Windows.Forms.Padding(2);
            this.diceControl_Dice.Name = "diceControl_Dice";
            this.diceControl_Dice.Size = new System.Drawing.Size(675, 31);
            this.diceControl_Dice.TabIndex = 104;
            // 
            // Items
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.diceControl_Dice);
            this.Controls.Add(this.comboBox_CardType);
            this.Controls.Add(this.label_CardType);
            this.Controls.Add(this.richTextBox_Text);
            this.Controls.Add(this.richTextBox_Name);
            this.Controls.Add(this.label_Thread);
            this.Controls.Add(this.numericUpDown_Thread);
            this.Controls.Add(this.label_Item_Special);
            this.Controls.Add(this.label_Attack_Type);
            this.Controls.Add(this.comboBox_Attack_Type);
            this.Controls.Add(this.comboBox_Item_Type);
            this.Controls.Add(this.label_Gold);
            this.Controls.Add(this.numericUpDown_Coins);
            this.Controls.Add(this.checkBox_2Hand);
            this.Controls.Add(this.checkBox_1Hand);
            this.Controls.Add(this.label_Item_Type);
            this.Controls.Add(this.button_Browse_Picture);
            this.Controls.Add(this.textBox_Picture);
            this.Controls.Add(this.label_Picture);
            this.Controls.Add(this.label_Name);
            this.DoubleBuffered = true;
            this.Name = "Items";
            this.Size = new System.Drawing.Size(679, 465);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Coins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Thread)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Item_Special;
        private System.Windows.Forms.Label label_Attack_Type;
        private System.Windows.Forms.ComboBox comboBox_Attack_Type;
        private System.Windows.Forms.ComboBox comboBox_Item_Type;
        private System.Windows.Forms.Label label_Gold;
        private System.Windows.Forms.NumericUpDown numericUpDown_Coins;
        private System.Windows.Forms.CheckBox checkBox_2Hand;
        private System.Windows.Forms.CheckBox checkBox_1Hand;
        private System.Windows.Forms.Label label_Item_Type;
        private System.Windows.Forms.Button button_Browse_Picture;
        private System.Windows.Forms.TextBox textBox_Picture;
        private System.Windows.Forms.Label label_Picture;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_Thread;
        private System.Windows.Forms.NumericUpDown numericUpDown_Thread;
        private System.Windows.Forms.RichTextBox richTextBox_Name;
        private System.Windows.Forms.RichTextBox richTextBox_Text;
        private System.Windows.Forms.ComboBox comboBox_CardType;
        private System.Windows.Forms.Label label_CardType;
        private DiceControl diceControl_Dice;
    }
}
