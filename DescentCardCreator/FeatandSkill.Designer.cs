﻿namespace DescentCardCreator
{
    partial class FeatandSkill
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_CardText = new System.Windows.Forms.Label();
            this.comboBox_CardType = new System.Windows.Forms.ComboBox();
            this.label_CardType = new System.Windows.Forms.Label();
            this.label_Name = new System.Windows.Forms.Label();
            this.richTextBox_Text = new System.Windows.Forms.RichTextBox();
            this.richTextBox_Name = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label_CardText
            // 
            this.label_CardText.AutoSize = true;
            this.label_CardText.BackColor = System.Drawing.Color.Transparent;
            this.label_CardText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CardText.Location = new System.Drawing.Point(3, 136);
            this.label_CardText.Name = "label_CardText";
            this.label_CardText.Size = new System.Drawing.Size(77, 20);
            this.label_CardText.TabIndex = 114;
            this.label_CardText.Text = "Card Text";
            // 
            // comboBox_CardType
            // 
            this.comboBox_CardType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_CardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_CardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_CardType.FormattingEnabled = true;
            this.comboBox_CardType.Items.AddRange(new object[] {
            "Feat Fighting",
            "Feat Subterfuge",
            "Feat Wizardary",
            "Skill Fighting",
            "Skill Subterfuge",
            "Skill Wizardary"});
            this.comboBox_CardType.Location = new System.Drawing.Point(3, 23);
            this.comboBox_CardType.Name = "comboBox_CardType";
            this.comboBox_CardType.Size = new System.Drawing.Size(284, 28);
            this.comboBox_CardType.TabIndex = 113;
            this.comboBox_CardType.SelectedIndexChanged += new System.EventHandler(this.comboBox_CardType_SelectedIndexChanged);
            // 
            // label_CardType
            // 
            this.label_CardType.AutoSize = true;
            this.label_CardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CardType.Location = new System.Drawing.Point(3, 0);
            this.label_CardType.Name = "label_CardType";
            this.label_CardType.Size = new System.Drawing.Size(81, 20);
            this.label_CardType.TabIndex = 112;
            this.label_CardType.Text = "Card Type";
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.Location = new System.Drawing.Point(3, 54);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(51, 20);
            this.label_Name.TabIndex = 110;
            this.label_Name.Text = "Name";
            // 
            // richTextBox_Text
            // 
            this.richTextBox_Text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Text.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_Text.Location = new System.Drawing.Point(3, 159);
            this.richTextBox_Text.Name = "richTextBox_Text";
            this.richTextBox_Text.Size = new System.Drawing.Size(284, 105);
            this.richTextBox_Text.TabIndex = 116;
            this.richTextBox_Text.Text = "";
            this.richTextBox_Text.TextChanged += new System.EventHandler(this.richTextBox_Text_TextChanged);
            // 
            // richTextBox_Name
            // 
            this.richTextBox_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Name.Font = new System.Drawing.Font("Kelmscott", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_Name.Location = new System.Drawing.Point(3, 77);
            this.richTextBox_Name.Multiline = false;
            this.richTextBox_Name.Name = "richTextBox_Name";
            this.richTextBox_Name.Size = new System.Drawing.Size(284, 56);
            this.richTextBox_Name.TabIndex = 115;
            this.richTextBox_Name.Text = "";
            this.richTextBox_Name.TextChanged += new System.EventHandler(this.richTextBox_Name_TextChanged);
            // 
            // FeatandSkill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.richTextBox_Text);
            this.Controls.Add(this.richTextBox_Name);
            this.Controls.Add(this.label_CardText);
            this.Controls.Add(this.comboBox_CardType);
            this.Controls.Add(this.label_CardType);
            this.Controls.Add(this.label_Name);
            this.Name = "FeatandSkill";
            this.Size = new System.Drawing.Size(290, 267);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_CardText;
        private System.Windows.Forms.ComboBox comboBox_CardType;
        private System.Windows.Forms.Label label_CardType;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.RichTextBox richTextBox_Text;
        private System.Windows.Forms.RichTextBox richTextBox_Name;

    }
}
