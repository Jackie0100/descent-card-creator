﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Descent Card Creator Suite")]
[assembly: AssemblyDescription("A completely new app for you to create your own cards and exspansions for your descent game... Remember to check back on the forums recently for updates and good stuff!... If you love this program show it and me some love by donate a few tips for the work - Thanks in advance!")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Jacks and EdensFire Entertainment")]
[assembly: AssemblyProduct("Descent Card Creator Suite")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("71b94404-a978-4bdf-a986-2b9b5f55e476")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.1.0")]
[assembly: AssemblyFileVersion("1.1.1.0")]
[assembly: NeutralResourcesLanguageAttribute("en")]
