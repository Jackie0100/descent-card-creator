﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DescentCardGraphics;

namespace DescentCardCreator
{
    public partial class Items : UserControl, ICardDesigner
    {
        public MainForm ParentOwner { get; set; }
        public RichTextBox ForcusedRTFControl { get; set; }
        public UserControl ThisControl { get; set; }

        public Items()
        {
            InitializeComponent();
            ThisControl = this;
            comboBox_CardType.SelectedIndex = 0;
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    c.GotFocus += new EventHandler(RTFGotFocus);
                    c.LostFocus += new EventHandler(RTFLostFocus);
                    (c as RichTextBox).SelectAll();
                    (c as RichTextBox).SelectionAlignment = HorizontalAlignment.Center;
                }
            }
            ForcusedRTFControl = this.richTextBox_Name;
        }

        public void SetProps(Card c)
        {
            richTextBox_Name.Text = (c as Item).Name;
            richTextBox_Text.Rtf = (c as Item).Text;
            textBox_Picture.Text = (c as Item).ItemImage;
            comboBox_CardType.SelectedIndex = (int)(c as Item).ItemType;
            comboBox_Item_Type.Text = (c as Item).Type;
            comboBox_Attack_Type.Text = (c as Item).AttackType;
            if ((c as Item).Hand == Hands.Hands1)
            {
                checkBox_1Hand.Checked = true;
            }
            else if ((c as Item).Hand == Hands.Hands2)
            {
                checkBox_2Hand.Checked = true;
            }
            else
            {
                checkBox_1Hand.Checked = false;
                checkBox_2Hand.Checked = false;
            }
            numericUpDown_Coins.Value = (c as Item).Price;
            if (c is DarkRelic)
            {
                numericUpDown_Thread.Value = decimal.Parse((c as DarkRelic).ThreadCost);
            }
            try
            {
                diceControl_Dice.SetDice((c as Item).Dice);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Reset()
        {
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    (c as RichTextBox).Text = "";
                }
                else if (c is TextBox)
                {
                    (c as TextBox).Text = "";
                }
                else if (c is NumericUpDown)
                {
                    (c as NumericUpDown).Value = 0;
                }
                else if (c is ComboBox)
                {
                    (c as ComboBox).SelectedIndex = 0;
                }
                else if (c is CheckBox)
                {
                    (c as CheckBox).Checked = false;
                }
            }
        }

        public void RTFLostFocus(object sender, EventArgs e)
        {
            ParentOwner.HideFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        public void RTFGotFocus(object sender, EventArgs e)
        {
            ParentOwner.ShowFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        public void UpdateCard()
        {
            try
            {
                if (ParentOwner.backgroundWorkerPictureUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerPictureUpdater.CancelAsync();
                }
                if (ParentOwner.backgroundWorkerListViewUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerListViewUpdater.CancelAsync();
                }

                (ParentOwner.CurrentActiveCard as Item).Dice = diceControl_Dice.GetDice();
                (ParentOwner.CurrentActiveCard as Item).Dice.Reverse();

                ParentOwner.backgroundWorkerPictureUpdater.RunWorkerAsync();
                ParentOwner.backgroundWorkerListViewUpdater.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as Item).Name = richTextBox_Name.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void textBox_Picture_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as Item).ItemImage = textBox_Picture.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as Item).Text = richTextBox_Text.Rtf;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void numericUpDown_Coins_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as Item).Price = (int)numericUpDown_Coins.Value;
            UpdateCard();
        }

        private void numericUpDown_Thread_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as DarkRelic).ThreadCost = numericUpDown_Thread.Value.ToString();
            UpdateCard();
        }

        private void button_Browse_Picture_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            try
            {
                dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
                dialog.Title = "select an image";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    textBox_Picture.Text = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            dialog.Dispose();
        }

        private void comboBox_Item_Type_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as Item).Type = comboBox_Item_Type.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void comboBox_Attack_Type_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as Item).AttackType = comboBox_Attack_Type.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void comboBox_CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_CardType.SelectedIndex == 2)
                {
                    numericUpDown_Thread.Enabled = true;
                    DarkRelic dr = new DarkRelic();
                    dr.Name = (ParentOwner.CurrentActiveCard as Item).Name;
                    dr.Text = (ParentOwner.CurrentActiveCard as Item).Text;
                    dr.Count = (ParentOwner.CurrentActiveCard as Item).Count;
                    dr.Dice = (ParentOwner.CurrentActiveCard as Item).Dice;
                    dr.Hand = (ParentOwner.CurrentActiveCard as Item).Hand;
                    dr.ItemImage = (ParentOwner.CurrentActiveCard as Item).ItemImage;
                    dr.ItemType = (ParentOwner.CurrentActiveCard as Item).ItemType;
                    dr.Price = (ParentOwner.CurrentActiveCard as Item).Price;
                    dr.Type = (ParentOwner.CurrentActiveCard as Item).Type;
                    dr.Symbol = (ParentOwner.CurrentActiveCard as Item).Symbol;
                    dr.ThreadCost = numericUpDown_Thread.Value.ToString();
                    DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards[DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards.IndexOf(ParentOwner.CurrentActiveCard)] = dr;
                    ParentOwner.CurrentActiveCard = dr;
                }
                else
                {
                    numericUpDown_Thread.Enabled = false;
                    if (ParentOwner.CurrentActiveCard is DarkRelic)
                    {
                        Item dr = new Item();
                        dr.Name = (ParentOwner.CurrentActiveCard as Item).Name;
                        dr.Text = (ParentOwner.CurrentActiveCard as Item).Text;
                        dr.Count = (ParentOwner.CurrentActiveCard as Item).Count;
                        dr.Dice = (ParentOwner.CurrentActiveCard as Item).Dice;
                        dr.Hand = (ParentOwner.CurrentActiveCard as Item).Hand;
                        dr.ItemImage = (ParentOwner.CurrentActiveCard as Item).ItemImage;
                        dr.ItemType = (ParentOwner.CurrentActiveCard as Item).ItemType;
                        dr.Price = (ParentOwner.CurrentActiveCard as Item).Price;
                        dr.Type = (ParentOwner.CurrentActiveCard as Item).Type;
                        dr.Symbol = (ParentOwner.CurrentActiveCard as Item).Symbol;
                        DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards[DatabasesHandler.Cards.Find(s => s.SetName == ParentOwner.ActiveDatabase).SetCards.IndexOf(ParentOwner.CurrentActiveCard)] = dr;
                        ParentOwner.CurrentActiveCard = dr;
                    }
                }
                (ParentOwner.CurrentActiveCard as Item).ItemType = (ItemType)comboBox_CardType.SelectedIndex;

                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void checkBox_1Hand_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_1Hand.Checked)
            {
                checkBox_2Hand.Checked = false;
                (ParentOwner.CurrentActiveCard as Item).Hand = Hands.Hands1;
            }
            else if (!checkBox_2Hand.Checked)
            {
                (ParentOwner.CurrentActiveCard as Item).Hand = Hands.None;
            }
            UpdateCard();
        }

        private void checkBox_2Hand_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_2Hand.Checked)
            {
                checkBox_1Hand.Checked = false;
                (ParentOwner.CurrentActiveCard as Item).Hand = Hands.Hands2;
            }
            else if (!checkBox_1Hand.Checked)
            {
                (ParentOwner.CurrentActiveCard as Item).Hand = Hands.None;
            }
            UpdateCard();
        }
    }
}
