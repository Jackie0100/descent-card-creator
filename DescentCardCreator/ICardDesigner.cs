﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DescentCardCreator
{
    public interface ICardDesigner
    {
        MainForm ParentOwner { get; set; }
        System.Windows.Forms.RichTextBox ForcusedRTFControl { get; set; }
        System.Windows.Forms.UserControl ThisControl { get; set; }
        void Reset();
        void UpdateCard();
        void SetProps(DescentCardGraphics.Card c);
    }
}
