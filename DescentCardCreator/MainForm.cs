﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using DescentCardGraphics;
using System.Threading;
using System.Diagnostics;

namespace DescentCardCreator
{
    public partial class MainForm : Form
    {
        List<ICardDesigner> CardDesignControls = new List<ICardDesigner>();
        public Card CurrentActiveCard;
        public string ActiveDatabase;

        public MainForm()
        {
            InitializeComponent();
            featandSkill1.Visible = false;
            heroSheet1.Visible = false;
            items1.Visible = false;
            monster1.Visible = false;
            overlord1.Visible = false;
            HideFontStyleButtons();
            featandSkill1.ParentOwner = this;
            heroSheet1.ParentOwner = this;
            items1.ParentOwner = this;
            monster1.ParentOwner = this;
            overlord1.ParentOwner = this;
            addCardToolStripMenuItem.Enabled = false;
            toolStripSplitButton_AddCard.Enabled = false;
            removeCardToolStripButton.Enabled = false;
            deleteCardToolStripMenuItem.Enabled = false;
            toolStripNumericUpDown_Cards.Enabled = false;
            changeNameToolStripMenuItem.Enabled = false;
            changeImageToolStripMenuItem.Enabled = false;

            CardDesignControls.Add(featandSkill1);
            CardDesignControls.Add(heroSheet1);
            CardDesignControls.Add(items1);
            CardDesignControls.Add(monster1);
            CardDesignControls.Add(overlord1);

            //Monster test = new Monster("test", "test", 5, "", AttackTypes.Figthing, 3, 3, 3, 3, 3, 3, "", "", 5, MonsterLevel.Diamond, MonsterType.Beast, 5, 5, 5, 5, 5, 5, "", "", 5, MonsterLevel.Diamond);
            //pictureBox1.Image = test.DrawCard();
        }

        #region MenuItemsHandler

        private void CreateNewDatabase(object sender, EventArgs e)
        {
            string name = (Microsoft.VisualBasic.Interaction.InputBox("Enter Database Name"));

            if (name == "" || name.Trim() == "")
            {
                MessageBox.Show(this, "The database " + name + " is invalid, please choose another name!", "Invalid Name!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (TreeNode s in treeView_Databases.Nodes)
            {
                if  (s.Text == name)
                {
                    MessageBox.Show(this, "The database " + name + " does already exist, please choose another name!", "Invalid Name!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
            dialog.Title = "select an image";
            dialog.ShowDialog();
            try
            {
                DatabasesHandler.Cards.Add(new DataBase(name, dialog.FileName));
            }
            catch
            {
                DatabasesHandler.Cards.Add(new DataBase(name));
            }
            TreeNode node = new TreeNode();
            node.Name = name;
            node.Text = name;
            node.Nodes.Add("Feats/Skills");
            node.Nodes.Add("HeroSheets");
            node.Nodes.Add("Items");
            node.Nodes.Add("Monsters");
            node.Nodes.Add("Overlord");
            treeView_Databases.Nodes.Add(node);
            dialog.Dispose();
        }

        private void LoadDataBase(object sender, EventArgs e)
        {
            DataFileHandler.LoadCreatorSet();
            treeView_Databases.Nodes.Clear();
            foreach (DataBase db in DatabasesHandler.Cards)
            {
                TreeNode node = new TreeNode();
                node.Name = db.SetName;
                node.Text = db.SetName;
                node.Nodes.Add("Feats/Skills");
                node.Nodes.Add("HeroSheets");
                node.Nodes.Add("Items");
                node.Nodes.Add("Monsters");
                node.Nodes.Add("Overlord");
                treeView_Databases.Nodes.Add(node);
            }
        }

        private void SaveDataBase(object sender, EventArgs e)
        {
            DataFileHandler.SaveCreatorSet();
        }

        private void Export(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Jacks Descent Card Creator File |*.dcc;";
            ofd.Title = "select a Jacks Descent Card Creator File";
            if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            Process.Start(new ProcessStartInfo("Exporter.exe", ofd.FileName)).WaitForExit();
        }

        private void About(object sender, EventArgs e)
        {
            new AboutBox(this).Show(this);
        }

        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PrintCards(object sender, EventArgs e)
        {
            MessageBox.Show("This feature is currently unavailble, we apologies... We'd like you all to look up \"Ifran View\" which is a free tool for printing high DPI and resolution pictures.");
        }

        private void donateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Donate d = new Donate();
            d.Show(this);
        }

        #endregion

        #region CardHandler

        private void AddNewCardSplitButton(object sender, EventArgs e)
        {
            toolStripSplitButton_AddCard.ShowDropDown();
        }

        private void AddFeatSkill(object sender, EventArgs e)
        {
            //CardDesignControls[0].Reset();
            FeatSkill f = new FeatSkill();
            f.Symbol = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetIcon;
            f.Count = (int)toolStripNumericUpDown_Cards.Value;
            DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Add(f);
            treeView_Databases.SelectedNode = treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(ActiveDatabase)].Nodes[0];
            PopulateListView();
        }

        private void AddHeroSheet(object sender, EventArgs e)
        {
            //CardDesignControls[1].Reset();
            HeroSheet h = new HeroSheet();
            h.Symbol = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetIcon;
            h.Count = (int)toolStripNumericUpDown_Cards.Value;
            DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Add(h);
            treeView_Databases.SelectedNode = treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(ActiveDatabase)].Nodes[1];
            PopulateListView();
        }

        private void AddItem(object sender, EventArgs e)
        {
            //CardDesignControls[2].Reset();
            Item i = new Item();
            i.Symbol = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetIcon;
            i.Count = (int)toolStripNumericUpDown_Cards.Value;
            DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Add(i);
            treeView_Databases.SelectedNode = treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(ActiveDatabase)].Nodes[2];
            PopulateListView();
        }

        private void AddMonster(object sender, EventArgs e)
        {
            //CardDesignControls[3].Reset();
            Monster m = new Monster();
            m.Symbol = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetIcon;
            m.Count = (int)toolStripNumericUpDown_Cards.Value;
            DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Add(m);
            treeView_Databases.SelectedNode = treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(ActiveDatabase)].Nodes[3];
            PopulateListView();
        }

        private void AddOverlord(object sender, EventArgs e)
        {
            //CardDesignControls[4].Reset();
            OverLord o = new OverLord();
            o.Symbol = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetIcon;
            o.Count = (int)toolStripNumericUpDown_Cards.Value;
            DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Add(o);
            treeView_Databases.SelectedNode = treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(ActiveDatabase)].Nodes[4];
            PopulateListView();
        }

        private void RemoveCard(object sender, EventArgs e)
        {
            DeleteCard();
        }

        public void DeleteCard()
        {
            try
            {
                DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Remove(CurrentActiveCard);
                PopulateListView();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #region TextandEditorHandlers

        public void ShowFontStyleButtons()
        {
            boldToolStripButton.Enabled = true;
            italicToolStripButton.Enabled = true;
            understrikeToolStripButton.Enabled = true;
            surgeToolStripButton.Enabled = true;
            toolStripNumericUpDown_TextSize.Enabled = true;
        }

        public void HideFontStyleButtons()
        {
            boldToolStripButton.Enabled = false;
            italicToolStripButton.Enabled = false;
            understrikeToolStripButton.Enabled = false;
            surgeToolStripButton.Enabled = false;
            toolStripNumericUpDown_TextSize.Enabled = false;
        }

        private void boldToolStripButton_Click(object sender, EventArgs e)
        {
            ICardDesigner icd = CardDesignControls.Find(s => s.ThisControl.Visible);
            if (icd.ForcusedRTFControl.SelectionFont.Bold)
            {
                icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont, FontStyle.Bold ^ icd.ForcusedRTFControl.SelectionFont.Style);
            }
            else
            {
                icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont, FontStyle.Bold | icd.ForcusedRTFControl.SelectionFont.Style);
            }
        }

        private void italicToolStripButton_Click(object sender, EventArgs e)
        {
            ICardDesigner icd = CardDesignControls.Find(s => s.ThisControl.Visible);
            if (icd.ForcusedRTFControl.SelectionFont.Bold)
            {
                icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont, FontStyle.Italic ^ icd.ForcusedRTFControl.SelectionFont.Style);
            }
            else
            {
                icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont, FontStyle.Italic | icd.ForcusedRTFControl.SelectionFont.Style);
            }
        }

        private void understrikeToolStripButton_Click(object sender, EventArgs e)
        {
            ICardDesigner icd = CardDesignControls.Find(s => s.ThisControl.Visible);
            if (icd.ForcusedRTFControl.SelectionFont.Underline)
            {
                icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont, FontStyle.Underline ^ icd.ForcusedRTFControl.SelectionFont.Style);
            }
            else
            {
                icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont, FontStyle.Underline | icd.ForcusedRTFControl.SelectionFont.Style);
            }
        }

        private void surgeToolStripButton_Click(object sender, EventArgs e)
        {
            ICardDesigner icd = CardDesignControls.Find(s => s.ThisControl.Visible);
            Clipboard.SetImage(new Bitmap(DescentCardGraphics.Properties.Resources.SurgeIcon, 13,10));
            icd.ForcusedRTFControl.Paste();
            Clipboard.Clear();
        }

        private void toolStripNumericUpDown_TextSize_ValueChanged(object sender, EventArgs e)
        {
            ICardDesigner icd = CardDesignControls.Find(s => s.ThisControl.Visible);
            icd.ForcusedRTFControl.Focus();
            icd.ForcusedRTFControl.SelectionFont = new Font(icd.ForcusedRTFControl.SelectionFont.FontFamily, float.Parse(toolStripNumericUpDown_TextSize.Value.ToString()), icd.ForcusedRTFControl.SelectionFont.Style);
        }

        #endregion

        #region DataBaseHandler

        private void treeView_Databases_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView_Databases.SelectedNode != null)
            {
                addCardToolStripMenuItem.Enabled = true;
                toolStripSplitButton_AddCard.Enabled = true;
                changeNameToolStripMenuItem.Enabled = true;
                changeImageToolStripMenuItem.Enabled = true;
            }
            else
            {
                addCardToolStripMenuItem.Enabled = false;
                toolStripSplitButton_AddCard.Enabled = false;
                changeNameToolStripMenuItem.Enabled = false;
                changeImageToolStripMenuItem.Enabled = false;
            }

            foreach (ICardDesigner i in CardDesignControls)
            {
                i.ThisControl.Visible = false;
            }

            if (treeView_Databases.SelectedNode.Parent == null)
            {
                ActiveDatabase = treeView_Databases.SelectedNode.Text;

                foreach (Card c in DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.SubItems.Add(c.Count.ToString());
                    lvi.SubItems.Add(c.Name);
                }
                return;
            }
            else
            {
                ActiveDatabase = treeView_Databases.SelectedNode.Parent.Text;
            }
            PopulateListView();
        }

        private void listView_Cards_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView_Cards.SelectedItems.Count == 0)
            {
                removeCardToolStripButton.Enabled = false;
                deleteCardToolStripMenuItem.Enabled = false;
                toolStripNumericUpDown_Cards.Enabled = false;
            }
            else
            {
                removeCardToolStripButton.Enabled = true;
                deleteCardToolStripMenuItem.Enabled = true;
                toolStripNumericUpDown_Cards.Enabled = true;
            }

            foreach (ICardDesigner i in CardDesignControls)
            {
                i.ThisControl.Visible = false;
            }
            if (listView_Cards.SelectedItems.Count == 0)
            {
                CurrentActiveCard = null;
                return;
            }
            else if (treeView_Databases.SelectedNode.Text == "Feats/Skills" || treeView_Databases.SelectedNode.Parent.Text == "Feats/Skills")
            {
                CurrentActiveCard = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as FeatSkill) != null).OfType<FeatSkill>().
                    ToList().First(s => CheckForNull(s.Count) == listView_Cards.SelectedItems[0].SubItems[0].Text
                        && CheckForNull(s.Name) == listView_Cards.SelectedItems[0].SubItems[1].Text
                        && CheckForNull(s.Text) == listView_Cards.SelectedItems[0].SubItems[2].Text
                        && CheckForNull(s.SkillType) == listView_Cards.SelectedItems[0].SubItems[3].Text);
                featandSkill1.Visible = true;
                CardDesignControls[0].SetProps(CurrentActiveCard);
            }
            else if (treeView_Databases.SelectedNode.Text == "HeroSheets" || treeView_Databases.SelectedNode.Parent.Text == "HeroSheets")
            {
                CurrentActiveCard = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as HeroSheet) != null).OfType<HeroSheet>().
                    ToList().First(s => CheckForNull(s.Count) == listView_Cards.SelectedItems[0].SubItems[0].Text
                        && CheckForNull(s.Name) == listView_Cards.SelectedItems[0].SubItems[1].Text
                        && CheckForNull(s.Wound) == listView_Cards.SelectedItems[0].SubItems[2].Text
                        && CheckForNull(s.Fatigue) == listView_Cards.SelectedItems[0].SubItems[3].Text
                        && CheckForNull(s.Armor) == listView_Cards.SelectedItems[0].SubItems[4].Text
                        && CheckForNull(s.Speed) == listView_Cards.SelectedItems[0].SubItems[5].Text
                        && CheckForNull(s.Text) == listView_Cards.SelectedItems[0].SubItems[6].Text
                        && CheckForNull(s.TraitMelee) == listView_Cards.SelectedItems[0].SubItems[7].Text
                        && CheckForNull(s.TraitSubterfuge) == listView_Cards.SelectedItems[0].SubItems[8].Text
                        && CheckForNull(s.TraitWizardary) == listView_Cards.SelectedItems[0].SubItems[9].Text
                        && CheckForNull(s.SkillMelee) == listView_Cards.SelectedItems[0].SubItems[10].Text
                        && CheckForNull(s.SkillSubterfuge) == listView_Cards.SelectedItems[0].SubItems[11].Text
                        && CheckForNull(s.SkillWizardary) == listView_Cards.SelectedItems[0].SubItems[12].Text
                        && CheckForNull(s.Picture) == listView_Cards.SelectedItems[0].SubItems[13].Text
                        && CheckForNull(s.ConquestValue) == listView_Cards.SelectedItems[0].SubItems[14].Text);
                heroSheet1.Visible = true;
                CardDesignControls[1].SetProps(CurrentActiveCard);
            }
            else if (treeView_Databases.SelectedNode.Text == "Items" || treeView_Databases.SelectedNode.Parent.Text == "Items")
            {
                CurrentActiveCard = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as Item) != null || (s as DarkRelic) != null).OfType<Item>().
                    ToList().First(s => CheckForNull(s.Count) == listView_Cards.SelectedItems[0].SubItems[0].Text
                        && CheckForNull(s.Name) == listView_Cards.SelectedItems[0].SubItems[1].Text
                        && CheckForNull(s.Text) == listView_Cards.SelectedItems[0].SubItems[2].Text
                        && CheckForNull(s.Type) == listView_Cards.SelectedItems[0].SubItems[3].Text
                        && CheckForNull(s.AttackType) == listView_Cards.SelectedItems[0].SubItems[4].Text
                        && CheckForNull(s.ItemImage) == listView_Cards.SelectedItems[0].SubItems[5].Text
                        && CheckForNull(s.Dice) == listView_Cards.SelectedItems[0].SubItems[6].Text
                        && CheckForNull(s.Hand) == listView_Cards.SelectedItems[0].SubItems[7].Text
                        && CheckForNull(s.Price) == listView_Cards.SelectedItems[0].SubItems[8].Text);
                items1.Visible = true;
                CardDesignControls[2].SetProps(CurrentActiveCard);
            }
            else if (treeView_Databases.SelectedNode.Text == "Monsters" || treeView_Databases.SelectedNode.Parent.Text == "Monsters")
            {
                CurrentActiveCard = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as Monster) != null).OfType<Monster>().
                    ToList().First(s => CheckForNull(s.Count) == listView_Cards.SelectedItems[0].SubItems[0].Text
                        && CheckForNull(s.Name) == listView_Cards.SelectedItems[0].SubItems[1].Text
                        && CheckForNull(s.SpeedN) == listView_Cards.SelectedItems[0].SubItems[2].Text
                        && CheckForNull(s.WoundN) == listView_Cards.SelectedItems[0].SubItems[3].Text
                        && CheckForNull(s.ArmorN) == listView_Cards.SelectedItems[0].SubItems[4].Text
                        && CheckForNull(s.DiceN) == listView_Cards.SelectedItems[0].SubItems[5].Text
                        && CheckForNull(s.Text.Split('\n')[0]) == listView_Cards.SelectedItems[0].SubItems[6].Text
                        && CheckForNull(s.SpeedM) == listView_Cards.SelectedItems[0].SubItems[7].Text
                        && CheckForNull(s.WoundM) == listView_Cards.SelectedItems[0].SubItems[8].Text
                        && CheckForNull(s.ArmorM) == listView_Cards.SelectedItems[0].SubItems[9].Text
                        && CheckForNull(s.DiceM) == listView_Cards.SelectedItems[0].SubItems[10].Text
                        && CheckForNull(s.Text.Split('\n')[1]) == listView_Cards.SelectedItems[0].SubItems[11].Text
                        && CheckForNull(s.MonsterImage) == listView_Cards.SelectedItems[0].SubItems[12].Text
                        && CheckForNull(s.PlayerCount) == listView_Cards.SelectedItems[0].SubItems[13].Text
                        && CheckForNull(s.MonsterLevel) == listView_Cards.SelectedItems[0].SubItems[14].Text
                        && CheckForNull(s.MonsterType) == listView_Cards.SelectedItems[0].SubItems[15].Text);
                monster1.Visible = true;
                CardDesignControls[3].SetProps(CurrentActiveCard);
            }
            else if (treeView_Databases.SelectedNode.Text == "Overlord" || treeView_Databases.SelectedNode.Parent.Text == "Overlord")
            {
                CurrentActiveCard = DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as OverLord) != null || (s as Treachery) != null).OfType<OverLord>().
                    ToList().First(s => CheckForNull(s.Count) == listView_Cards.SelectedItems[0].SubItems[0].Text
                        && CheckForNull(s.Name) == listView_Cards.SelectedItems[0].SubItems[1].Text
                        && CheckForNull(s.Text) == listView_Cards.SelectedItems[0].SubItems[2].Text
                        && CheckForNull(s.CardType) == listView_Cards.SelectedItems[0].SubItems[3].Text
                        && CheckForNull(s.ThreadCost) == listView_Cards.SelectedItems[0].SubItems[4].Text
                        && CheckForNull(s.DiscardThread) == listView_Cards.SelectedItems[0].SubItems[5].Text);
                overlord1.Visible = true;
                CardDesignControls[4].SetProps(CurrentActiveCard);
            }
            UpdateImage();
        }

        private void PopulateListView()
        {
            listView_Cards.Clear();
            listView_Cards.Columns.Clear();
            listView_Cards.Items.Clear();
            listView_Cards.Columns.Add("Numbers", 80);
            listView_Cards.Columns.Add("Name", 80);

            if (treeView_Databases.SelectedNode.Text == "Feats/Skills" || treeView_Databases.SelectedNode.Parent.Text == "Feats/Skills")
            {
                listView_Cards.Columns.Add("Text", 80);
                listView_Cards.Columns.Add("Type", 80);

                foreach (FeatSkill c in DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as FeatSkill) != null).OfType<FeatSkill>().ToList())
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = CheckForNull(c.Count);
                    lvi.SubItems.Add(CheckForNull(c.Name));
                    lvi.SubItems.Add(CheckForNull(c.Text));
                    lvi.SubItems.Add(CheckForNull(c.SkillType));
                    listView_Cards.Items.Add(lvi);
                }
            }
            else if (treeView_Databases.SelectedNode.Text == "HeroSheets" || treeView_Databases.SelectedNode.Parent.Text == "HeroSheets")
            {
                listView_Cards.Columns.Add("Wounds", 80);
                listView_Cards.Columns.Add("Fatigue", 80);
                listView_Cards.Columns.Add("Armor", 80);
                listView_Cards.Columns.Add("Speed", 80);
                listView_Cards.Columns.Add("Special Ability", 80);
                listView_Cards.Columns.Add("Trait Melee", 80);
                listView_Cards.Columns.Add("Trait Subterfuge", 80);
                listView_Cards.Columns.Add("Trait Wizardary", 80);
                listView_Cards.Columns.Add("Skill Melee", 80);
                listView_Cards.Columns.Add("Skill Subterfuge", 80);
                listView_Cards.Columns.Add("Skill Wizardary", 80);
                listView_Cards.Columns.Add("Picture", 80);
                listView_Cards.Columns.Add("Conquest Value", 80);

                foreach (HeroSheet c in DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as HeroSheet) != null).OfType<HeroSheet>().ToList())
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = CheckForNull(c.Count);
                    lvi.SubItems.Add(CheckForNull(c.Name));
                    lvi.SubItems.Add(CheckForNull(c.Wound));
                    lvi.SubItems.Add(CheckForNull(c.Fatigue));
                    lvi.SubItems.Add(CheckForNull(c.Armor));
                    lvi.SubItems.Add(CheckForNull(c.Speed));
                    lvi.SubItems.Add(CheckForNull(c.Text));
                    lvi.SubItems.Add(CheckForNull(c.TraitMelee));
                    lvi.SubItems.Add(CheckForNull(c.TraitSubterfuge));
                    lvi.SubItems.Add(CheckForNull(c.TraitWizardary));
                    lvi.SubItems.Add(CheckForNull(c.SkillMelee));
                    lvi.SubItems.Add(CheckForNull(c.SkillSubterfuge));
                    lvi.SubItems.Add(CheckForNull(c.SkillWizardary));
                    lvi.SubItems.Add(CheckForNull(c.Picture));
                    lvi.SubItems.Add(CheckForNull(c.ConquestValue));
                    listView_Cards.Items.Add(lvi);
                }
            }
            else if (treeView_Databases.SelectedNode.Text == "Items" || treeView_Databases.SelectedNode.Parent.Text == "Items")
            {
                listView_Cards.Columns.Add("Item Text", 80);
                listView_Cards.Columns.Add("Item Type", 80);
                listView_Cards.Columns.Add("Attack Type", 80);
                listView_Cards.Columns.Add("Picture", 80);
                listView_Cards.Columns.Add("Dice", 80);
                listView_Cards.Columns.Add("Hands", 80);
                listView_Cards.Columns.Add("Cost", 80);
                listView_Cards.Columns.Add("Thread Cost", 80);

                foreach (Item c in DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as Item) != null || (s as DarkRelic) != null).OfType<Item>().ToList())
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = CheckForNull(c.Count);
                    lvi.SubItems.Add(CheckForNull(c.Name));
                    lvi.SubItems.Add(CheckForNull(c.Text));
                    lvi.SubItems.Add(CheckForNull(c.Type));
                    lvi.SubItems.Add(CheckForNull(c.AttackType));
                    lvi.SubItems.Add(CheckForNull(c.ItemImage));
                    lvi.SubItems.Add(CheckForNull(c.Dice));
                    lvi.SubItems.Add(CheckForNull(c.Hand));
                    lvi.SubItems.Add(CheckForNull(c.Price));
                    if (c.ItemType == ItemType.DarkRelic)
                    {
                        lvi.SubItems.Add(CheckForNull((c as DarkRelic).ThreadCost));
                    }
                    listView_Cards.Items.Add(lvi);
                }
            }
            else if (treeView_Databases.SelectedNode.Text == "Monsters" || treeView_Databases.SelectedNode.Parent.Text == "Monsters")
            {
                listView_Cards.Columns.Add("Normal Speed", 80);
                listView_Cards.Columns.Add("Normal Wounds", 80);
                listView_Cards.Columns.Add("Normal Armor", 80);
                listView_Cards.Columns.Add("Normal Dice", 80);
                listView_Cards.Columns.Add("Normal Abilities", 80);
                listView_Cards.Columns.Add("Master Speed", 80);
                listView_Cards.Columns.Add("Master Wounds", 80);
                listView_Cards.Columns.Add("Master Armor", 80);
                listView_Cards.Columns.Add("Master Dice", 80);
                listView_Cards.Columns.Add("Master Abilities", 80);
                listView_Cards.Columns.Add("Picture", 80);
                listView_Cards.Columns.Add("Player Count", 80);
                listView_Cards.Columns.Add("Campaign Level", 80);
                listView_Cards.Columns.Add("Campaign Monster Type", 80);

                foreach (Monster c in DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as Monster) != null).OfType<Monster>().ToList())
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = CheckForNull(c.Count);
                    lvi.SubItems.Add(CheckForNull(c.Name));
                    lvi.SubItems.Add(CheckForNull(c.SpeedN));
                    lvi.SubItems.Add(CheckForNull(c.WoundN));
                    lvi.SubItems.Add(CheckForNull(c.ArmorN));
                    lvi.SubItems.Add(CheckForNull(c.DiceN));
                    lvi.SubItems.Add(CheckForNull(c.Text.Split('\n')[0]));
                    lvi.SubItems.Add(CheckForNull(c.SpeedM));
                    lvi.SubItems.Add(CheckForNull(c.WoundM));
                    lvi.SubItems.Add(CheckForNull(c.ArmorM));
                    lvi.SubItems.Add(CheckForNull(c.DiceM));
                    lvi.SubItems.Add(CheckForNull(c.Text.Split('\n')[1]));
                    lvi.SubItems.Add(CheckForNull(c.MonsterImage));
                    lvi.SubItems.Add(CheckForNull(c.PlayerCount));
                    lvi.SubItems.Add(CheckForNull(c.MonsterLevel));
                    lvi.SubItems.Add(CheckForNull(c.MonsterType));
                    listView_Cards.Items.Add(lvi);
                }
            }
            else if (treeView_Databases.SelectedNode.Text == "Overlord" || treeView_Databases.SelectedNode.Parent.Text == "Overlord")
            {
                listView_Cards.Columns.Add("Text", 80);
                listView_Cards.Columns.Add("Type", 80);
                listView_Cards.Columns.Add("Thread Cost", 80);
                listView_Cards.Columns.Add("Discard Thread", 80);
                listView_Cards.Columns.Add("Treachery", 80);

                foreach (OverLord c in DatabasesHandler.Cards.Find(s => s.SetName == ActiveDatabase).SetCards.Where(s => (s as OverLord) != null || (s as Treachery) != null).OfType<OverLord>().ToList())
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = CheckForNull(c.Count);
                    lvi.SubItems.Add(CheckForNull(c.Name));
                    lvi.SubItems.Add(CheckForNull(c.Text));
                    lvi.SubItems.Add(CheckForNull(c.CardType));
                    lvi.SubItems.Add(CheckForNull(c.ThreadCost));
                    lvi.SubItems.Add(CheckForNull(c.DiscardThread));
                    if (c.OverlordCardType == OverLoadCardType.TreacheryEvent || c.OverlordCardType == OverLoadCardType.TreacherySpawn || c.OverlordCardType == OverLoadCardType.TreacheryTrap)
                    {
                        lvi.SubItems.Add(CheckForNull((c as Treachery).TreacheryCost));
                    }
                    listView_Cards.Items.Add(lvi);
                }
            }
        }

        private string CheckForNull (object o)
        {
            if (o != null && o.ToString() != "")
            {
                if (o is List<DiceType>)
                {
                    string ret = "";
                    foreach (DiceType d in (o as List<DiceType>))
                    {
                        ret += d.ToString() + "; ";
                    }
                    if (ret == "")
                    {
                        return "None";
                    }
                    return ret;
                }
                return o.ToString();
            }
            else
            {
                return "None";
            }
        }

        #endregion

        public void UpdateImage()
        {
            if (backgroundWorkerPictureUpdater.IsBusy)
            {
                return;
            }
            backgroundWorkerPictureUpdater.RunWorkerAsync();
        }

        private void listView_Cards_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewSorter Sorter = new ListViewSorter();
            listView_Cards.ListViewItemSorter = Sorter;
            if (!(listView_Cards.ListViewItemSorter is ListViewSorter))
                return;
            Sorter = (ListViewSorter)listView_Cards.ListViewItemSorter;

            if (Sorter.LastSort == e.Column)
            {
                if (listView_Cards.Sorting == SortOrder.Ascending)
                    listView_Cards.Sorting = SortOrder.Descending;
                else
                    listView_Cards.Sorting = SortOrder.Ascending;
            }
            else
            {
                listView_Cards.Sorting = SortOrder.Descending;
            }
            Sorter.ByColumn = e.Column;

            listView_Cards.Sort();
        }

        private void toolStripNumericUpDown_Cards_Click(object sender, EventArgs e)
        {
            if (CurrentActiveCard != null)
            {
                CurrentActiveCard.Count = (int)toolStripNumericUpDown_Cards.Value;
                PopulateListView();
                SendKeys.Send("{esc}");
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pictureBox1.Image = CurrentActiveCard.DrawCard();
        }

        private void backgroundWorkerListViewUpdater_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            PopulateListView();
        }

        private void changeNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name = (Microsoft.VisualBasic.Interaction.InputBox("Enter Database Name"));

            if (name == "" || name.Trim() == "")
            {
                MessageBox.Show(this, "The database " + name + " is invalid, please choose another name!", "Invalid Name!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (TreeNode s in treeView_Databases.Nodes)
            {
                if (s.Text == name)
                {
                    MessageBox.Show(this, "The database " + name + " does already exist, please choose another name!", "Invalid Name!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            DatabasesHandler.Cards[DatabasesHandler.Cards.FindIndex(s => s.SetName == ActiveDatabase)].SetName = name;
            treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(ActiveDatabase)].Name = name;
            treeView_Databases.Nodes[treeView_Databases.Nodes.IndexOfKey(name)].Text = name;
            ActiveDatabase = name;
        }

        private void changeImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
            dialog.Title = "select an image";
            dialog.ShowDialog();
            try
            {
                DatabasesHandler.Cards[DatabasesHandler.Cards.FindIndex(s => s.SetName == ActiveDatabase)].SetIcon = dialog.FileName;
                foreach (Card c in DatabasesHandler.Cards[DatabasesHandler.Cards.FindIndex(s => s.SetName == ActiveDatabase)].SetCards)
                {
                    c.Symbol = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            dialog.Dispose();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            DialogResult dr = MessageBox.Show("Save First?", "Save Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dr == System.Windows.Forms.DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                DataFileHandler.SaveCreatorSet();
            }

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}

public class ListViewSorter : System.Collections.IComparer
{
    public int Compare(object o1, object o2)
    {
        if (!(o1 is ListViewItem))
            return (0);
        if (!(o2 is ListViewItem))
            return (0);

        ListViewItem lvi1 = (ListViewItem)o2;
        string str1 = lvi1.SubItems[ByColumn].Text;
        ListViewItem lvi2 = (ListViewItem)o1;
        string str2 = lvi2.SubItems[ByColumn].Text;

        int result;
        if (lvi1.ListView.Sorting == SortOrder.Ascending)
            result = String.Compare(str1, str2);
        else
            result = String.Compare(str2, str1);

        LastSort = ByColumn;

        return (result);
    }


    public int ByColumn
    {
        get { return Column; }
        set { Column = value; }
    }
    int Column = 0;

    public int LastSort
    {
        get { return LastColumn; }
        set { LastColumn = value; }
    }
    int LastColumn = 0;
}