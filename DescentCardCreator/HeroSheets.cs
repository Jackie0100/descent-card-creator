﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DescentCardGraphics;

namespace DescentCardCreator
{
    public partial class HeroSheets : UserControl, ICardDesigner
    {
        public MainForm ParentOwner { get; set; }
        public RichTextBox ForcusedRTFControl { get; set; }
        public UserControl ThisControl { get; set; }

        public HeroSheets()
        {
            InitializeComponent();
            ThisControl = this;
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    c.GotFocus += new EventHandler(RTFGotFocus);
                    c.LostFocus += new EventHandler(RTFLostFocus);
                }
            }
            ForcusedRTFControl = this.richTextBox_Name;
        }

        public void SetProps(Card c)
        {
            richTextBox_Name.Text = (c as HeroSheet).Name;
            richTextBox_SpecialAbility.Rtf = (c as HeroSheet).Text;
            numericUpDown_ConquestValue.Value = decimal.Parse((c as HeroSheet).ConquestValue);
            textBox_Picture.Text = (c as HeroSheet).Picture;
            if ((c as HeroSheet).Wound != "*")
            {
                numericUpDown_Wound.Value = decimal.Parse((c as HeroSheet).Wound);
            }
            else
            {
                checkBox_Wound.Checked = true;
                numericUpDown_Wound.Enabled = false;
            }
            if ((c as HeroSheet).Fatigue != "*")
            {
                numericUpDown_Fatigue.Value = decimal.Parse((c as HeroSheet).Fatigue);
            }
            else
            {
                checkBox_Fatigue.Checked = true;
                numericUpDown_Fatigue.Enabled = false;
            }
            if ((c as HeroSheet).Armor != "*")
            {
                numericUpDown_Armor.Value = decimal.Parse((c as HeroSheet).Armor);
            }
            else
            {
                checkBox_Armor.Checked = true;
                numericUpDown_Armor.Enabled = false;
            }
            if ((c as HeroSheet).Speed != "*")
            {
                numericUpDown_Speed.Value = decimal.Parse((c as HeroSheet).Speed);
            }
            else
            {
                checkBox_Speed.Checked = true;
                numericUpDown_Speed.Enabled = false;
            }
            numericUpDown_MeleeTrait.Value = (c as HeroSheet).TraitMelee;
            numericUpDown_SubterfugeTrait.Value = (c as HeroSheet).TraitSubterfuge;
            numericUpDown_WizardaryTrait.Value = (c as HeroSheet).TraitWizardary;
            if ((c as HeroSheet).SkillMelee != "*")
            {
                numericUpDown_MeleeSkill.Value = decimal.Parse((c as HeroSheet).SkillMelee);
            }
            else
            {
                checkBox_MeleeSkill.Checked = true;
                numericUpDown_MeleeSkill.Enabled = false;
            }
            if ((c as HeroSheet).SkillSubterfuge != "*")
            {
                numericUpDown_SubterfugeSkill.Value = decimal.Parse((c as HeroSheet).SkillSubterfuge);
            }
            else
            {
                checkBox_MeleeSkill.Checked = true;
                numericUpDown_SubterfugeSkill.Enabled = false;
            }
            if ((c as HeroSheet).SkillWizardary != "*")
            {
                numericUpDown_WizardarySkill.Value = decimal.Parse((c as HeroSheet).SkillWizardary);
            }
            else
            {
                checkBox_MeleeSkill.Checked = true;
                numericUpDown_WizardarySkill.Enabled = false;
            }
        }

        public void Reset()
        {
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    (c as RichTextBox).Text = "";
                }
                else if (c is TextBox)
                {
                    (c as TextBox).Text = "";
                }
                else if (c is NumericUpDown)
                {
                    (c as NumericUpDown).Value = 0;
                }
                else if (c is ComboBox)
                {
                    (c as ComboBox).SelectedIndex = 0;
                }
                else if (c is CheckBox)
                {
                    (c as CheckBox).Checked = false;
                }
            }
        }

        public void RTFLostFocus(object sender, EventArgs e)
        {
            ParentOwner.HideFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        public void RTFGotFocus(object sender, EventArgs e)
        {
            ParentOwner.ShowFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        private void checkBox_Wound_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Wound.Checked)
            {
                numericUpDown_Wound.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).Wound = "*";
            }
            else
            {
                numericUpDown_Wound.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).Wound = numericUpDown_Wound.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Fatigue_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Fatigue.Checked)
            {
                numericUpDown_Fatigue.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).Fatigue = "*";
            }
            else
            {
                numericUpDown_Fatigue.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).Fatigue = numericUpDown_Fatigue.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Armor_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Armor.Checked)
            {
                numericUpDown_Armor.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).Armor = "*";
            }
            else
            {
                numericUpDown_Armor.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).Armor = numericUpDown_Armor.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_Speed_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Speed.Checked)
            {
                numericUpDown_Speed.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).Speed = "*";
            }
            else
            {
                numericUpDown_Speed.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).Speed = numericUpDown_Speed.Value.ToString();
            }
        }

        private void checkBox_MeleeSkill_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_MeleeSkill.Checked)
            {
                numericUpDown_MeleeSkill.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).SkillMelee = "*";
            }
            else
            {
                numericUpDown_MeleeSkill.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).SkillMelee = numericUpDown_MeleeSkill.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_SubterfugeSkill_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_SubterfugeSkill.Checked)
            {
                numericUpDown_SubterfugeSkill.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).SkillSubterfuge = "*";
            }
            else
            {
                numericUpDown_SubterfugeSkill.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).SkillSubterfuge = numericUpDown_SubterfugeSkill.Value.ToString();
            }
            UpdateCard();
        }

        private void checkBox_WizadarySkill_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_WizadarySkill.Checked)
            {
                numericUpDown_WizardarySkill.Enabled = false;
                (ParentOwner.CurrentActiveCard as HeroSheet).SkillWizardary = "*";
            }
            else
            {
                numericUpDown_WizardarySkill.Enabled = true;
                (ParentOwner.CurrentActiveCard as HeroSheet).SkillWizardary = numericUpDown_WizardarySkill.Value.ToString();
            }
            UpdateCard();
        }

        private void richTextBox_Name_TextChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Name = richTextBox_Name.Text;
            UpdateCard();
        }

        private void richTextBox_SpecialAbility_TextChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Text = richTextBox_SpecialAbility.Rtf;
            UpdateCard();
        }

        private void numericUpDown_ConquestValue_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).ConquestValue = numericUpDown_ConquestValue.Value.ToString();
            UpdateCard();
        }

        private void textBox_Picture_TextChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Picture = textBox_Picture.Text;
            UpdateCard();
        }

        private void button_Browse_Picture_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            try
            {
                dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
                dialog.Title = "select an image";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    textBox_Picture.Text = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            dialog.Dispose();
        }

        private void numericUpDown_Wound_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Wound = numericUpDown_Wound.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_Fatigue_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Fatigue = numericUpDown_Fatigue.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_Armor_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Armor = numericUpDown_Armor.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_Speed_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).Speed = numericUpDown_Speed.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_MeleeTrait_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).TraitMelee = (int)numericUpDown_MeleeTrait.Value;
            UpdateCard();
        }

        private void numericUpDown_SubterfugeTrait_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).TraitSubterfuge = (int)numericUpDown_SubterfugeTrait.Value;
            UpdateCard();
        }

        private void numericUpDown_WizardaryTrait_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).TraitWizardary = (int)numericUpDown_WizardaryTrait.Value;
            UpdateCard();
        }

        private void numericUpDown_MeleeSkill_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).SkillMelee = numericUpDown_MeleeSkill.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_SubterfugeSkill_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).SkillSubterfuge = numericUpDown_SubterfugeSkill.Value.ToString();
            UpdateCard();
        }

        private void numericUpDown_WizardarySkill_ValueChanged(object sender, EventArgs e)
        {
            (ParentOwner.CurrentActiveCard as HeroSheet).SkillWizardary = numericUpDown_WizardarySkill.Value.ToString();
            UpdateCard();
        }

        public void UpdateCard()
        {
            try
            {
                if (ParentOwner.backgroundWorkerPictureUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerPictureUpdater.CancelAsync();
                }
                if (ParentOwner.backgroundWorkerListViewUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerListViewUpdater.CancelAsync();
                }
                ParentOwner.backgroundWorkerPictureUpdater.RunWorkerAsync();
                ParentOwner.backgroundWorkerListViewUpdater.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}