﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DescentCardGraphics;

namespace DescentCardCreator
{
	public partial class Monsters : UserControl, ICardDesigner
	{
		public MainForm ParentOwner { get; set; }
		public RichTextBox ForcusedRTFControl { get; set; }
		public UserControl ThisControl { get; set; }

		public Monsters()
		{
			InitializeComponent();
			ThisControl = this;
			comboBox_AttackType.SelectedIndex = 0;
			comboBox_CampaignLevel.SelectedIndex = 0;
			comboBox_MonsterType.SelectedIndex = 0;

			foreach (Control c in this.Controls)
			{
				if (c is RichTextBox)
				{
					c.GotFocus += new EventHandler(RTFGotFocus);
					c.LostFocus += new EventHandler(RTFLostFocus);
				}
			}
			ForcusedRTFControl = this.richTextBox_Name;
		}

		public void SetProps(Card c)
		{
			richTextBox_Name.Text = (c as Monster).Name;
			textBox_Picture.Text = (c as Monster).MonsterImage;
			comboBox_AttackType.SelectedIndex = (int)(c as Monster).AttackType;
			comboBox_MonsterType.SelectedIndex = (int)(c as Monster).MonsterType;
			comboBox_CampaignLevel.SelectedIndex = (int)(c as Monster).MonsterLevel;
			comboBox1.SelectedIndex = (int)(c as Monster).bMonsterLevel;
			textBox_NormalAbility.Text = (c as Monster).Text.Split('\n')[0];
			textBox_MasterAbility.Text = (c as Monster).Text.Split('\n')[1];
			textBox_bNormalAbility.Text = (c as Monster).Text.Split('\n')[2];
			textBox_bMasterAbility.Text = (c as Monster).Text.Split('\n')[3];
			numericUpDown_Playercount.Value = (c as Monster).PlayerCount;
			numericUpDown25.Value = (c as Monster).bPlayerCount;

            numericUpDown_NormalSpeed.Value = (c as Monster).SpeedN;
            numericUpDown_NormalWounds.Value = (c as Monster).WoundN;
            numericUpDown_NormalArmor.Value = (c as Monster).ArmorN;

			numericUpDown_MasterSpeed.Value = (c as Monster).SpeedM;
			numericUpDown_MasterWounds.Value = (c as Monster).WoundM;
            numericUpDown_MasterArmor.Value = (c as Monster).ArmorM;

			numericUpDown_bNormalSpeed.Value = (c as Monster).bSpeedN;
			numericUpDown_bNormalWound.Value = (c as Monster).bWoundN;
			numericUpDown_bNormalArmor.Value = (c as Monster).bArmorN;

			numericUpDown_bMasterSpeed.Value = (c as Monster).bSpeedM;
			numericUpDown_bMasterWound.Value = (c as Monster).bWoundM;
			numericUpDown_bMasterArmor.Value = (c as Monster).bArmorM;

            diceControl_FrontNormal.SetDice((c as Monster).DiceN);
            diceControl_FrontMaster.SetDice((c as Monster).DiceM);
            diceControl_BackMaster.SetDice((c as Monster).DiceN);
            diceControl_BackNormal.SetDice((c as Monster).DiceM);
		}

		public void Reset()
		{
			foreach (Control c in this.Controls)
			{
				if (c is RichTextBox)
				{
					(c as RichTextBox).Text = "";
				}
				else if (c is TextBox)
				{
					(c as TextBox).Text = "";
				}
				else if (c is NumericUpDown)
				{
					(c as NumericUpDown).Value = 0;
				}
				else if (c is ComboBox)
				{
					(c as ComboBox).SelectedIndex = 0;
				}
				else if (c is CheckBox)
				{
					(c as CheckBox).Checked = false;
				}
			}
		}
        
		public void RTFLostFocus(object sender, EventArgs e)
		{
			ParentOwner.HideFontStyleButtons();
			ForcusedRTFControl = (RichTextBox)sender;
		}

		public void RTFGotFocus(object sender, EventArgs e)
		{
			ParentOwner.ShowFontStyleButtons();
			ForcusedRTFControl = (RichTextBox)sender;
		}

		private void numericUpDown_Playercount_ValueChanged(object sender, EventArgs e)
		{
			if (numericUpDown_Playercount.Value != 0)
			{
				comboBox_CampaignLevel.Enabled = false;
				comboBox_MonsterType.Enabled = false;
				comboBox_CampaignLevel.SelectedIndex = 0;
				comboBox_MonsterType.SelectedIndex = 0;
			}
			else
			{
				comboBox_CampaignLevel.Enabled = true;
				comboBox_MonsterType.Enabled = true;
			}
			(ParentOwner.CurrentActiveCard as Monster).PlayerCount = (int)numericUpDown_Playercount.Value;
			UpdateCard();
		}

		private void comboBox_CampaignLevel_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (comboBox_CampaignLevel.SelectedIndex != 0 || comboBox_MonsterType.SelectedIndex != 0)
				{
					numericUpDown_Playercount.Enabled = false;
				}
				else
				{
					numericUpDown_Playercount.Enabled = true;
				}
				(ParentOwner.CurrentActiveCard as Monster).MonsterType = (MonsterType)comboBox_MonsterType.SelectedIndex;
				(ParentOwner.CurrentActiveCard as Monster).MonsterLevel = (MonsterLevel)comboBox_CampaignLevel.SelectedIndex;
				UpdateCard();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		private void button_Browse_Picture_Click(object sender, EventArgs e)
		{
			try
			{
				OpenFileDialog dialog = new OpenFileDialog();
				dialog.Filter = "Picture Files|*.jpg;*.jpeg;*.png;*.gif;";
				dialog.Title = "select an image";
				if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				{
					textBox_Picture.Text = dialog.FileName;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		private void textBox_Picture_TextChanged(object sender, EventArgs e)
		{
			try
			{
				(ParentOwner.CurrentActiveCard as Monster).MonsterImage = textBox_Picture.Text;
				UpdateCard();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		private void richTextBox_Name_TextChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).Name = richTextBox_Name.Text;
			UpdateCard();
		}

		private void comboBox_AttackType_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				(ParentOwner.CurrentActiveCard as Monster).AttackType = (AttackTypes)comboBox_AttackType.SelectedIndex;
				UpdateCard();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		private void numericUpDown_SpeedNormal_ValueChanged(object sender, EventArgs e)
		{
            (ParentOwner.CurrentActiveCard as Monster).SpeedN = (int)numericUpDown_NormalSpeed.Value;
			UpdateCard();
		}

		private void numericUpDown_WoundsNormal_ValueChanged(object sender, EventArgs e)
		{
            (ParentOwner.CurrentActiveCard as Monster).WoundN = (int)numericUpDown_NormalWounds.Value;
			UpdateCard();
		}

		private void numericUpDown_ArmorNormal_ValueChanged(object sender, EventArgs e)
		{
            (ParentOwner.CurrentActiveCard as Monster).ArmorN = (int)numericUpDown_NormalArmor.Value;
			UpdateCard();
		}

		private void numericUpDown_SpeedMaster_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).SpeedM = (int)numericUpDown_MasterSpeed.Value;
			UpdateCard();
		}

		private void numericUpDown_WoundsMaster_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).WoundM = (int)numericUpDown_MasterWounds.Value;
			UpdateCard();
		}

		private void numericUpDown_ArmorMaster_ValueChanged(object sender, EventArgs e)
		{
            (ParentOwner.CurrentActiveCard as Monster).ArmorM = (int)numericUpDown_MasterArmor.Value;
			UpdateCard();
		}

		private void textBox_NormalAbility_TextChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).Text = textBox_NormalAbility.Text + "\n" + textBox_MasterAbility.Text + "\n" + textBox_bNormalAbility.Text + "\n" + textBox_bMasterAbility.Text;
			UpdateCard();
		}

		private void textBox_MasterAbility_TextChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).Text = textBox_NormalAbility.Text + "\n" + textBox_MasterAbility.Text + "\n" + textBox_bNormalAbility.Text + "\n" + textBox_bMasterAbility.Text;
			UpdateCard();
		}

        public void UpdateCard()
		{
			try
			{
				if (ParentOwner.backgroundWorkerPictureUpdater.WorkerSupportsCancellation)
				{
					ParentOwner.backgroundWorkerPictureUpdater.CancelAsync();
				}
				if (ParentOwner.backgroundWorkerListViewUpdater.WorkerSupportsCancellation)
				{
					ParentOwner.backgroundWorkerListViewUpdater.CancelAsync();
				}

                (ParentOwner.CurrentActiveCard as Monster).DiceN = diceControl_FrontNormal.GetDice();
                (ParentOwner.CurrentActiveCard as Monster).DiceM = diceControl_FrontMaster.GetDice();
                (ParentOwner.CurrentActiveCard as Monster).bDiceN = diceControl_BackNormal.GetDice();
                (ParentOwner.CurrentActiveCard as Monster).bDiceM = diceControl_BackMaster.GetDice();

				ParentOwner.backgroundWorkerPictureUpdater.RunWorkerAsync();
				ParentOwner.backgroundWorkerListViewUpdater.RunWorkerAsync();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (comboBox1.SelectedIndex != 0 || comboBox_MonsterType.SelectedIndex != 0)
				{
					numericUpDown_Playercount.Enabled = false;
				}
				else
				{
					numericUpDown_Playercount.Enabled = true;
				}
				(ParentOwner.CurrentActiveCard as Monster).MonsterType = (MonsterType)comboBox_MonsterType.SelectedIndex;
				(ParentOwner.CurrentActiveCard as Monster).bMonsterLevel = (MonsterLevel)comboBox1.SelectedIndex;
				UpdateCard();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		private void numericUpDown25_ValueChanged(object sender, EventArgs e)
		{
			if (numericUpDown25.Value != 0)
			{
				comboBox1.Enabled = false;
				comboBox_MonsterType.Enabled = false;
				comboBox1.SelectedIndex = 0;
				comboBox_MonsterType.SelectedIndex = 0;
			}
			else
			{
				comboBox1.Enabled = true;
				comboBox_MonsterType.Enabled = true;
			}
			(ParentOwner.CurrentActiveCard as Monster).PlayerCount = (int)numericUpDown_Playercount.Value;
			UpdateCard();
		}

		private void numericUpDown13_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).bArmorN = (int)numericUpDown_bNormalArmor.Value;
			UpdateCard();
		}

		private void numericUpDown14_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).bWoundN = (int)numericUpDown_bNormalWound.Value;
			UpdateCard();
		}

		private void numericUpDown15_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).bSpeedN = (int)numericUpDown_bNormalSpeed.Value;
			UpdateCard();
		}

		private void numericUpDown10_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).bArmorM = (int)numericUpDown_bMasterArmor.Value;
			UpdateCard();
		}

		private void numericUpDown11_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).bWoundM = (int)numericUpDown_bMasterWound.Value;
			UpdateCard();
		}

		private void numericUpDown12_ValueChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).bSpeedM = (int)numericUpDown_bMasterSpeed.Value;
			UpdateCard();
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).Text = textBox_NormalAbility.Text + "\n" + textBox_MasterAbility.Text + "\n" + textBox_bNormalAbility.Text + "\n" + textBox_bMasterAbility.Text;
			UpdateCard();
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			(ParentOwner.CurrentActiveCard as Monster).Text = textBox_NormalAbility.Text + "\n" + textBox_MasterAbility.Text + "\n" + textBox_bNormalAbility.Text + "\n" + textBox_bMasterAbility.Text;
			UpdateCard();
		}
	}
}
