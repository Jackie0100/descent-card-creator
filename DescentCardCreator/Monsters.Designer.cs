﻿namespace DescentCardCreator
{
    partial class Monsters
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox_Name = new System.Windows.Forms.RichTextBox();
            this.label_Name = new System.Windows.Forms.Label();
            this.comboBox_AttackType = new System.Windows.Forms.ComboBox();
            this.label_AttackType = new System.Windows.Forms.Label();
            this.label_Number = new System.Windows.Forms.Label();
            this.numericUpDown_Playercount = new System.Windows.Forms.NumericUpDown();
            this.comboBox_CampaignLevel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_MonsterType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_Browse_Picture = new System.Windows.Forms.Button();
            this.textBox_Picture = new System.Windows.Forms.TextBox();
            this.label_Picture = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown25 = new System.Windows.Forms.NumericUpDown();
            this.textBox_NormalAbility = new System.Windows.Forms.TextBox();
            this.label_SpeedNormal = new System.Windows.Forms.Label();
            this.label_AbilitiesNormal = new System.Windows.Forms.Label();
            this.label_WoundsNormal = new System.Windows.Forms.Label();
            this.label_ArmorNormal = new System.Windows.Forms.Label();
            this.label_FrontNormal = new System.Windows.Forms.Label();
            this.numericUpDown_NormalArmor = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDown_NormalWounds = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox_MasterAbility = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown_MasterSpeed = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown_MasterWounds = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDown_MasterArmor = new System.Windows.Forms.NumericUpDown();
            this.label_FrontMaster = new System.Windows.Forms.Label();
            this.textBox_bNormalAbility = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown_bNormalSpeed = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown_bNormalWound = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown_bNormalArmor = new System.Windows.Forms.NumericUpDown();
            this.label_BackNormal = new System.Windows.Forms.Label();
            this.textBox_bMasterAbility = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_bMasterSpeed = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_bMasterWound = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_bMasterArmor = new System.Windows.Forms.NumericUpDown();
            this.label_BackMaster = new System.Windows.Forms.Label();
            this.numericUpDown_NormalSpeed = new System.Windows.Forms.NumericUpDown();
            this.diceControl_BackMaster = new DescentCardCreator.DiceControl();
            this.diceControl_BackNormal = new DescentCardCreator.DiceControl();
            this.diceControl_FrontMaster = new DescentCardCreator.DiceControl();
            this.diceControl_FrontNormal = new DescentCardCreator.DiceControl();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Playercount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NormalArmor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NormalWounds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MasterSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MasterWounds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MasterArmor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bNormalSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bNormalWound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bNormalArmor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bMasterSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bMasterWound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bMasterArmor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NormalSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox_Name
            // 
            this.richTextBox_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Name.Location = new System.Drawing.Point(3, 23);
            this.richTextBox_Name.Multiline = false;
            this.richTextBox_Name.Name = "richTextBox_Name";
            this.richTextBox_Name.Size = new System.Drawing.Size(670, 56);
            this.richTextBox_Name.TabIndex = 126;
            this.richTextBox_Name.Text = "";
            this.richTextBox_Name.TextChanged += new System.EventHandler(this.richTextBox_Name_TextChanged);
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.Location = new System.Drawing.Point(3, 0);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(51, 20);
            this.label_Name.TabIndex = 125;
            this.label_Name.Text = "Name";
            // 
            // comboBox_AttackType
            // 
            this.comboBox_AttackType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AttackType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_AttackType.FormattingEnabled = true;
            this.comboBox_AttackType.Items.AddRange(new object[] {
            "Melee",
            "Ranged",
            "Magic",
            "Morph",
            "Psionic"});
            this.comboBox_AttackType.Location = new System.Drawing.Point(3, 157);
            this.comboBox_AttackType.Name = "comboBox_AttackType";
            this.comboBox_AttackType.Size = new System.Drawing.Size(220, 28);
            this.comboBox_AttackType.TabIndex = 154;
            this.comboBox_AttackType.SelectedIndexChanged += new System.EventHandler(this.comboBox_AttackType_SelectedIndexChanged);
            // 
            // label_AttackType
            // 
            this.label_AttackType.AutoSize = true;
            this.label_AttackType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_AttackType.Location = new System.Drawing.Point(3, 134);
            this.label_AttackType.Name = "label_AttackType";
            this.label_AttackType.Size = new System.Drawing.Size(93, 20);
            this.label_AttackType.TabIndex = 153;
            this.label_AttackType.Text = "Attack Type";
            // 
            // label_Number
            // 
            this.label_Number.AutoSize = true;
            this.label_Number.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Number.Location = new System.Drawing.Point(453, 135);
            this.label_Number.Name = "label_Number";
            this.label_Number.Size = new System.Drawing.Size(141, 20);
            this.label_Number.TabIndex = 183;
            this.label_Number.Text = "Front Player Count";
            // 
            // numericUpDown_Playercount
            // 
            this.numericUpDown_Playercount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Playercount.Location = new System.Drawing.Point(455, 158);
            this.numericUpDown_Playercount.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_Playercount.Name = "numericUpDown_Playercount";
            this.numericUpDown_Playercount.Size = new System.Drawing.Size(220, 26);
            this.numericUpDown_Playercount.TabIndex = 184;
            this.numericUpDown_Playercount.ValueChanged += new System.EventHandler(this.numericUpDown_Playercount_ValueChanged);
            // 
            // comboBox_CampaignLevel
            // 
            this.comboBox_CampaignLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_CampaignLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_CampaignLevel.FormattingEnabled = true;
            this.comboBox_CampaignLevel.Items.AddRange(new object[] {
            "None",
            "Copper",
            "Silver",
            "Gold",
            "Diamond"});
            this.comboBox_CampaignLevel.Location = new System.Drawing.Point(229, 157);
            this.comboBox_CampaignLevel.Name = "comboBox_CampaignLevel";
            this.comboBox_CampaignLevel.Size = new System.Drawing.Size(220, 28);
            this.comboBox_CampaignLevel.TabIndex = 186;
            this.comboBox_CampaignLevel.SelectedIndexChanged += new System.EventHandler(this.comboBox_CampaignLevel_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(227, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 20);
            this.label1.TabIndex = 185;
            this.label1.Text = "Front Campaign Level";
            // 
            // comboBox_MonsterType
            // 
            this.comboBox_MonsterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_MonsterType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_MonsterType.FormattingEnabled = true;
            this.comboBox_MonsterType.Items.AddRange(new object[] {
            "None",
            "Humanoid",
            "Beast",
            "Eldritch"});
            this.comboBox_MonsterType.Location = new System.Drawing.Point(3, 210);
            this.comboBox_MonsterType.Name = "comboBox_MonsterType";
            this.comboBox_MonsterType.Size = new System.Drawing.Size(220, 28);
            this.comboBox_MonsterType.TabIndex = 188;
            this.comboBox_MonsterType.SelectedIndexChanged += new System.EventHandler(this.comboBox_CampaignLevel_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 187;
            this.label2.Text = "Monster Type";
            // 
            // button_Browse_Picture
            // 
            this.button_Browse_Picture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Browse_Picture.Location = new System.Drawing.Point(598, 108);
            this.button_Browse_Picture.Name = "button_Browse_Picture";
            this.button_Browse_Picture.Size = new System.Drawing.Size(75, 23);
            this.button_Browse_Picture.TabIndex = 190;
            this.button_Browse_Picture.Text = "Browse";
            this.button_Browse_Picture.UseVisualStyleBackColor = true;
            this.button_Browse_Picture.Click += new System.EventHandler(this.button_Browse_Picture_Click);
            // 
            // textBox_Picture
            // 
            this.textBox_Picture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Picture.Location = new System.Drawing.Point(3, 105);
            this.textBox_Picture.Name = "textBox_Picture";
            this.textBox_Picture.Size = new System.Drawing.Size(589, 26);
            this.textBox_Picture.TabIndex = 189;
            this.textBox_Picture.TextChanged += new System.EventHandler(this.textBox_Picture_TextChanged);
            // 
            // label_Picture
            // 
            this.label_Picture.AutoSize = true;
            this.label_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Picture.Location = new System.Drawing.Point(3, 82);
            this.label_Picture.Name = "label_Picture";
            this.label_Picture.Size = new System.Drawing.Size(58, 20);
            this.label_Picture.TabIndex = 191;
            this.label_Picture.Text = "Picture";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "Copper",
            "Silver",
            "Gold",
            "Diamond"});
            this.comboBox1.Location = new System.Drawing.Point(229, 210);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(220, 28);
            this.comboBox1.TabIndex = 197;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(227, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(162, 20);
            this.label11.TabIndex = 196;
            this.label11.Text = "Back Campaign Level";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(453, 188);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 20);
            this.label12.TabIndex = 194;
            this.label12.Text = "Back Player Count";
            // 
            // numericUpDown25
            // 
            this.numericUpDown25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown25.Location = new System.Drawing.Point(455, 211);
            this.numericUpDown25.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown25.Name = "numericUpDown25";
            this.numericUpDown25.Size = new System.Drawing.Size(220, 26);
            this.numericUpDown25.TabIndex = 195;
            this.numericUpDown25.ValueChanged += new System.EventHandler(this.numericUpDown25_ValueChanged);
            // 
            // textBox_NormalAbility
            // 
            this.textBox_NormalAbility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_NormalAbility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_NormalAbility.Location = new System.Drawing.Point(246, 294);
            this.textBox_NormalAbility.Name = "textBox_NormalAbility";
            this.textBox_NormalAbility.Size = new System.Drawing.Size(427, 26);
            this.textBox_NormalAbility.TabIndex = 207;
            // 
            // label_SpeedNormal
            // 
            this.label_SpeedNormal.AutoSize = true;
            this.label_SpeedNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SpeedNormal.Location = new System.Drawing.Point(3, 271);
            this.label_SpeedNormal.Name = "label_SpeedNormal";
            this.label_SpeedNormal.Size = new System.Drawing.Size(56, 20);
            this.label_SpeedNormal.TabIndex = 200;
            this.label_SpeedNormal.Text = "Speed";
            // 
            // label_AbilitiesNormal
            // 
            this.label_AbilitiesNormal.AutoSize = true;
            this.label_AbilitiesNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_AbilitiesNormal.Location = new System.Drawing.Point(246, 271);
            this.label_AbilitiesNormal.Name = "label_AbilitiesNormal";
            this.label_AbilitiesNormal.Size = new System.Drawing.Size(63, 20);
            this.label_AbilitiesNormal.TabIndex = 202;
            this.label_AbilitiesNormal.Text = "Abilities";
            // 
            // label_WoundsNormal
            // 
            this.label_WoundsNormal.AutoSize = true;
            this.label_WoundsNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WoundsNormal.Location = new System.Drawing.Point(80, 271);
            this.label_WoundsNormal.Name = "label_WoundsNormal";
            this.label_WoundsNormal.Size = new System.Drawing.Size(68, 20);
            this.label_WoundsNormal.TabIndex = 203;
            this.label_WoundsNormal.Text = "Wounds";
            // 
            // label_ArmorNormal
            // 
            this.label_ArmorNormal.AutoSize = true;
            this.label_ArmorNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ArmorNormal.Location = new System.Drawing.Point(165, 271);
            this.label_ArmorNormal.Name = "label_ArmorNormal";
            this.label_ArmorNormal.Size = new System.Drawing.Size(52, 20);
            this.label_ArmorNormal.TabIndex = 205;
            this.label_ArmorNormal.Text = "Armor";
            // 
            // label_FrontNormal
            // 
            this.label_FrontNormal.AutoSize = true;
            this.label_FrontNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FrontNormal.Location = new System.Drawing.Point(3, 246);
            this.label_FrontNormal.Name = "label_FrontNormal";
            this.label_FrontNormal.Size = new System.Drawing.Size(101, 20);
            this.label_FrontNormal.TabIndex = 209;
            this.label_FrontNormal.Text = "Front Normal";
            // 
            // numericUpDown_NormalArmor
            // 
            this.numericUpDown_NormalArmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_NormalArmor.Location = new System.Drawing.Point(165, 294);
            this.numericUpDown_NormalArmor.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_NormalArmor.Name = "numericUpDown_NormalArmor";
            this.numericUpDown_NormalArmor.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_NormalArmor.TabIndex = 206;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(165, 271);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 20);
            this.label17.TabIndex = 205;
            this.label17.Text = "Armor";
            // 
            // numericUpDown_NormalWounds
            // 
            this.numericUpDown_NormalWounds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_NormalWounds.Location = new System.Drawing.Point(84, 294);
            this.numericUpDown_NormalWounds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_NormalWounds.Name = "numericUpDown_NormalWounds";
            this.numericUpDown_NormalWounds.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_NormalWounds.TabIndex = 204;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(80, 271);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 20);
            this.label18.TabIndex = 203;
            this.label18.Text = "Wounds";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(246, 271);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 20);
            this.label19.TabIndex = 202;
            this.label19.Text = "Abilities";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 271);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 20);
            this.label20.TabIndex = 200;
            this.label20.Text = "Speed";
            // 
            // textBox_MasterAbility
            // 
            this.textBox_MasterAbility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_MasterAbility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MasterAbility.Location = new System.Drawing.Point(246, 412);
            this.textBox_MasterAbility.Name = "textBox_MasterAbility";
            this.textBox_MasterAbility.Size = new System.Drawing.Size(427, 26);
            this.textBox_MasterAbility.TabIndex = 217;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 389);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 20);
            this.label13.TabIndex = 210;
            this.label13.Text = "Speed";
            // 
            // numericUpDown_MasterSpeed
            // 
            this.numericUpDown_MasterSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_MasterSpeed.Location = new System.Drawing.Point(3, 412);
            this.numericUpDown_MasterSpeed.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_MasterSpeed.Name = "numericUpDown_MasterSpeed";
            this.numericUpDown_MasterSpeed.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_MasterSpeed.TabIndex = 211;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(246, 389);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 20);
            this.label14.TabIndex = 212;
            this.label14.Text = "Abilities";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(80, 389);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 20);
            this.label15.TabIndex = 213;
            this.label15.Text = "Wounds";
            // 
            // numericUpDown_MasterWounds
            // 
            this.numericUpDown_MasterWounds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_MasterWounds.Location = new System.Drawing.Point(84, 412);
            this.numericUpDown_MasterWounds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_MasterWounds.Name = "numericUpDown_MasterWounds";
            this.numericUpDown_MasterWounds.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_MasterWounds.TabIndex = 214;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(165, 389);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 20);
            this.label16.TabIndex = 215;
            this.label16.Text = "Armor";
            // 
            // numericUpDown_MasterArmor
            // 
            this.numericUpDown_MasterArmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_MasterArmor.Location = new System.Drawing.Point(165, 412);
            this.numericUpDown_MasterArmor.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_MasterArmor.Name = "numericUpDown_MasterArmor";
            this.numericUpDown_MasterArmor.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_MasterArmor.TabIndex = 216;
            // 
            // label_FrontMaster
            // 
            this.label_FrontMaster.AutoSize = true;
            this.label_FrontMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FrontMaster.Location = new System.Drawing.Point(3, 365);
            this.label_FrontMaster.Name = "label_FrontMaster";
            this.label_FrontMaster.Size = new System.Drawing.Size(100, 20);
            this.label_FrontMaster.TabIndex = 219;
            this.label_FrontMaster.Text = "Front Master";
            // 
            // textBox_bNormalAbility
            // 
            this.textBox_bNormalAbility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_bNormalAbility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_bNormalAbility.Location = new System.Drawing.Point(246, 532);
            this.textBox_bNormalAbility.Name = "textBox_bNormalAbility";
            this.textBox_bNormalAbility.Size = new System.Drawing.Size(427, 26);
            this.textBox_bNormalAbility.TabIndex = 227;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 509);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 20);
            this.label7.TabIndex = 220;
            this.label7.Text = "Speed";
            // 
            // numericUpDown_bNormalSpeed
            // 
            this.numericUpDown_bNormalSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_bNormalSpeed.Location = new System.Drawing.Point(3, 532);
            this.numericUpDown_bNormalSpeed.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bNormalSpeed.Name = "numericUpDown_bNormalSpeed";
            this.numericUpDown_bNormalSpeed.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_bNormalSpeed.TabIndex = 221;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(246, 509);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 20);
            this.label8.TabIndex = 222;
            this.label8.Text = "Abilities";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(80, 509);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 20);
            this.label9.TabIndex = 223;
            this.label9.Text = "Wounds";
            // 
            // numericUpDown_bNormalWound
            // 
            this.numericUpDown_bNormalWound.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_bNormalWound.Location = new System.Drawing.Point(84, 532);
            this.numericUpDown_bNormalWound.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bNormalWound.Name = "numericUpDown_bNormalWound";
            this.numericUpDown_bNormalWound.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_bNormalWound.TabIndex = 224;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(165, 509);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 20);
            this.label10.TabIndex = 225;
            this.label10.Text = "Armor";
            // 
            // numericUpDown_bNormalArmor
            // 
            this.numericUpDown_bNormalArmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_bNormalArmor.Location = new System.Drawing.Point(165, 532);
            this.numericUpDown_bNormalArmor.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bNormalArmor.Name = "numericUpDown_bNormalArmor";
            this.numericUpDown_bNormalArmor.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_bNormalArmor.TabIndex = 226;
            // 
            // label_BackNormal
            // 
            this.label_BackNormal.AutoSize = true;
            this.label_BackNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_BackNormal.Location = new System.Drawing.Point(3, 485);
            this.label_BackNormal.Name = "label_BackNormal";
            this.label_BackNormal.Size = new System.Drawing.Size(99, 20);
            this.label_BackNormal.TabIndex = 229;
            this.label_BackNormal.Text = "Back Normal";
            // 
            // textBox_bMasterAbility
            // 
            this.textBox_bMasterAbility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_bMasterAbility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_bMasterAbility.Location = new System.Drawing.Point(246, 654);
            this.textBox_bMasterAbility.Name = "textBox_bMasterAbility";
            this.textBox_bMasterAbility.Size = new System.Drawing.Size(427, 26);
            this.textBox_bMasterAbility.TabIndex = 237;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 631);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 230;
            this.label3.Text = "Speed";
            // 
            // numericUpDown_bMasterSpeed
            // 
            this.numericUpDown_bMasterSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_bMasterSpeed.Location = new System.Drawing.Point(3, 654);
            this.numericUpDown_bMasterSpeed.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bMasterSpeed.Name = "numericUpDown_bMasterSpeed";
            this.numericUpDown_bMasterSpeed.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_bMasterSpeed.TabIndex = 231;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(246, 631);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 232;
            this.label4.Text = "Abilities";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(80, 631);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.TabIndex = 233;
            this.label5.Text = "Wounds";
            // 
            // numericUpDown_bMasterWound
            // 
            this.numericUpDown_bMasterWound.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_bMasterWound.Location = new System.Drawing.Point(84, 654);
            this.numericUpDown_bMasterWound.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bMasterWound.Name = "numericUpDown_bMasterWound";
            this.numericUpDown_bMasterWound.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_bMasterWound.TabIndex = 234;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(165, 631);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 20);
            this.label6.TabIndex = 235;
            this.label6.Text = "Armor";
            // 
            // numericUpDown_bMasterArmor
            // 
            this.numericUpDown_bMasterArmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_bMasterArmor.Location = new System.Drawing.Point(165, 654);
            this.numericUpDown_bMasterArmor.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bMasterArmor.Name = "numericUpDown_bMasterArmor";
            this.numericUpDown_bMasterArmor.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_bMasterArmor.TabIndex = 236;
            // 
            // label_BackMaster
            // 
            this.label_BackMaster.AutoSize = true;
            this.label_BackMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_BackMaster.Location = new System.Drawing.Point(3, 607);
            this.label_BackMaster.Name = "label_BackMaster";
            this.label_BackMaster.Size = new System.Drawing.Size(98, 20);
            this.label_BackMaster.TabIndex = 239;
            this.label_BackMaster.Text = "Back Master";
            // 
            // numericUpDown_NormalSpeed
            // 
            this.numericUpDown_NormalSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_NormalSpeed.Location = new System.Drawing.Point(3, 294);
            this.numericUpDown_NormalSpeed.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_NormalSpeed.Name = "numericUpDown_NormalSpeed";
            this.numericUpDown_NormalSpeed.Size = new System.Drawing.Size(75, 26);
            this.numericUpDown_NormalSpeed.TabIndex = 201;
            // 
            // diceControl_BackMaster
            // 
            this.diceControl_BackMaster.Location = new System.Drawing.Point(0, 685);
            this.diceControl_BackMaster.Margin = new System.Windows.Forms.Padding(2);
            this.diceControl_BackMaster.Name = "diceControl_BackMaster";
            this.diceControl_BackMaster.Size = new System.Drawing.Size(670, 40);
            this.diceControl_BackMaster.TabIndex = 238;
            // 
            // diceControl_BackNormal
            // 
            this.diceControl_BackNormal.Location = new System.Drawing.Point(0, 563);
            this.diceControl_BackNormal.Margin = new System.Windows.Forms.Padding(2);
            this.diceControl_BackNormal.Name = "diceControl_BackNormal";
            this.diceControl_BackNormal.Size = new System.Drawing.Size(671, 38);
            this.diceControl_BackNormal.TabIndex = 228;
            // 
            // diceControl_FrontMaster
            // 
            this.diceControl_FrontMaster.Location = new System.Drawing.Point(0, 443);
            this.diceControl_FrontMaster.Margin = new System.Windows.Forms.Padding(2);
            this.diceControl_FrontMaster.Name = "diceControl_FrontMaster";
            this.diceControl_FrontMaster.Size = new System.Drawing.Size(673, 35);
            this.diceControl_FrontMaster.TabIndex = 218;
            // 
            // diceControl_FrontNormal
            // 
            this.diceControl_FrontNormal.Location = new System.Drawing.Point(0, 325);
            this.diceControl_FrontNormal.Margin = new System.Windows.Forms.Padding(2);
            this.diceControl_FrontNormal.Name = "diceControl_FrontNormal";
            this.diceControl_FrontNormal.Size = new System.Drawing.Size(671, 31);
            this.diceControl_FrontNormal.TabIndex = 208;
            // 
            // Monsters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.label_BackMaster);
            this.Controls.Add(this.diceControl_BackMaster);
            this.Controls.Add(this.textBox_bMasterAbility);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDown_bMasterSpeed);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDown_bMasterWound);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown_bMasterArmor);
            this.Controls.Add(this.label_BackNormal);
            this.Controls.Add(this.diceControl_BackNormal);
            this.Controls.Add(this.textBox_bNormalAbility);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDown_bNormalSpeed);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numericUpDown_bNormalWound);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numericUpDown_bNormalArmor);
            this.Controls.Add(this.label_FrontMaster);
            this.Controls.Add(this.diceControl_FrontMaster);
            this.Controls.Add(this.textBox_MasterAbility);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.numericUpDown_MasterSpeed);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.numericUpDown_MasterWounds);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.numericUpDown_MasterArmor);
            this.Controls.Add(this.label_FrontNormal);
            this.Controls.Add(this.diceControl_FrontNormal);
            this.Controls.Add(this.textBox_NormalAbility);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label_SpeedNormal);
            this.Controls.Add(this.numericUpDown_NormalSpeed);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label_AbilitiesNormal);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label_WoundsNormal);
            this.Controls.Add(this.numericUpDown_NormalWounds);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label_ArmorNormal);
            this.Controls.Add(this.numericUpDown_NormalArmor);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.numericUpDown25);
            this.Controls.Add(this.label_Picture);
            this.Controls.Add(this.button_Browse_Picture);
            this.Controls.Add(this.textBox_Picture);
            this.Controls.Add(this.comboBox_MonsterType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox_CampaignLevel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_Number);
            this.Controls.Add(this.numericUpDown_Playercount);
            this.Controls.Add(this.comboBox_AttackType);
            this.Controls.Add(this.label_AttackType);
            this.Controls.Add(this.richTextBox_Name);
            this.Controls.Add(this.label_Name);
            this.Name = "Monsters";
            this.Size = new System.Drawing.Size(676, 730);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Playercount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NormalArmor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NormalWounds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MasterSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MasterWounds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MasterArmor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bNormalSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bNormalWound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bNormalArmor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bMasterSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bMasterWound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bMasterArmor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NormalSpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox_Name;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.ComboBox comboBox_AttackType;
        private System.Windows.Forms.Label label_AttackType;
        private System.Windows.Forms.Label label_Number;
        private System.Windows.Forms.NumericUpDown numericUpDown_Playercount;
        private System.Windows.Forms.ComboBox comboBox_CampaignLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_MonsterType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_Browse_Picture;
        private System.Windows.Forms.TextBox textBox_Picture;
        private System.Windows.Forms.Label label_Picture;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDown25;
        private System.Windows.Forms.TextBox textBox_NormalAbility;
        private System.Windows.Forms.Label label_SpeedNormal;
        private System.Windows.Forms.Label label_AbilitiesNormal;
        private System.Windows.Forms.Label label_WoundsNormal;
        private System.Windows.Forms.Label label_ArmorNormal;
        private DiceControl diceControl_FrontNormal;
        private System.Windows.Forms.Label label_FrontNormal;
        private System.Windows.Forms.NumericUpDown numericUpDown_NormalArmor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDown_NormalWounds;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private DiceControl diceControl_FrontMaster;
        private System.Windows.Forms.TextBox textBox_MasterAbility;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDown_MasterSpeed;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown_MasterWounds;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDown_MasterArmor;
        private System.Windows.Forms.Label label_FrontMaster;
        private DiceControl diceControl_BackNormal;
        private System.Windows.Forms.TextBox textBox_bNormalAbility;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_bNormalSpeed;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown_bNormalWound;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown_bNormalArmor;
        private System.Windows.Forms.Label label_BackNormal;
        private DiceControl diceControl_BackMaster;
        private System.Windows.Forms.TextBox textBox_bMasterAbility;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown_bMasterSpeed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown_bMasterWound;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_bMasterArmor;
        private System.Windows.Forms.Label label_BackMaster;
        private System.Windows.Forms.NumericUpDown numericUpDown_NormalSpeed;
    }
}
