﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DescentCardCreator
{
    public partial class CostXYZControl : UserControl
    {
        public string CostValue
        {
            get
            {
                if (checkBox_X_Cost.Checked)
                {
                    return "X";
                }
                else if (checkBox_Y_Cost.Checked)
                {
                    return "Y";
                }
                else if (checkBox_Z_Cost.Checked)
                {
                    return "Z";
                }
                else
                {
                    return numericUpDown_Cost.Value.ToString();
                }
            }
        }

        public CostXYZControl(string _labelname)
        {
            InitializeComponent();
            label_ThreadCost.Text = _labelname;
        }

        private void checkBox_X_Cost_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_X_Cost.Checked)
            {
                checkBox_Y_Cost.Checked = false;
                checkBox_Z_Cost.Checked = false;
                numericUpDown_Cost.Enabled = false;
            }
            else
            {
                if (!checkBox_Y_Cost.Checked || !checkBox_Z_Cost.Checked)
                {
                    numericUpDown_Cost.Enabled = true;
                }
            }
        }

        private void checkBox_Y_Cost_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Y_Cost.Checked)
            {
                checkBox_X_Cost.Checked = false;
                checkBox_Z_Cost.Checked = false;
                numericUpDown_Cost.Enabled = false;
            }
            else
            {
                if (!checkBox_X_Cost.Checked || !checkBox_Z_Cost.Checked)
                {
                    numericUpDown_Cost.Enabled = true;
                }
            }
        }
        
        private void checkBox_Z_Cost_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_Z_Cost.Checked)
            {
                checkBox_Y_Cost.Checked = false;
                checkBox_X_Cost.Checked = false;
                numericUpDown_Cost.Enabled = false;
            }
            else
            {
                if (!checkBox_Y_Cost.Checked || !checkBox_X_Cost.Checked)
                {
                    numericUpDown_Cost.Enabled = true;
                }
            }
        }

        private void numericUpDown_ThreadCost_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown_Cost.Value != 0)
            {
                checkBox_X_Cost.Checked = false;
                checkBox_Y_Cost.Checked = false;
                checkBox_Z_Cost.Checked = false;
                checkBox_X_Cost.Enabled = false;
                checkBox_Y_Cost.Enabled = false;
                checkBox_Z_Cost.Enabled = false;
            }
            else
            {
                checkBox_X_Cost.Enabled = true;
                checkBox_Y_Cost.Enabled = true;
                checkBox_Z_Cost.Enabled = true;
            }
        }
    }
}