﻿namespace DescentCardCreator
{
    partial class HeroSheets
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Wound = new System.Windows.Forms.Label();
            this.numericUpDown_Wound = new System.Windows.Forms.NumericUpDown();
            this.label_Fatigue = new System.Windows.Forms.Label();
            this.numericUpDown_Fatigue = new System.Windows.Forms.NumericUpDown();
            this.label_Armor = new System.Windows.Forms.Label();
            this.numericUpDown_Armor = new System.Windows.Forms.NumericUpDown();
            this.label_Speed = new System.Windows.Forms.Label();
            this.numericUpDown_Speed = new System.Windows.Forms.NumericUpDown();
            this.richTextBox_SpecialAbility = new System.Windows.Forms.RichTextBox();
            this.label_CardText = new System.Windows.Forms.Label();
            this.richTextBox_Name = new System.Windows.Forms.RichTextBox();
            this.label_Name = new System.Windows.Forms.Label();
            this.label_Picture = new System.Windows.Forms.Label();
            this.button_Browse_Picture = new System.Windows.Forms.Button();
            this.textBox_Picture = new System.Windows.Forms.TextBox();
            this.label_ConquestValue = new System.Windows.Forms.Label();
            this.numericUpDown_ConquestValue = new System.Windows.Forms.NumericUpDown();
            this.label_MeleeTrait = new System.Windows.Forms.Label();
            this.numericUpDown_MeleeTrait = new System.Windows.Forms.NumericUpDown();
            this.label_SubterfugeTrait = new System.Windows.Forms.Label();
            this.numericUpDown_SubterfugeTrait = new System.Windows.Forms.NumericUpDown();
            this.label_WizardaryTrait = new System.Windows.Forms.Label();
            this.numericUpDown_WizardaryTrait = new System.Windows.Forms.NumericUpDown();
            this.checkBox_WizadarySkill = new System.Windows.Forms.CheckBox();
            this.checkBox_SubterfugeSkill = new System.Windows.Forms.CheckBox();
            this.checkBox_MeleeSkill = new System.Windows.Forms.CheckBox();
            this.label_MeleeSkill = new System.Windows.Forms.Label();
            this.numericUpDown_MeleeSkill = new System.Windows.Forms.NumericUpDown();
            this.label_SubterfugeSkill = new System.Windows.Forms.Label();
            this.numericUpDown_SubterfugeSkill = new System.Windows.Forms.NumericUpDown();
            this.label_WizadarySkill = new System.Windows.Forms.Label();
            this.numericUpDown_WizardarySkill = new System.Windows.Forms.NumericUpDown();
            this.checkBox_Wound = new System.Windows.Forms.CheckBox();
            this.checkBox_Fatigue = new System.Windows.Forms.CheckBox();
            this.checkBox_Armor = new System.Windows.Forms.CheckBox();
            this.checkBox_Speed = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Wound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Fatigue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Armor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Speed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ConquestValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MeleeTrait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubterfugeTrait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_WizardaryTrait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MeleeSkill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubterfugeSkill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_WizardarySkill)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Wound
            // 
            this.label_Wound.AutoSize = true;
            this.label_Wound.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Wound.Location = new System.Drawing.Point(3, 285);
            this.label_Wound.Name = "label_Wound";
            this.label_Wound.Size = new System.Drawing.Size(68, 20);
            this.label_Wound.TabIndex = 135;
            this.label_Wound.Text = "Wounds";
            // 
            // numericUpDown_Wound
            // 
            this.numericUpDown_Wound.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Wound.Location = new System.Drawing.Point(3, 308);
            this.numericUpDown_Wound.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_Wound.Name = "numericUpDown_Wound";
            this.numericUpDown_Wound.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_Wound.TabIndex = 136;
            this.numericUpDown_Wound.ValueChanged += new System.EventHandler(this.numericUpDown_Wound_ValueChanged);
            // 
            // label_Fatigue
            // 
            this.label_Fatigue.AutoSize = true;
            this.label_Fatigue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Fatigue.Location = new System.Drawing.Point(122, 285);
            this.label_Fatigue.Name = "label_Fatigue";
            this.label_Fatigue.Size = new System.Drawing.Size(63, 20);
            this.label_Fatigue.TabIndex = 138;
            this.label_Fatigue.Text = "Fatigue";
            // 
            // numericUpDown_Fatigue
            // 
            this.numericUpDown_Fatigue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Fatigue.Location = new System.Drawing.Point(126, 308);
            this.numericUpDown_Fatigue.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_Fatigue.Name = "numericUpDown_Fatigue";
            this.numericUpDown_Fatigue.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_Fatigue.TabIndex = 139;
            this.numericUpDown_Fatigue.ValueChanged += new System.EventHandler(this.numericUpDown_Fatigue_ValueChanged);
            // 
            // label_Armor
            // 
            this.label_Armor.AutoSize = true;
            this.label_Armor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Armor.Location = new System.Drawing.Point(249, 285);
            this.label_Armor.Name = "label_Armor";
            this.label_Armor.Size = new System.Drawing.Size(52, 20);
            this.label_Armor.TabIndex = 140;
            this.label_Armor.Text = "Armor";
            // 
            // numericUpDown_Armor
            // 
            this.numericUpDown_Armor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Armor.Location = new System.Drawing.Point(249, 308);
            this.numericUpDown_Armor.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_Armor.Name = "numericUpDown_Armor";
            this.numericUpDown_Armor.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_Armor.TabIndex = 141;
            this.numericUpDown_Armor.ValueChanged += new System.EventHandler(this.numericUpDown_Armor_ValueChanged);
            // 
            // label_Speed
            // 
            this.label_Speed.AutoSize = true;
            this.label_Speed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Speed.Location = new System.Drawing.Point(372, 285);
            this.label_Speed.Name = "label_Speed";
            this.label_Speed.Size = new System.Drawing.Size(56, 20);
            this.label_Speed.TabIndex = 142;
            this.label_Speed.Text = "Speed";
            // 
            // numericUpDown_Speed
            // 
            this.numericUpDown_Speed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Speed.Location = new System.Drawing.Point(372, 308);
            this.numericUpDown_Speed.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_Speed.Name = "numericUpDown_Speed";
            this.numericUpDown_Speed.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_Speed.TabIndex = 143;
            this.numericUpDown_Speed.ValueChanged += new System.EventHandler(this.numericUpDown_Speed_ValueChanged);
            // 
            // richTextBox_SpecialAbility
            // 
            this.richTextBox_SpecialAbility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_SpecialAbility.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_SpecialAbility.Location = new System.Drawing.Point(3, 108);
            this.richTextBox_SpecialAbility.Name = "richTextBox_SpecialAbility";
            this.richTextBox_SpecialAbility.Size = new System.Drawing.Size(487, 122);
            this.richTextBox_SpecialAbility.TabIndex = 147;
            this.richTextBox_SpecialAbility.Text = "";
            this.richTextBox_SpecialAbility.TextChanged += new System.EventHandler(this.richTextBox_SpecialAbility_TextChanged);
            // 
            // label_CardText
            // 
            this.label_CardText.AutoSize = true;
            this.label_CardText.BackColor = System.Drawing.Color.Transparent;
            this.label_CardText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CardText.Location = new System.Drawing.Point(3, 85);
            this.label_CardText.Name = "label_CardText";
            this.label_CardText.Size = new System.Drawing.Size(106, 20);
            this.label_CardText.TabIndex = 146;
            this.label_CardText.Text = "Special Ability";
            // 
            // richTextBox_Name
            // 
            this.richTextBox_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Name.Font = new System.Drawing.Font("Kelmscott", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_Name.Location = new System.Drawing.Point(3, 26);
            this.richTextBox_Name.Name = "richTextBox_Name";
            this.richTextBox_Name.Size = new System.Drawing.Size(487, 56);
            this.richTextBox_Name.TabIndex = 145;
            this.richTextBox_Name.Text = "";
            this.richTextBox_Name.TextChanged += new System.EventHandler(this.richTextBox_Name_TextChanged);
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.Location = new System.Drawing.Point(3, 3);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(51, 20);
            this.label_Name.TabIndex = 144;
            this.label_Name.Text = "Name";
            // 
            // label_Picture
            // 
            this.label_Picture.AutoSize = true;
            this.label_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Picture.Location = new System.Drawing.Point(122, 233);
            this.label_Picture.Name = "label_Picture";
            this.label_Picture.Size = new System.Drawing.Size(58, 20);
            this.label_Picture.TabIndex = 196;
            this.label_Picture.Text = "Picture";
            // 
            // button_Browse_Picture
            // 
            this.button_Browse_Picture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Browse_Picture.Location = new System.Drawing.Point(415, 258);
            this.button_Browse_Picture.Name = "button_Browse_Picture";
            this.button_Browse_Picture.Size = new System.Drawing.Size(75, 23);
            this.button_Browse_Picture.TabIndex = 195;
            this.button_Browse_Picture.Text = "Browse";
            this.button_Browse_Picture.UseVisualStyleBackColor = true;
            this.button_Browse_Picture.Click += new System.EventHandler(this.button_Browse_Picture_Click);
            // 
            // textBox_Picture
            // 
            this.textBox_Picture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Picture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Picture.Location = new System.Drawing.Point(126, 256);
            this.textBox_Picture.Name = "textBox_Picture";
            this.textBox_Picture.Size = new System.Drawing.Size(283, 26);
            this.textBox_Picture.TabIndex = 194;
            this.textBox_Picture.TextChanged += new System.EventHandler(this.textBox_Picture_TextChanged);
            // 
            // label_ConquestValue
            // 
            this.label_ConquestValue.AutoSize = true;
            this.label_ConquestValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ConquestValue.Location = new System.Drawing.Point(1, 233);
            this.label_ConquestValue.Name = "label_ConquestValue";
            this.label_ConquestValue.Size = new System.Drawing.Size(123, 20);
            this.label_ConquestValue.TabIndex = 192;
            this.label_ConquestValue.Text = "Conquest Value";
            // 
            // numericUpDown_ConquestValue
            // 
            this.numericUpDown_ConquestValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_ConquestValue.Location = new System.Drawing.Point(3, 256);
            this.numericUpDown_ConquestValue.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_ConquestValue.Name = "numericUpDown_ConquestValue";
            this.numericUpDown_ConquestValue.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_ConquestValue.TabIndex = 193;
            this.numericUpDown_ConquestValue.ValueChanged += new System.EventHandler(this.numericUpDown_ConquestValue_ValueChanged);
            // 
            // label_MeleeTrait
            // 
            this.label_MeleeTrait.AutoSize = true;
            this.label_MeleeTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MeleeTrait.Location = new System.Drawing.Point(3, 367);
            this.label_MeleeTrait.Name = "label_MeleeTrait";
            this.label_MeleeTrait.Size = new System.Drawing.Size(87, 20);
            this.label_MeleeTrait.TabIndex = 197;
            this.label_MeleeTrait.Text = "Melee Trait";
            // 
            // numericUpDown_MeleeTrait
            // 
            this.numericUpDown_MeleeTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_MeleeTrait.Location = new System.Drawing.Point(3, 390);
            this.numericUpDown_MeleeTrait.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_MeleeTrait.Name = "numericUpDown_MeleeTrait";
            this.numericUpDown_MeleeTrait.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_MeleeTrait.TabIndex = 198;
            this.numericUpDown_MeleeTrait.ValueChanged += new System.EventHandler(this.numericUpDown_MeleeTrait_ValueChanged);
            // 
            // label_SubterfugeTrait
            // 
            this.label_SubterfugeTrait.AutoSize = true;
            this.label_SubterfugeTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SubterfugeTrait.Location = new System.Drawing.Point(122, 367);
            this.label_SubterfugeTrait.Name = "label_SubterfugeTrait";
            this.label_SubterfugeTrait.Size = new System.Drawing.Size(124, 20);
            this.label_SubterfugeTrait.TabIndex = 199;
            this.label_SubterfugeTrait.Text = "Subterfuge Trait";
            // 
            // numericUpDown_SubterfugeTrait
            // 
            this.numericUpDown_SubterfugeTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_SubterfugeTrait.Location = new System.Drawing.Point(126, 390);
            this.numericUpDown_SubterfugeTrait.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_SubterfugeTrait.Name = "numericUpDown_SubterfugeTrait";
            this.numericUpDown_SubterfugeTrait.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_SubterfugeTrait.TabIndex = 200;
            this.numericUpDown_SubterfugeTrait.ValueChanged += new System.EventHandler(this.numericUpDown_SubterfugeTrait_ValueChanged);
            // 
            // label_WizardaryTrait
            // 
            this.label_WizardaryTrait.AutoSize = true;
            this.label_WizardaryTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WizardaryTrait.Location = new System.Drawing.Point(245, 367);
            this.label_WizardaryTrait.Name = "label_WizardaryTrait";
            this.label_WizardaryTrait.Size = new System.Drawing.Size(114, 20);
            this.label_WizardaryTrait.TabIndex = 201;
            this.label_WizardaryTrait.Text = "Wizardary Trait";
            // 
            // numericUpDown_WizardaryTrait
            // 
            this.numericUpDown_WizardaryTrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_WizardaryTrait.Location = new System.Drawing.Point(249, 390);
            this.numericUpDown_WizardaryTrait.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_WizardaryTrait.Name = "numericUpDown_WizardaryTrait";
            this.numericUpDown_WizardaryTrait.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_WizardaryTrait.TabIndex = 202;
            this.numericUpDown_WizardaryTrait.ValueChanged += new System.EventHandler(this.numericUpDown_WizardaryTrait_ValueChanged);
            // 
            // checkBox_WizadarySkill
            // 
            this.checkBox_WizadarySkill.AutoSize = true;
            this.checkBox_WizadarySkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_WizadarySkill.Location = new System.Drawing.Point(249, 474);
            this.checkBox_WizadarySkill.Name = "checkBox_WizadarySkill";
            this.checkBox_WizadarySkill.Size = new System.Drawing.Size(34, 24);
            this.checkBox_WizadarySkill.TabIndex = 214;
            this.checkBox_WizadarySkill.Text = "*";
            this.checkBox_WizadarySkill.UseVisualStyleBackColor = true;
            this.checkBox_WizadarySkill.CheckedChanged += new System.EventHandler(this.checkBox_WizadarySkill_CheckedChanged);
            // 
            // checkBox_SubterfugeSkill
            // 
            this.checkBox_SubterfugeSkill.AutoSize = true;
            this.checkBox_SubterfugeSkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_SubterfugeSkill.Location = new System.Drawing.Point(126, 474);
            this.checkBox_SubterfugeSkill.Name = "checkBox_SubterfugeSkill";
            this.checkBox_SubterfugeSkill.Size = new System.Drawing.Size(34, 24);
            this.checkBox_SubterfugeSkill.TabIndex = 213;
            this.checkBox_SubterfugeSkill.Text = "*";
            this.checkBox_SubterfugeSkill.UseVisualStyleBackColor = true;
            this.checkBox_SubterfugeSkill.CheckedChanged += new System.EventHandler(this.checkBox_SubterfugeSkill_CheckedChanged);
            // 
            // checkBox_MeleeSkill
            // 
            this.checkBox_MeleeSkill.AutoSize = true;
            this.checkBox_MeleeSkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_MeleeSkill.Location = new System.Drawing.Point(3, 474);
            this.checkBox_MeleeSkill.Name = "checkBox_MeleeSkill";
            this.checkBox_MeleeSkill.Size = new System.Drawing.Size(34, 24);
            this.checkBox_MeleeSkill.TabIndex = 212;
            this.checkBox_MeleeSkill.Text = "*";
            this.checkBox_MeleeSkill.UseVisualStyleBackColor = true;
            this.checkBox_MeleeSkill.CheckedChanged += new System.EventHandler(this.checkBox_MeleeSkill_CheckedChanged);
            // 
            // label_MeleeSkill
            // 
            this.label_MeleeSkill.AutoSize = true;
            this.label_MeleeSkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MeleeSkill.Location = new System.Drawing.Point(3, 419);
            this.label_MeleeSkill.Name = "label_MeleeSkill";
            this.label_MeleeSkill.Size = new System.Drawing.Size(88, 20);
            this.label_MeleeSkill.TabIndex = 206;
            this.label_MeleeSkill.Text = "Melee Skill ";
            // 
            // numericUpDown_MeleeSkill
            // 
            this.numericUpDown_MeleeSkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_MeleeSkill.Location = new System.Drawing.Point(3, 442);
            this.numericUpDown_MeleeSkill.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_MeleeSkill.Name = "numericUpDown_MeleeSkill";
            this.numericUpDown_MeleeSkill.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_MeleeSkill.TabIndex = 207;
            this.numericUpDown_MeleeSkill.ValueChanged += new System.EventHandler(this.numericUpDown_MeleeSkill_ValueChanged);
            // 
            // label_SubterfugeSkill
            // 
            this.label_SubterfugeSkill.AutoSize = true;
            this.label_SubterfugeSkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SubterfugeSkill.Location = new System.Drawing.Point(122, 419);
            this.label_SubterfugeSkill.Name = "label_SubterfugeSkill";
            this.label_SubterfugeSkill.Size = new System.Drawing.Size(125, 20);
            this.label_SubterfugeSkill.TabIndex = 208;
            this.label_SubterfugeSkill.Text = "Subterfuge Skill ";
            // 
            // numericUpDown_SubterfugeSkill
            // 
            this.numericUpDown_SubterfugeSkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_SubterfugeSkill.Location = new System.Drawing.Point(126, 442);
            this.numericUpDown_SubterfugeSkill.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_SubterfugeSkill.Name = "numericUpDown_SubterfugeSkill";
            this.numericUpDown_SubterfugeSkill.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_SubterfugeSkill.TabIndex = 209;
            this.numericUpDown_SubterfugeSkill.ValueChanged += new System.EventHandler(this.numericUpDown_SubterfugeSkill_ValueChanged);
            // 
            // label_WizadarySkill
            // 
            this.label_WizadarySkill.AutoSize = true;
            this.label_WizadarySkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WizadarySkill.Location = new System.Drawing.Point(245, 419);
            this.label_WizadarySkill.Name = "label_WizadarySkill";
            this.label_WizadarySkill.Size = new System.Drawing.Size(106, 20);
            this.label_WizadarySkill.TabIndex = 210;
            this.label_WizadarySkill.Text = "Wizadary Skill";
            // 
            // numericUpDown_WizardarySkill
            // 
            this.numericUpDown_WizardarySkill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_WizardarySkill.Location = new System.Drawing.Point(249, 442);
            this.numericUpDown_WizardarySkill.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_WizardarySkill.Name = "numericUpDown_WizardarySkill";
            this.numericUpDown_WizardarySkill.Size = new System.Drawing.Size(117, 26);
            this.numericUpDown_WizardarySkill.TabIndex = 211;
            this.numericUpDown_WizardarySkill.ValueChanged += new System.EventHandler(this.numericUpDown_WizardarySkill_ValueChanged);
            // 
            // checkBox_Wound
            // 
            this.checkBox_Wound.AutoSize = true;
            this.checkBox_Wound.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Wound.Location = new System.Drawing.Point(3, 340);
            this.checkBox_Wound.Name = "checkBox_Wound";
            this.checkBox_Wound.Size = new System.Drawing.Size(34, 24);
            this.checkBox_Wound.TabIndex = 215;
            this.checkBox_Wound.Text = "*";
            this.checkBox_Wound.UseVisualStyleBackColor = true;
            this.checkBox_Wound.CheckedChanged += new System.EventHandler(this.checkBox_Wound_CheckedChanged);
            // 
            // checkBox_Fatigue
            // 
            this.checkBox_Fatigue.AutoSize = true;
            this.checkBox_Fatigue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Fatigue.Location = new System.Drawing.Point(126, 340);
            this.checkBox_Fatigue.Name = "checkBox_Fatigue";
            this.checkBox_Fatigue.Size = new System.Drawing.Size(34, 24);
            this.checkBox_Fatigue.TabIndex = 216;
            this.checkBox_Fatigue.Text = "*";
            this.checkBox_Fatigue.UseVisualStyleBackColor = true;
            this.checkBox_Fatigue.CheckedChanged += new System.EventHandler(this.checkBox_Fatigue_CheckedChanged);
            // 
            // checkBox_Armor
            // 
            this.checkBox_Armor.AutoSize = true;
            this.checkBox_Armor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Armor.Location = new System.Drawing.Point(249, 340);
            this.checkBox_Armor.Name = "checkBox_Armor";
            this.checkBox_Armor.Size = new System.Drawing.Size(34, 24);
            this.checkBox_Armor.TabIndex = 217;
            this.checkBox_Armor.Text = "*";
            this.checkBox_Armor.UseVisualStyleBackColor = true;
            this.checkBox_Armor.CheckedChanged += new System.EventHandler(this.checkBox_Armor_CheckedChanged);
            // 
            // checkBox_Speed
            // 
            this.checkBox_Speed.AutoSize = true;
            this.checkBox_Speed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Speed.Location = new System.Drawing.Point(372, 340);
            this.checkBox_Speed.Name = "checkBox_Speed";
            this.checkBox_Speed.Size = new System.Drawing.Size(34, 24);
            this.checkBox_Speed.TabIndex = 218;
            this.checkBox_Speed.Text = "*";
            this.checkBox_Speed.UseVisualStyleBackColor = true;
            this.checkBox_Speed.CheckedChanged += new System.EventHandler(this.checkBox_Speed_CheckedChanged);
            // 
            // HeroSheets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkBox_Speed);
            this.Controls.Add(this.checkBox_Armor);
            this.Controls.Add(this.checkBox_Fatigue);
            this.Controls.Add(this.checkBox_Wound);
            this.Controls.Add(this.checkBox_WizadarySkill);
            this.Controls.Add(this.checkBox_SubterfugeSkill);
            this.Controls.Add(this.checkBox_MeleeSkill);
            this.Controls.Add(this.label_MeleeSkill);
            this.Controls.Add(this.numericUpDown_MeleeSkill);
            this.Controls.Add(this.label_SubterfugeSkill);
            this.Controls.Add(this.numericUpDown_SubterfugeSkill);
            this.Controls.Add(this.label_WizadarySkill);
            this.Controls.Add(this.numericUpDown_WizardarySkill);
            this.Controls.Add(this.label_MeleeTrait);
            this.Controls.Add(this.numericUpDown_MeleeTrait);
            this.Controls.Add(this.label_SubterfugeTrait);
            this.Controls.Add(this.numericUpDown_SubterfugeTrait);
            this.Controls.Add(this.label_WizardaryTrait);
            this.Controls.Add(this.numericUpDown_WizardaryTrait);
            this.Controls.Add(this.label_Picture);
            this.Controls.Add(this.button_Browse_Picture);
            this.Controls.Add(this.textBox_Picture);
            this.Controls.Add(this.label_ConquestValue);
            this.Controls.Add(this.numericUpDown_ConquestValue);
            this.Controls.Add(this.richTextBox_SpecialAbility);
            this.Controls.Add(this.label_CardText);
            this.Controls.Add(this.richTextBox_Name);
            this.Controls.Add(this.label_Name);
            this.Controls.Add(this.label_Speed);
            this.Controls.Add(this.numericUpDown_Speed);
            this.Controls.Add(this.label_Wound);
            this.Controls.Add(this.numericUpDown_Wound);
            this.Controls.Add(this.label_Fatigue);
            this.Controls.Add(this.numericUpDown_Fatigue);
            this.Controls.Add(this.label_Armor);
            this.Controls.Add(this.numericUpDown_Armor);
            this.Name = "HeroSheets";
            this.Size = new System.Drawing.Size(493, 514);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Wound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Fatigue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Armor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Speed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ConquestValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MeleeTrait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubterfugeTrait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_WizardaryTrait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MeleeSkill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubterfugeSkill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_WizardarySkill)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Wound;
        private System.Windows.Forms.NumericUpDown numericUpDown_Wound;
        private System.Windows.Forms.Label label_Fatigue;
        private System.Windows.Forms.NumericUpDown numericUpDown_Fatigue;
        private System.Windows.Forms.Label label_Armor;
        private System.Windows.Forms.NumericUpDown numericUpDown_Armor;
        private System.Windows.Forms.Label label_Speed;
        private System.Windows.Forms.NumericUpDown numericUpDown_Speed;
        private System.Windows.Forms.RichTextBox richTextBox_SpecialAbility;
        private System.Windows.Forms.Label label_CardText;
        private System.Windows.Forms.RichTextBox richTextBox_Name;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_Picture;
        private System.Windows.Forms.Button button_Browse_Picture;
        private System.Windows.Forms.TextBox textBox_Picture;
        private System.Windows.Forms.Label label_ConquestValue;
        private System.Windows.Forms.NumericUpDown numericUpDown_ConquestValue;
        private System.Windows.Forms.Label label_MeleeTrait;
        private System.Windows.Forms.NumericUpDown numericUpDown_MeleeTrait;
        private System.Windows.Forms.Label label_SubterfugeTrait;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubterfugeTrait;
        private System.Windows.Forms.Label label_WizardaryTrait;
        private System.Windows.Forms.NumericUpDown numericUpDown_WizardaryTrait;
        private System.Windows.Forms.CheckBox checkBox_WizadarySkill;
        private System.Windows.Forms.CheckBox checkBox_SubterfugeSkill;
        private System.Windows.Forms.CheckBox checkBox_MeleeSkill;
        private System.Windows.Forms.Label label_MeleeSkill;
        private System.Windows.Forms.NumericUpDown numericUpDown_MeleeSkill;
        private System.Windows.Forms.Label label_SubterfugeSkill;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubterfugeSkill;
        private System.Windows.Forms.Label label_WizadarySkill;
        private System.Windows.Forms.NumericUpDown numericUpDown_WizardarySkill;
        private System.Windows.Forms.CheckBox checkBox_Wound;
        private System.Windows.Forms.CheckBox checkBox_Fatigue;
        private System.Windows.Forms.CheckBox checkBox_Armor;
        private System.Windows.Forms.CheckBox checkBox_Speed;
    }
}
