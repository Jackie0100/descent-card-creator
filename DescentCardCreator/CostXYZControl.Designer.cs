﻿namespace DescentCardCreator
{
    partial class CostXYZControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox_Z_Cost = new System.Windows.Forms.CheckBox();
            this.checkBox_Y_Cost = new System.Windows.Forms.CheckBox();
            this.checkBox_X_Cost = new System.Windows.Forms.CheckBox();
            this.numericUpDown_Cost = new System.Windows.Forms.NumericUpDown();
            this.label_ThreadCost = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Cost)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox_Z_Cost
            // 
            this.checkBox_Z_Cost.AutoSize = true;
            this.checkBox_Z_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Z_Cost.Location = new System.Drawing.Point(9, 141);
            this.checkBox_Z_Cost.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_Z_Cost.Name = "checkBox_Z_Cost";
            this.checkBox_Z_Cost.Size = new System.Drawing.Size(46, 29);
            this.checkBox_Z_Cost.TabIndex = 134;
            this.checkBox_Z_Cost.Text = "Z";
            this.checkBox_Z_Cost.UseVisualStyleBackColor = true;
            this.checkBox_Z_Cost.CheckedChanged += new System.EventHandler(this.checkBox_Z_Cost_CheckedChanged);
            // 
            // checkBox_Y_Cost
            // 
            this.checkBox_Y_Cost.AutoSize = true;
            this.checkBox_Y_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Y_Cost.Location = new System.Drawing.Point(9, 104);
            this.checkBox_Y_Cost.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_Y_Cost.Name = "checkBox_Y_Cost";
            this.checkBox_Y_Cost.Size = new System.Drawing.Size(47, 29);
            this.checkBox_Y_Cost.TabIndex = 133;
            this.checkBox_Y_Cost.Text = "Y";
            this.checkBox_Y_Cost.UseVisualStyleBackColor = true;
            this.checkBox_Y_Cost.CheckedChanged += new System.EventHandler(this.checkBox_Y_Cost_CheckedChanged);
            // 
            // checkBox_X_Cost
            // 
            this.checkBox_X_Cost.AutoSize = true;
            this.checkBox_X_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_X_Cost.Location = new System.Drawing.Point(9, 67);
            this.checkBox_X_Cost.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_X_Cost.Name = "checkBox_X_Cost";
            this.checkBox_X_Cost.Size = new System.Drawing.Size(48, 29);
            this.checkBox_X_Cost.TabIndex = 132;
            this.checkBox_X_Cost.Text = "X";
            this.checkBox_X_Cost.UseVisualStyleBackColor = true;
            this.checkBox_X_Cost.CheckedChanged += new System.EventHandler(this.checkBox_X_Cost_CheckedChanged);
            // 
            // numericUpDown_Cost
            // 
            this.numericUpDown_Cost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Cost.Location = new System.Drawing.Point(9, 29);
            this.numericUpDown_Cost.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown_Cost.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_Cost.Name = "numericUpDown_Cost";
            this.numericUpDown_Cost.Size = new System.Drawing.Size(142, 30);
            this.numericUpDown_Cost.TabIndex = 131;
            this.numericUpDown_Cost.ValueChanged += new System.EventHandler(this.numericUpDown_ThreadCost_ValueChanged);
            // 
            // label_ThreadCost
            // 
            this.label_ThreadCost.AutoSize = true;
            this.label_ThreadCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ThreadCost.Location = new System.Drawing.Point(4, 0);
            this.label_ThreadCost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_ThreadCost.Name = "label_ThreadCost";
            this.label_ThreadCost.Size = new System.Drawing.Size(147, 25);
            this.label_ThreadCost.TabIndex = 130;
            this.label_ThreadCost.Text = "Treachery Cost";
            // 
            // CostXYZControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkBox_Z_Cost);
            this.Controls.Add(this.checkBox_Y_Cost);
            this.Controls.Add(this.checkBox_X_Cost);
            this.Controls.Add(this.numericUpDown_Cost);
            this.Controls.Add(this.label_ThreadCost);
            this.Name = "CostXYZControl";
            this.Size = new System.Drawing.Size(158, 177);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Cost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox_Z_Cost;
        private System.Windows.Forms.CheckBox checkBox_Y_Cost;
        private System.Windows.Forms.CheckBox checkBox_X_Cost;
        private System.Windows.Forms.NumericUpDown numericUpDown_Cost;
        private System.Windows.Forms.Label label_ThreadCost;
    }
}
