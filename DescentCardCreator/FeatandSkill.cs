﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DescentCardGraphics;

namespace DescentCardCreator
{
    public partial class FeatandSkill : UserControl, ICardDesigner
    {
        public MainForm ParentOwner { get; set; }
        public RichTextBox ForcusedRTFControl { get; set; }
        public UserControl ThisControl { get; set; }

        public FeatandSkill()
        {
            InitializeComponent();
            comboBox_CardType.SelectedIndex = 0;
            ThisControl = this;
            foreach (Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    c.GotFocus += new EventHandler(RTFGotFocus);
                    c.LostFocus += new EventHandler(RTFLostFocus);
                    (c as RichTextBox).SelectAll();
                    (c as RichTextBox).SelectionAlignment = HorizontalAlignment.Center;
                }
            }
        }

        public void SetProps(Card c)
        {
            richTextBox_Name.Text = (c as FeatSkill).Name;
            richTextBox_Text.Rtf = (c as FeatSkill).Text;
            comboBox_CardType.SelectedIndex = (int)(c as FeatSkill).SkillType;
        }

        public void Reset()
        {
            foreach(Control c in this.Controls)
            {
                if (c is RichTextBox)
                {
                    (c as RichTextBox).Text = "";
                }
                else if (c is TextBox)
                {
                    (c as TextBox).Text = "";
                }
                else if (c is NumericUpDown)
                {
                    (c as NumericUpDown).Value = 0;
                }
                else if (c is ComboBox)
                {
                    (c as ComboBox).SelectedIndex = 0;
                }
                else if (c is CheckBox)
                {
                    (c as CheckBox).Checked = false;
                }
            }
        }

        public void RTFLostFocus(object sender, EventArgs e)
        {
            ParentOwner.HideFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        public void RTFGotFocus(object sender, EventArgs e)
        {
            ParentOwner.ShowFontStyleButtons();
            ForcusedRTFControl = (RichTextBox)sender;
        }

        private void comboBox_CardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                (ParentOwner.CurrentActiveCard as FeatSkill).SkillType = (FeatSkillTypes)comboBox_CardType.SelectedIndex;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            UpdateCard();
        }

        //private void richTextBox_ItemText_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ParentOwner.CurrentActiveCard.Text = richTextBox_ItemText.Rtf;
        //        UpdateCard();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //    }
        //}

        public void UpdateCard()
        {
            try
            {
                if (ParentOwner.backgroundWorkerPictureUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerPictureUpdater.CancelAsync();
                }
                if (ParentOwner.backgroundWorkerListViewUpdater.WorkerSupportsCancellation)
                {
                    ParentOwner.backgroundWorkerListViewUpdater.CancelAsync();
                }
                ParentOwner.backgroundWorkerPictureUpdater.RunWorkerAsync();
                ParentOwner.backgroundWorkerListViewUpdater.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void richTextBox_Name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ParentOwner.CurrentActiveCard.Name = richTextBox_Name.Text;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void richTextBox_Text_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ParentOwner.CurrentActiveCard.Text = richTextBox_Text.Rtf;
                UpdateCard();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
